# <img src="https://raw.githubusercontent.com/robolaunch/trademark/main/logos/png/robolaunch.png" width="70" height="70" align="center"> robolaunch Kubernetes Robot Operator

<div align="center">
  <p align="center">
    <a href="https://github.com/robolaunch/robot-operator/releases">
      <img src="https://img.shields.io/github/go-mod/go-version/robolaunch/robot-operator" alt="release">
    </a>
    <a href="https://github.com/robolaunch/robot-operator/releases">
      <img src="https://img.shields.io/github/v/release/robolaunch/robot-operator" alt="release">
    </a>
    <a href="https://github.com/robolaunch/robot-operator/blob/main/LICENSE">
      <img src="https://img.shields.io/github/license/robolaunch/robot-operator" alt="license">
    </a>
    <a href="https://hub.docker.com/u/robolaunchio/robot-op-dev">
      <img src="https://img.shields.io/docker/pulls/robolaunchio/robot-op-dev" alt="pulls">
    </a>
    <a href="https://github.com/robolaunch/robot-operator/issues">
      <img src="https://img.shields.io/github/issues/robolaunch/robot-operator" alt="issues">
    </a>
    <a href="https://github.com/robolaunch/robot-operator/actions">
      <img src="https://github.com/robolaunch/robot-operator/actions/workflows/build.yml/badge.svg" alt="build">
    </a>
  </p>
</div>

robolaunch Kubernetes Robot Operator manages lifecycle of ROS 2 based robots and enables defining, deploying and distributing robots declaratively.

## Table of Contents  
- [Idea](#idea)
- [Quick Start](#quick-start)
  - [Installation](#installation)
  - [Deploy Your First Robot](#deploy-your-first-robot)
- [Aims & Roadmap](#aims--roadmap)
- [Contributing](#aims--roadmap)


## Idea

The main idea of this project is to manage robots as Kubernetes custom resources. As a custom resource, a robot's lifecycle contains following operations and benefits.

- **Robot Lifecycle Management**
  - Deployment
  - Update
  - Upgrade
  - Vertical Scaling
    - Adjusting robot's resources
  - Self-healing
- **Robot Observability**
  - ROS observability tools (eg. rViz, Foxglove, ROS Tracker)
  - ROS topic observability
  - Events, metrics and logs
- **GPU Acceleration**
  - Simulation (Gazebo, Ignition)
  - VDI
  - Training Environment
- **Geoghraphic Distribution**
  - Cloud-powered robot
  - Cloud-connected robot
- **Software development lifecycle**
  - Cloud IDE
- **Connectivity**
  - Robot-to-Robot Discovery
  - Node-to-Node Discovery
- **Alerting**
- **Security**

Refer [robolaunch.io](robolaunch.io) and [project wiki](https://github.com/robolaunch/robot-operator/wiki) for more architectural details and documentations.

## Quick Start

### Installation

Make sure your Kubernetes cluster meets [prerequisites here](./docs/installation/installation.md#prerequisites).

Label one of your Kubernetes nodes.

```bash
kubectl label node <NODE> robolaunch.io/platform=true
```

Apply release YAML.

### Deploy Your First Robot

You can try example robots under [`config/samples/`](./config/samples/). For example, to deploy Linorobot 2, first label the node with a specific cluster name.

```bash
kubectl label node <NODE> cluster=my-cluster
```

Then deploy Linorobot 2.

```yaml
# linorobot2.yaml
apiVersion: robot.roboscale.io/v1alpha1
kind: Robot
metadata:
  name: linorobot2
spec:
  robot:
    clusterSelector: cluster
    nodeSelector:
      robolaunch.io/platform: "true"
    distro: foxy
    state: Launched
    tools:
      tracker:
        enabled: true
      cloudIDE:
        enabled: true
      bridge:
        enabled: false
      foxglove:
        enabled: false
      vdi:
        enabled: false
    mode: Single
    resources:
      storage: 15000
      cpuPerContainer: 800m
      memoryPerContainer: 512Mi
    namespacing: false
    workspaces:
    - name: linorobot-ws
      repositories:
      - name: master_br
        url: https://github.com/tunahanertekin/linorobot2
        branch: tuna
        launch:
          launchFilePath: linorobot2_gazebo/launch/gazebo.launch.py
          env:
          - name: LINOROBOT2_BASE
            value: 2wd
      build: Standard
```

```bash
kubectl apply -f linorobot2.yaml
```

After applying YAML, check robot's status by using:
```bash
watch "kubectl get robot linorobot2"
```

To see events and other robot-specific configurations, run:
```bash
kubectl describe robot linorobot2
```

## Aims & Roadmap

- Extending validation rules
- Adding more ROS 2 robots
- Supporting ROS GEMs
- Extending simulation options
- Comprehensive integration tests
- Supporting older ROS distros
- Supporting ROS topic encryption
- Developing fleet interfaces for robot
- Supporting storage classes that supports `ReadWriteMany` access mode

## Contributing

Please see [this guide](./CONTRIBUTING) if you want to contribute.