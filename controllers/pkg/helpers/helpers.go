package helpers

import (
	"strings"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	corev1 "k8s.io/api/core/v1"
)

func PortsFromMapToSlice(portMap map[string]corev1.ContainerPort) []corev1.ContainerPort {
	portSlice := []corev1.ContainerPort{}

	for _, v := range portMap {
		portSlice = append(portSlice, v)
	}

	return portSlice
}

func GetDiscoveryServerDNS(robot robotv1alpha1.Robot) string {

	dsConfig := robot.Spec.Robot.DiscoveryServer
	cluster := robot.Status.Cluster

	var serviceDNSBuilder strings.Builder
	if dsConfig.Cluster == cluster {
		serviceDNSBuilder.WriteString(dsConfig.Hostname + "." + dsConfig.Subdomain + "." + robot.Namespace + ".svc." + cluster + ".local")
	} else {
		serviceDNSBuilder.WriteString(dsConfig.Hostname + "." + dsConfig.Cluster + "." + dsConfig.Subdomain + "." + robot.Namespace + ".svc.clusterset.local")
	}

	return serviceDNSBuilder.String()
}

func GetNodePortMap(svc corev1.Service) map[string]int32 {
	nodePorts := map[string]int32{}
	for _, p := range svc.Spec.Ports {
		nodePorts[p.Name] = p.NodePort
	}

	return nodePorts
}

func Bash(command string) []string {
	return []string{
		"/bin/bash",
		"-c",
		command,
	}
}

func Env(key string, value string) corev1.EnvVar {
	return corev1.EnvVar{
		Name:  key,
		Value: value,
	}
}
