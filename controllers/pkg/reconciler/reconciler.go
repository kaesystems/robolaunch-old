package reconciler

import (
	"time"

	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

func Requeue(
	result *reconcile.Result) {

	result.Requeue = true
	result.RequeueAfter = 3 * time.Second

}
