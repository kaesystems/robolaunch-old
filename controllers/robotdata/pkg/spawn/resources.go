package spawn

import (
	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetRobotNode(cr *robotv1alpha1.RobotData) *robotv1alpha1.RobotNode {

	meta := cr.GetRobotNodeMetadata()

	robotNode := robotv1alpha1.RobotNode{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: robotv1alpha1.RobotNodeSpec{
			UpdateFrequency: cr.Spec.DataExport.Nodes.UpdateFrequency,
		},
	}

	return &robotNode
}

func GetRobotTopic(cr *robotv1alpha1.RobotData) *robotv1alpha1.RobotTopic {

	meta := cr.GetRobotTopicMetadata()

	robotTopic := robotv1alpha1.RobotTopic{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: robotv1alpha1.RobotTopicSpec{
			UpdateFrequency: cr.Spec.DataExport.Topics.UpdateFrequency,
		},
	}

	return &robotTopic
}

func GetRobotService(cr *robotv1alpha1.RobotData) *robotv1alpha1.RobotService {

	meta := cr.GetRobotServiceMetadata()

	robotService := robotv1alpha1.RobotService{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: robotv1alpha1.RobotServiceSpec{
			UpdateFrequency: cr.Spec.DataExport.Services.UpdateFrequency,
		},
	}

	return &robotService
}

func GetRobotAction(cr *robotv1alpha1.RobotData) *robotv1alpha1.RobotAction {

	meta := cr.GetRobotActionMetadata()

	robotAction := robotv1alpha1.RobotAction{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: robotv1alpha1.RobotActionSpec{
			UpdateFrequency: cr.Spec.DataExport.Actions.UpdateFrequency,
		},
	}

	return &robotAction
}
