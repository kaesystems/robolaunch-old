package robotdata

import (
	"context"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
)

// RobotDataReconciler reconciles a RobotData object
type RobotDataReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotdata,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotdata/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotdata/finalizers,verbs=update

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotnodes,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robottopics,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotservices,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotactions,verbs=get;list;watch;create;update;patch;delete

var logger logr.Logger

func (r *RobotDataReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger = log.FromContext(ctx)

	instance, err := r.reconcileGetInstance(ctx, req.NamespacedName)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckOwnedResources(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *RobotDataReconciler) reconcileGetInstance(ctx context.Context, meta types.NamespacedName) (*robotv1alpha1.RobotData, error) {
	instance := &robotv1alpha1.RobotData{}
	err := r.Get(ctx, meta, instance)
	if err != nil {
		return &robotv1alpha1.RobotData{}, err
	}
	return instance, nil
}

func (r *RobotDataReconciler) reconcileCheckStatus(ctx context.Context, instance *robotv1alpha1.RobotData) error {

	if instance.Spec.DataExport.Nodes.Enabled {
		switch instance.Status.NodeStatus.Created {
		case true:
			// watch health
		case false:
			err := r.createRobotNode(ctx, instance)
			if err != nil {
				return err
			}
		}
	}

	if instance.Spec.DataExport.Topics.Enabled {
		switch instance.Status.TopicStatus.Created {
		case true:
			// watch health
		case false:
			err := r.createRobotTopic(ctx, instance)
			if err != nil {
				return err
			}
		}
	}

	if instance.Spec.DataExport.Services.Enabled {
		switch instance.Status.ServiceStatus.Created {
		case true:
			// watch health
		case false:
			err := r.createRobotService(ctx, instance)
			if err != nil {
				return err
			}
		}
	}

	if instance.Spec.DataExport.Actions.Enabled {
		switch instance.Status.ActionStatus.Created {
		case true:
			// watch health
		case false:
			err := r.createRobotAction(ctx, instance)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (r *RobotDataReconciler) reconcileCheckOwnedResources(ctx context.Context, instance *robotv1alpha1.RobotData) error {

	if instance.Spec.DataExport.Nodes.Enabled {
		robotNodeQuery := &robotv1alpha1.RobotNode{}
		err := r.Get(ctx, *instance.GetRobotNodeMetadata(), robotNodeQuery)
		if err != nil && errors.IsNotFound(err) {
			instance.Status.NodeStatus = robotv1alpha1.NodeStatus{}
		} else if err != nil {
			return err
		} else {
			instance.Status.NodeStatus.Reference = robotv1alpha1.RobotDataTypeReference{
				Name:      robotNodeQuery.Name,
				Namespace: robotNodeQuery.Namespace,
			}
		}
	}

	if instance.Spec.DataExport.Topics.Enabled {
		robotTopicQuery := &robotv1alpha1.RobotTopic{}
		err := r.Get(ctx, *instance.GetRobotTopicMetadata(), robotTopicQuery)
		if err != nil && errors.IsNotFound(err) {
			instance.Status.TopicStatus = robotv1alpha1.TopicStatus{}
		} else if err != nil {
			return err
		} else {
			instance.Status.TopicStatus.Reference = robotv1alpha1.RobotDataTypeReference{
				Name:      robotTopicQuery.Name,
				Namespace: robotTopicQuery.Namespace,
			}
		}
	}

	if instance.Spec.DataExport.Services.Enabled {
		robotServiceQuery := &robotv1alpha1.RobotService{}
		err := r.Get(ctx, *instance.GetRobotServiceMetadata(), robotServiceQuery)
		if err != nil && errors.IsNotFound(err) {
			instance.Status.ServiceStatus = robotv1alpha1.ServiceStatus{}
		} else if err != nil {
			return err
		} else {
			instance.Status.ServiceStatus.Reference = robotv1alpha1.RobotDataTypeReference{
				Name:      robotServiceQuery.Name,
				Namespace: robotServiceQuery.Namespace,
			}
		}
	}

	if instance.Spec.DataExport.Actions.Enabled {
		robotActionQuery := &robotv1alpha1.RobotAction{}
		err := r.Get(ctx, *instance.GetRobotActionMetadata(), robotActionQuery)
		if err != nil && errors.IsNotFound(err) {
			instance.Status.ActionStatus = robotv1alpha1.ActionStatus{}
		} else if err != nil {
			return err
		} else {
			instance.Status.ActionStatus.Reference = robotv1alpha1.RobotDataTypeReference{
				Name:      robotActionQuery.Name,
				Namespace: robotActionQuery.Namespace,
			}
		}
	}

	return nil
}

func (r *RobotDataReconciler) reconcileUpdateInstanceStatus(ctx context.Context, instance *robotv1alpha1.RobotData) error {
	return retry.RetryOnConflict(retry.DefaultRetry, func() error {
		instanceLV := &robotv1alpha1.RobotData{}
		err := r.Get(ctx, types.NamespacedName{
			Name:      instance.Name,
			Namespace: instance.Namespace,
		}, instanceLV)

		if err == nil {
			instance.ResourceVersion = instanceLV.ResourceVersion
		}

		err1 := r.Status().Update(ctx, instance)
		return err1
	})
}

// SetupWithManager sets up the controller with the Manager.
func (r *RobotDataReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&robotv1alpha1.RobotData{}).
		Owns(&robotv1alpha1.RobotNode{}).
		Owns(&robotv1alpha1.RobotTopic{}).
		Owns(&robotv1alpha1.RobotService{}).
		Owns(&robotv1alpha1.RobotAction{}).
		Complete(r)
}
