package robotdata

import (
	"context"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robotdata/pkg/spawn"
	ctrl "sigs.k8s.io/controller-runtime"
)

func (r *RobotDataReconciler) createRobotNode(ctx context.Context, instance *robotv1alpha1.RobotData) error {
	robotNode := spawn.GetRobotNode(instance)

	err := ctrl.SetControllerReference(instance, robotNode, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotNode)
	if err != nil {
		return err
	}

	logger.Info("STATUS: RobotNode is created.")
	instance.Status.NodeStatus.Created = true

	return nil
}

func (r *RobotDataReconciler) createRobotTopic(ctx context.Context, instance *robotv1alpha1.RobotData) error {
	robotTopic := spawn.GetRobotTopic(instance)

	err := ctrl.SetControllerReference(instance, robotTopic, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotTopic)
	if err != nil {
		return err
	}

	logger.Info("STATUS: RobotTopic is created.")
	instance.Status.TopicStatus.Created = true

	return nil
}

func (r *RobotDataReconciler) createRobotService(ctx context.Context, instance *robotv1alpha1.RobotData) error {
	robotService := spawn.GetRobotService(instance)

	err := ctrl.SetControllerReference(instance, robotService, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotService)
	if err != nil {
		return err
	}

	logger.Info("STATUS: RobotService is created.")
	instance.Status.ServiceStatus.Created = true

	return nil
}

func (r *RobotDataReconciler) createRobotAction(ctx context.Context, instance *robotv1alpha1.RobotData) error {
	robotAction := spawn.GetRobotAction(instance)

	err := ctrl.SetControllerReference(instance, robotAction, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotAction)
	if err != nil {
		return err
	}

	logger.Info("STATUS: RobotAction is created.")
	instance.Status.ActionStatus.Created = true

	return nil
}
