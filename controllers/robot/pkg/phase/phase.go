package phase

import (
	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
)

func SetRobotPhase(instance *robotv1alpha1.Robot) {

	configStatus := instance.Status.ConfigStatus.Status
	cloneStatus := instance.Status.CloneStatus.Status
	buildStatus := instance.Status.BuildStatus.Status
	runtimeStatus := instance.Status.RuntimeStatus.Status
	launchStatus := instance.Status.RuntimeStatus.Status.LaunchStatus.Status

	var phase robotv1alpha1.RobotPhase

	switch configStatus.Phase {
	// robotconfig succeeded
	case robotv1alpha1.RobotConfigPhaseReady:

		switch cloneStatus.Phase {
		// robotclone succeeded
		case robotv1alpha1.RobotClonePhaseReady:

			switch buildStatus.Phase {
			// robotbuild succeeded
			case robotv1alpha1.RobotBuildPhaseReady:

				switch runtimeStatus.Phase {
				// robotruntime succeeded
				case robotv1alpha1.RobotRuntimePhaseReady:

					switch launchStatus.Phase {
					// robotlaunch succeeded
					case robotv1alpha1.RobotLaunchPhaseReady:
						phase = robotv1alpha1.RobotPhaseReady

					// robotlaunch active
					case robotv1alpha1.RobotLaunchPhaseCreatingPod:
						phase = robotv1alpha1.RobotPhaseLaunchingNodes

					// robotlaunch not started
					case "":
						phase = robotv1alpha1.RobotPhaseLaunchingNodes

					// robotlaunch failed or malfunctioned
					default:
						phase = robotv1alpha1.RobotPhaseFailedLaunching
					}

				// robotruntime active
				case robotv1alpha1.RobotRuntimePhaseCreatingRobotLaunch:
					phase = robotv1alpha1.RobotPhaseLaunchingNodes

				// robotruntime not started
				case "":
					phase = robotv1alpha1.RobotPhaseLaunchingNodes

				// robotruntime failed or malfunctioned
				default:
					phase = robotv1alpha1.RobotPhaseFailedLaunching
				}

			// robotbuild active
			case robotv1alpha1.RobotBuildPhaseCreatingConfigMap:
				phase = robotv1alpha1.RobotPhaseBuildingWorkspaces

			// robotbuild active
			case robotv1alpha1.RobotBuildPhaseBuildingRobot:
				phase = robotv1alpha1.RobotPhaseBuildingWorkspaces

			// robotbuild not started
			case "":
				phase = robotv1alpha1.RobotPhaseBuildingWorkspaces

			// robotbuild failed or malfunctioned
			default:
				phase = robotv1alpha1.RobotPhaseFailedBuilding
			}

		// robotclone active
		case robotv1alpha1.RobotClonePhaseCreatingWorkspace:
			phase = robotv1alpha1.RobotPhaseCloningRepositories

		// robotclone active
		case robotv1alpha1.RobotClonePhaseCloningRepositories:
			phase = robotv1alpha1.RobotPhaseCloningRepositories

		// robotclone not started
		case "":
			phase = robotv1alpha1.RobotPhaseCloningRepositories

		// robotclone failed or malfunctioned
		default:
			phase = robotv1alpha1.RobotPhaseFailedCloning
		}

	// robotconfig active
	case robotv1alpha1.RobotConfigPhaseConfiguringEnvironment:
		phase = robotv1alpha1.RobotPhaseConfiguringEnvironment

	// robotconfig not started
	case "":
		phase = robotv1alpha1.RobotPhaseConfiguringEnvironment

	// robotconfig failed or malfunctioned
	default:
		phase = robotv1alpha1.RobotPhaseFailedConfiguration
	}

	instance.Status.Phase = phase
}
