package spawn

import (
	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robot/pkg/helpers"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetRobotArtifact(cr *robotv1alpha1.Robot, artifacts robotv1alpha1.Artifacts) *robotv1alpha1.RobotArtifact {

	meta := cr.GetRobotArtifactMetadata()

	robotArtifact := robotv1alpha1.RobotArtifact{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Artifacts: artifacts,
		Robot:     cr.Spec.Robot,
	}

	return &robotArtifact
}

func GetRobotConfig(cr *robotv1alpha1.Robot) *robotv1alpha1.RobotConfig {

	meta := cr.GetRobotConfigMetadata()

	configSpec := robotv1alpha1.RobotConfigSpec{}

	robotConfig := robotv1alpha1.RobotConfig{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: configSpec,
	}

	return &robotConfig
}

func GetRobotData(cr *robotv1alpha1.Robot) *robotv1alpha1.RobotData {

	meta := cr.GetRobotDataMetadata()

	dataSpec := robotv1alpha1.RobotDataSpec{
		DataExport: cr.Spec.Robot.DataExport,
	}

	robotData := robotv1alpha1.RobotData{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: dataSpec,
	}

	return &robotData
}

func GetRobotClone(cr *robotv1alpha1.Robot) *robotv1alpha1.RobotClone {

	meta := cr.GetRobotCloneMetadata()

	cloneSpec := robotv1alpha1.RobotCloneSpec{}

	robotClone := robotv1alpha1.RobotClone{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: cloneSpec,
	}

	return &robotClone
}

func GetRobotBuild(cr *robotv1alpha1.Robot) *robotv1alpha1.RobotBuild {

	meta := cr.GetRobotBuildMetadata()

	buildSpec := robotv1alpha1.RobotBuildSpec{
		Steps: helpers.DeriveCommands(*cr),
	}

	robotBuild := robotv1alpha1.RobotBuild{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: buildSpec,
	}

	return &robotBuild
}

func GetRobotTools(cr *robotv1alpha1.Robot) *robotv1alpha1.RobotTools {

	meta := cr.GetRobotToolsMetadata()

	toolsSpec := robotv1alpha1.RobotToolsSpec{
		Tools: cr.Spec.Robot.Tools,
	}

	robotTools := robotv1alpha1.RobotTools{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: toolsSpec,
	}

	return &robotTools
}

func GetRobotRuntime(cr *robotv1alpha1.Robot) *robotv1alpha1.RobotRuntime {

	meta := cr.GetRobotRuntimeMetadata()

	runtimeSpec := robotv1alpha1.RobotRuntimeSpec{}

	robotRuntime := robotv1alpha1.RobotRuntime{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: runtimeSpec,
	}

	return &robotRuntime
}
