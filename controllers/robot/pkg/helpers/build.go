package helpers

import (
	"strconv"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
)

func DeriveCommands(
	cr robotv1alpha1.Robot,
) []robotv1alpha1.Step {

	rosSpec := cr.Spec.Robot

	if len(rosSpec.Workspaces) > 0 {

		stepsSlice := []robotv1alpha1.Step{}

		for wsKey, ws := range rosSpec.Workspaces {
			wsKeyStr := strconv.Itoa(wsKey)

			switch rosSpec.Workspaces[wsKey].Build {
			case robotv1alpha1.WorkspaceBuildTypeStandard:

				stdStepsSlice := []robotv1alpha1.Step{
					{
						Name: wsKeyStr + "-dependencies",
						Command: "source /opt/ros/$DISTRO/setup.bash" +
							" && cd " + rosSpec.WorkspacesPath + "/" + ws.Name +
							" && apt update && rosdep update && rosdep install --from-paths src --ignore-src -r -y",
					},
					{
						Name: wsKeyStr + "-build",
						Command: "source /opt/ros/$DISTRO/setup.bash" +
							" && cd " + rosSpec.WorkspacesPath + "/" + ws.Name +
							" && colcon build",
					},
				}

				stepsSlice = append(stepsSlice, stdStepsSlice...)

			case robotv1alpha1.WorkspaceBuildTypeCustom:

				customStepsSlice := []robotv1alpha1.Step{}

				for _, step := range rosSpec.Workspaces[wsKey].BuildSteps {

					if step.Command != "" {

						customStepsSlice = append(customStepsSlice, robotv1alpha1.Step{
							Name:    wsKeyStr + "-" + step.Name,
							Command: step.Command,
						})

					} else if step.Script != "" {
						scriptName := wsKeyStr + "-" + step.Name

						customStepsSlice = append(customStepsSlice, robotv1alpha1.Step{
							Name:   scriptName,
							Script: step.Script,
						})
					}
				}

				stepsSlice = append(stepsSlice, customStepsSlice...)

			default:
				return nil
			}
		}

		stepsSliceOrdered := GetOrderedStepsSlice(cr, stepsSlice)

		return stepsSliceOrdered
	} else {
		return nil
	}

}

func GetStepPrefix(key int) string {
	if key < 9 {
		return "0" + strconv.Itoa(key+1) + "-"
	} else {
		return strconv.Itoa(key+1) + "-"
	}
}

func GetStepsMap(steps []robotv1alpha1.Step) map[string]robotv1alpha1.Step {

	stepsMap := map[string]robotv1alpha1.Step{}

	for key, step := range steps {
		stepsMap[GetStepPrefix(key)+step.Name] = step
	}

	return stepsMap
}

func GetOrderedStepsSlice(
	cr robotv1alpha1.Robot,
	steps []robotv1alpha1.Step,
) []robotv1alpha1.Step {

	stepsSliceOrdered := []robotv1alpha1.Step{}

	for key, step := range steps {

		newStep := robotv1alpha1.Step{
			Name:    cr.Name + "-build-" + GetStepPrefix(key) + step.Name,
			Command: step.Command,
			Script:  step.Script,
		}

		if step.Script != "" {
			newStep.Command = robotv1alpha1.CustomScriptsPath + "/scripts/" + newStep.Name
		}

		stepsSliceOrdered = append(stepsSliceOrdered, newStep)

	}

	return stepsSliceOrdered
}
