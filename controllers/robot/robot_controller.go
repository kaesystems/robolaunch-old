package robot

import (
	"context"
	basicErr "errors"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robot/pkg/phase"
)

type RobotReconciler struct {
	client.Client
	Scheme   *runtime.Scheme
	Recorder record.EventRecorder
}

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robots,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robots/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robots/finalizers,verbs=update

//+kubebuilder:rbac:groups=core,resources=nodes,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotartifacts,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotconfigs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotclones,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotbuilds,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotruntimes,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotdatas,verbs=get;list;watch;create;update;patch;delete

var logger logr.Logger

func (r *RobotReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger = log.FromContext(ctx)

	instance, err := r.reconcileGetInstance(ctx, req.NamespacedName)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	err = r.reconcileGetClusterLabel(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateNodeInfo(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	phase.SetRobotPhase(instance)

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckOwnedResources(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	phase.SetRobotPhase(instance)

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *RobotReconciler) reconcileGetInstance(ctx context.Context, meta types.NamespacedName) (*robotv1alpha1.Robot, error) {
	instance := &robotv1alpha1.Robot{}
	err := r.Get(ctx, meta, instance)
	if err != nil {
		return &robotv1alpha1.Robot{}, err
	}

	return instance, nil
}

func (r *RobotReconciler) reconcileCheckStatus(ctx context.Context, instance *robotv1alpha1.Robot) error {

	switch instance.Status.Cluster {
	case "":
		return basicErr.New("cannot get cluster name")
	}

	switch instance.Status.ArtifactStatus.Created {
	case true:

		switch instance.Status.ConfigStatus.Created {
		case true:

			switch instance.Status.ConfigStatus.Status.Phase {
			case robotv1alpha1.RobotConfigPhaseReady:

				switch instance.Status.CloneStatus.Created {
				case true:

					switch instance.Status.CloneStatus.Status.Phase {
					case robotv1alpha1.RobotClonePhaseReady:

						switch instance.Status.BuildStatus.Created ||
							instance.Spec.Robot.State == robotv1alpha1.RobotStatePassive {
						case true:

							switch instance.Status.BuildStatus.Status.Phase {
							case robotv1alpha1.RobotBuildPhaseReady:

								switch instance.Status.RuntimeStatus.Created ||
									instance.Spec.Robot.State == robotv1alpha1.RobotStatePassive ||
									instance.Spec.Robot.State == robotv1alpha1.RobotStateBuilt {
								case true:

								case false:
									err := r.createRobotRuntime(ctx, instance)
									if err != nil {
										return err
									}
								}
							}

						case false:
							err := r.createRobotBuild(ctx, instance)
							if err != nil {
								return err
							}
						}

						switch instance.Status.DataStatus.Created {
						case true:

							// check if robotdata phase is ready

							switch instance.Status.ToolsStatus.Created {
							case true:

							case false:
								err := r.createRobotTools(ctx, instance)
								if err != nil {
									return err
								}
							}

						case false:
							err := r.createRobotData(ctx, instance)
							if err != nil {
								return err
							}
						}

					}

				case false:
					err := r.createRobotClone(ctx, instance)
					if err != nil {
						return err
					}
				}
			}

		case false:
			err := r.createRobotConfig(ctx, instance)
			if err != nil {
				return err
			}
		}

	case false:
		err := r.createRobotArtifact(ctx, instance)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *RobotReconciler) reconcileCheckOwnedResources(ctx context.Context, instance *robotv1alpha1.Robot) error {

	err := r.checkRobotArtifact(ctx, instance)
	if err != nil {
		return err
	}

	err = r.checkRobotConfig(ctx, instance)
	if err != nil {
		return err
	}

	err = r.checkRobotClone(ctx, instance)
	if err != nil {
		return err
	}

	err = r.checkRobotBuild(ctx, instance)
	if err != nil {
		return err
	}

	err = r.checkRobotRuntime(ctx, instance)
	if err != nil {
		return err
	}

	err = r.checkRobotTools(ctx, instance)
	if err != nil {
		return err
	}

	err = r.checkRobotData(ctx, instance)
	if err != nil {
		return err
	}

	return nil
}

func (r *RobotReconciler) reconcileUpdateInstanceStatus(ctx context.Context, instance *robotv1alpha1.Robot) error {
	return retry.RetryOnConflict(retry.DefaultRetry, func() error {
		instanceLV := &robotv1alpha1.Robot{}
		err := r.Get(ctx, types.NamespacedName{
			Name:      instance.Name,
			Namespace: instance.Namespace,
		}, instanceLV)

		if err == nil {
			instance.ResourceVersion = instanceLV.ResourceVersion
		}

		err1 := r.Status().Update(ctx, instance)
		return err1
	})
}

func (r *RobotReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&robotv1alpha1.Robot{}).
		Owns(&robotv1alpha1.RobotArtifact{}).
		Owns(&robotv1alpha1.RobotConfig{}).
		Owns(&robotv1alpha1.RobotClone{}).
		Owns(&robotv1alpha1.RobotBuild{}).
		Owns(&robotv1alpha1.RobotRuntime{}).
		Owns(&robotv1alpha1.RobotData{}).
		Complete(r)
}
