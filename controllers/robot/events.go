package robot

type robotEventType string

const (
	robotEventTypeNormal  robotEventType = "Normal"
	robotEventTypeFailure robotEventType = "Failure"
)

type robotEventReason string

const (
	robotEventReasonCreated       robotEventReason = "Created"
	robotEventReasonUpdated       robotEventReason = "Updated"
	robotEventReasonDeleted       robotEventReason = "Deleted"
	robotEventReasonMalfunctioned robotEventReason = "Malfunctioned"
)
