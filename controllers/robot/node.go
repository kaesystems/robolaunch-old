package robot

import (
	"context"
	basicErr "errors"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func (r *RobotReconciler) reconcileGetClusterLabel(ctx context.Context, instance *robotv1alpha1.Robot) error {
	requirement, err := labels.NewRequirement(instance.Spec.Robot.ClusterSelector, selection.Exists, []string{})
	if err != nil {
		return err
	}

	nodeSelector := labels.NewSelector().Add(*requirement)

	nodes := &corev1.NodeList{}
	err = r.List(ctx, nodes, &client.ListOptions{
		LabelSelector: nodeSelector,
	})
	if err != nil {
		return err
	}

	if len(nodes.Items) == 0 {
		return basicErr.New("no nodes are listed with cluster selector")
	}

	instance.Status.Cluster = nodes.Items[0].Labels[instance.Spec.Robot.ClusterSelector]

	return nil
}

func (r *RobotReconciler) reconcileUpdateNodeInfo(ctx context.Context, instance *robotv1alpha1.Robot) error {

	requirements := []labels.Requirement{}
	requirementsMap := instance.Spec.Robot.NodeSelector
	for k, v := range requirementsMap {
		newReq, err := labels.NewRequirement(k, selection.In, []string{v})
		if err != nil {
			return err
		}
		requirements = append(requirements, *newReq)
	}

	nodeSelector := labels.NewSelector().Add(requirements...)

	nodes := &corev1.NodeList{}
	err := r.List(ctx, nodes, &client.ListOptions{
		LabelSelector: nodeSelector,
	})
	if err != nil {
		return err
	}

	if len(nodes.Items) == 0 {
		return basicErr.New("no nodes are listed with node selector")
	} else if len(nodes.Items) > 1 {
		return basicErr.New("multiple nodes are listed, no specific target")
	}

	node := nodes.Items[0]
	instance.Status.NodeInfo.Name = node.Name

	if _, ok := node.Status.Allocatable["nvidia.com/gpu"]; ok {
		instance.Status.NodeInfo.HasGPU = true
	} else {
		instance.Status.NodeInfo.HasGPU = false
	}

	return nil
}
