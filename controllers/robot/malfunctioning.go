package robot

import (
	"context"
	"fmt"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func (r *RobotReconciler) deleteMalfunctionedRobotConfig(
	ctx context.Context,
	instance *robotv1alpha1.Robot,
	robotConfig *robotv1alpha1.RobotConfig,
) error {

	additionalEventMessage := "[Malfunctioned RobotConfig]"

	robotRuntimeQuery := &robotv1alpha1.RobotRuntime{}
	err := r.Get(ctx, *instance.GetRobotRuntimeMetadata(), robotRuntimeQuery)
	if err != nil && errors.IsNotFound(err) {
	} else if err != nil {
		return err
	} else {
		err = r.Delete(ctx, robotRuntimeQuery)
		if err != nil {
			return err
		}

		r.Recorder.Event(
			instance,
			string(robotEventTypeNormal),
			string(robotEventReasonMalfunctioned),
			fmt.Sprintf(
				"Deleted RobotRuntime %s/%s - %s",
				instance.GetRobotRuntimeMetadata().Namespace,
				instance.GetRobotRuntimeMetadata().Name,
				additionalEventMessage,
			),
		)
	}

	var gracePeriod int64 = 5
	var propagationPolicy metav1.DeletionPropagation = metav1.DeletePropagationForeground

	robotBuildQuery := &robotv1alpha1.RobotBuild{}
	err = r.Get(ctx, *instance.GetRobotBuildMetadata(), robotBuildQuery)
	if err != nil && errors.IsNotFound(err) {
	} else if err != nil {
		return err
	} else {
		err = r.Delete(ctx, robotBuildQuery, &client.DeleteOptions{
			GracePeriodSeconds: &gracePeriod,
			PropagationPolicy:  &propagationPolicy,
		})
		if err != nil {
			return err
		}

		r.Recorder.Event(
			instance,
			string(robotEventTypeNormal),
			string(robotEventReasonMalfunctioned),
			fmt.Sprintf(
				"Deleted RobotBuild %s/%s - %s",
				instance.GetRobotBuildMetadata().Namespace,
				instance.GetRobotBuildMetadata().Name,
				additionalEventMessage,
			),
		)
	}

	err = r.Delete(ctx, robotConfig, &client.DeleteOptions{
		GracePeriodSeconds: &gracePeriod,
		PropagationPolicy:  &propagationPolicy,
	})
	if err != nil {
		return err
	}

	r.Recorder.Event(
		instance,
		string(robotEventTypeNormal),
		string(robotEventReasonMalfunctioned),
		fmt.Sprintf(
			"Deleted RobotConfig %s/%s - %s",
			instance.GetRobotConfigMetadata().Namespace,
			instance.GetRobotConfigMetadata().Name,
			additionalEventMessage,
		),
	)

	return nil
}

func (r *RobotReconciler) deleteMalfunctionedRobotBuild(
	ctx context.Context,
	instance *robotv1alpha1.Robot,
	robotBuild *robotv1alpha1.RobotBuild,
) error {

	additionalEventMessage := "[Malfunctioned RobotBuild]"

	robotRuntimeQuery := &robotv1alpha1.RobotRuntime{}
	err := r.Get(ctx, *instance.GetRobotRuntimeMetadata(), robotRuntimeQuery)
	if err != nil && errors.IsNotFound(err) {
	} else if err != nil {
		return err
	} else {
		err = r.Delete(ctx, robotRuntimeQuery)
		if err != nil {
			return err
		}

		r.Recorder.Event(
			instance,
			string(robotEventTypeNormal),
			string(robotEventReasonMalfunctioned),
			fmt.Sprintf(
				"Deleted RobotRuntime %s/%s - %s",
				instance.GetRobotRuntimeMetadata().Namespace,
				instance.GetRobotRuntimeMetadata().Name,
				additionalEventMessage,
			),
		)
	}

	var gracePeriod int64 = 5
	var propagationPolicy metav1.DeletionPropagation = metav1.DeletePropagationForeground

	err = r.Delete(ctx, robotBuild, &client.DeleteOptions{
		GracePeriodSeconds: &gracePeriod,
		PropagationPolicy:  &propagationPolicy,
	})
	if err != nil {
		return err
	}

	r.Recorder.Event(
		instance,
		string(robotEventTypeNormal),
		string(robotEventReasonMalfunctioned),
		fmt.Sprintf(
			"Deleted RobotBuild %s/%s - %s",
			instance.GetRobotBuildMetadata().Namespace,
			instance.GetRobotBuildMetadata().Name,
			additionalEventMessage,
		),
	)

	return nil
}

func (r *RobotReconciler) deleteMalfunctionedRobotRuntime(
	ctx context.Context,
	instance *robotv1alpha1.Robot,
	robotRuntime *robotv1alpha1.RobotRuntime,
) error {

	additionalEventMessage := "[Malfunctioned RobotRuntime]"

	err := r.Delete(ctx, robotRuntime)
	if err != nil {
		return err
	}

	r.Recorder.Event(
		instance,
		string(robotEventTypeNormal),
		string(robotEventReasonMalfunctioned),
		fmt.Sprintf(
			"Deleted RobotRuntime %s/%s - %s",
			instance.GetRobotRuntimeMetadata().Namespace,
			instance.GetRobotRuntimeMetadata().Name,
			additionalEventMessage,
		),
	)

	return nil
}
