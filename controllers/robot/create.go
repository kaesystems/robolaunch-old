package robot

import (
	"context"
	"fmt"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robot/pkg/spawn"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
)

func (r *RobotReconciler) createRobotArtifact(ctx context.Context, instance *robotv1alpha1.Robot) error {
	referenceArtifact := &robotv1alpha1.RobotArtifact{}
	err := r.Get(ctx, types.NamespacedName{
		Namespace: instance.Spec.ArtifactReference.Namespace,
		Name:      instance.Spec.ArtifactReference.Name,
	}, referenceArtifact)
	if err != nil && errors.IsNotFound(err) {
		referenceArtifact = &robotv1alpha1.RobotArtifact{}
	} else if err != nil {
		return err
	}

	robotArtifact := spawn.GetRobotArtifact(instance, referenceArtifact.Artifacts)

	err = ctrl.SetControllerReference(instance, robotArtifact, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotArtifact)
	if err != nil {
		return err
	}

	logger.Info("STATUS: RobotArtifact is created.")
	instance.Status.ArtifactStatus.Created = true

	r.Recorder.Event(
		instance,
		string(robotEventTypeNormal),
		string(robotEventReasonCreated),
		fmt.Sprintf(
			"Created RobotArtifact %s/%s",
			instance.GetRobotArtifactMetadata().Namespace,
			instance.GetRobotArtifactMetadata().Name,
		),
	)

	return nil
}

func (r *RobotReconciler) createRobotConfig(ctx context.Context, instance *robotv1alpha1.Robot) error {
	robotConfig := spawn.GetRobotConfig(instance)

	err := ctrl.SetControllerReference(instance, robotConfig, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotConfig)
	if err != nil {
		return err
	}

	logger.Info("STATUS: RobotConfig is created.")
	instance.Status.ConfigStatus.Created = true

	r.Recorder.Event(
		instance,
		string(robotEventTypeNormal),
		string(robotEventReasonCreated),
		fmt.Sprintf(
			"Created RobotConfig %s/%s",
			instance.GetRobotConfigMetadata().Namespace,
			instance.GetRobotConfigMetadata().Name,
		),
	)

	return nil
}

func (r *RobotReconciler) createRobotClone(ctx context.Context, instance *robotv1alpha1.Robot) error {
	robotClone := spawn.GetRobotClone(instance)

	err := ctrl.SetControllerReference(instance, robotClone, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotClone)
	if err != nil {
		return err
	}

	logger.Info("STATUS: RobotClone is created.")
	instance.Status.CloneStatus.Created = true

	r.Recorder.Event(
		instance,
		string(robotEventTypeNormal),
		string(robotEventReasonCreated),
		fmt.Sprintf(
			"Created RobotClone %s/%s",
			instance.GetRobotCloneMetadata().Namespace,
			instance.GetRobotCloneMetadata().Name,
		),
	)

	return nil
}

func (r *RobotReconciler) createRobotTools(ctx context.Context, instance *robotv1alpha1.Robot) error {
	robotTools := spawn.GetRobotTools(instance)

	err := ctrl.SetControllerReference(instance, robotTools, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotTools)
	if err != nil {
		return err
	}

	logger.Info("STATUS: RobotTools is created.")
	instance.Status.ToolsStatus.Created = true

	r.Recorder.Event(
		instance,
		string(robotEventTypeNormal),
		string(robotEventReasonCreated),
		fmt.Sprintf(
			"Created RobotTools %s/%s",
			instance.GetRobotToolsMetadata().Namespace,
			instance.GetRobotToolsMetadata().Name,
		),
	)

	return nil
}

func (r *RobotReconciler) createRobotBuild(ctx context.Context, instance *robotv1alpha1.Robot) error {
	robotBuild := spawn.GetRobotBuild(instance)

	err := ctrl.SetControllerReference(instance, robotBuild, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotBuild)
	if err != nil {
		return err
	}

	logger.Info("STATUS: RobotBuild is created.")
	instance.Status.BuildStatus.Created = true

	r.Recorder.Event(
		instance,
		string(robotEventTypeNormal),
		string(robotEventReasonCreated),
		fmt.Sprintf(
			"Created RobotBuild %s/%s",
			instance.GetRobotBuildMetadata().Namespace,
			instance.GetRobotBuildMetadata().Name,
		),
	)

	return nil
}

func (r *RobotReconciler) createRobotRuntime(ctx context.Context, instance *robotv1alpha1.Robot) error {
	robotRuntime := spawn.GetRobotRuntime(instance)

	err := ctrl.SetControllerReference(instance, robotRuntime, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotRuntime)
	if err != nil {
		return err
	}

	logger.Info("STATUS: RobotRuntime is created.")
	instance.Status.RuntimeStatus.Created = true

	r.Recorder.Event(
		instance,
		string(robotEventTypeNormal),
		string(robotEventReasonCreated),
		fmt.Sprintf(
			"Created RobotRuntime %s/%s",
			instance.GetRobotRuntimeMetadata().Namespace,
			instance.GetRobotRuntimeMetadata().Name,
		),
	)

	return nil
}

func (r *RobotReconciler) createRobotData(ctx context.Context, instance *robotv1alpha1.Robot) error {
	robotData := spawn.GetRobotData(instance)

	err := ctrl.SetControllerReference(instance, robotData, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotData)
	if err != nil {
		return err
	}

	logger.Info("STATUS: RobotData is created.")
	instance.Status.DataStatus.Created = true

	r.Recorder.Event(
		instance,
		string(robotEventTypeNormal),
		string(robotEventReasonCreated),
		fmt.Sprintf(
			"Created RobotData %s/%s",
			instance.GetRobotDataMetadata().Namespace,
			instance.GetRobotDataMetadata().Name,
		),
	)

	return nil
}
