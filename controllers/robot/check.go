package robot

import (
	"context"
	"fmt"
	"reflect"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
)

func (r *RobotReconciler) checkRobotArtifact(ctx context.Context, instance *robotv1alpha1.Robot) error {
	robotArtifactQuery := &robotv1alpha1.RobotArtifact{}
	err := r.Get(ctx, *instance.GetRobotArtifactMetadata(), robotArtifactQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.ArtifactStatus = robotv1alpha1.ArtifactStatus{}
	} else if err != nil {
		return err
	}
	return nil
}

func (r *RobotReconciler) checkRobotConfig(ctx context.Context, instance *robotv1alpha1.Robot) error {
	robotConfigQuery := &robotv1alpha1.RobotConfig{}
	err := r.Get(ctx, *instance.GetRobotConfigMetadata(), robotConfigQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.ConfigStatus = robotv1alpha1.ConfigStatus{}
	} else if err != nil {
		return err
	} else {

		// if !reflect.DeepEqual(instance.Spec.Robot, robotConfigQuery.Spec.Robot) {
		// 	robotConfigQuery.Spec.Robot = instance.Spec.Robot
		// 	err = r.Update(ctx, robotConfigQuery)
		// 	if err != nil {
		// 		return err
		// 	}

		// 	r.Recorder.Event(
		// 		instance,
		// 		string(robotEventTypeNormal),
		// 		string(robotEventReasonUpdated),
		// 		fmt.Sprintf(
		// 			"Updated RobotConfig %s/%s",
		// 			instance.GetRobotConfigMetadata().Namespace,
		// 			instance.GetRobotConfigMetadata().Name,
		// 		),
		// 	)
		// }

		if robotConfigQuery.Status.Phase == robotv1alpha1.RobotConfigPhaseMalfunctioned {
			err := r.deleteMalfunctionedRobotConfig(ctx, instance, robotConfigQuery)
			if err != nil {
				return err
			}
		}

		instance.Status.ConfigStatus.Status = robotConfigQuery.Status
	}

	return nil
}

func (r *RobotReconciler) checkRobotClone(ctx context.Context, instance *robotv1alpha1.Robot) error {
	robotCloneQuery := &robotv1alpha1.RobotClone{}
	err := r.Get(ctx, *instance.GetRobotCloneMetadata(), robotCloneQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.CloneStatus = robotv1alpha1.CloneStatus{}
	} else if err != nil {
		return err
	} else {
		// if !reflect.DeepEqual(instance.Spec.Robot, robotCloneQuery.Spec.Robot) {
		// 	robotCloneQuery.Spec.Robot = instance.Spec.Robot
		// 	err = r.Update(ctx, robotCloneQuery)
		// 	if err != nil {
		// 		return err
		// 	}

		// 	r.Recorder.Event(
		// 		instance,
		// 		string(robotEventTypeNormal),
		// 		string(robotEventReasonUpdated),
		// 		fmt.Sprintf(
		// 			"Updated RobotClone %s/%s",
		// 			instance.GetRobotCloneMetadata().Namespace,
		// 			instance.GetRobotCloneMetadata().Name,
		// 		),
		// 	)
		// }
		instance.Status.CloneStatus.Status = robotCloneQuery.Status
	}

	return nil
}

func (r *RobotReconciler) checkRobotBuild(ctx context.Context, instance *robotv1alpha1.Robot) error {

	switch instance.Spec.Robot.State {
	case robotv1alpha1.RobotStateLaunched, robotv1alpha1.RobotStateBuilt:
		robotBuildQuery := &robotv1alpha1.RobotBuild{}
		err := r.Get(ctx, *instance.GetRobotBuildMetadata(), robotBuildQuery)
		if err != nil && errors.IsNotFound(err) {
			instance.Status.BuildStatus = robotv1alpha1.BuildStatus{}
		} else if err != nil {
			return err
		} else {

			// if !reflect.DeepEqual(instance.Spec.Robot, robotBuildQuery.Spec.Robot) {
			// 	robotBuildQuery.Spec.Robot = instance.Spec.Robot
			// 	err = r.Update(ctx, robotBuildQuery)
			// 	if err != nil {
			// 		return err
			// 	}

			// 	r.Recorder.Event(
			// 		instance,
			// 		string(robotEventTypeNormal),
			// 		string(robotEventReasonUpdated),
			// 		fmt.Sprintf(
			// 			"Updated RobotBuild %s/%s",
			// 			instance.GetRobotBuildMetadata().Namespace,
			// 			instance.GetRobotBuildMetadata().Name,
			// 		),
			// 	)
			// }

			if robotBuildQuery.Status.Phase == robotv1alpha1.RobotBuildPhaseMalfunctioned {
				err := r.deleteMalfunctionedRobotBuild(ctx, instance, robotBuildQuery)
				if err != nil {
					return err
				}
			}

			instance.Status.BuildStatus.Status = robotBuildQuery.Status
		}

	case robotv1alpha1.RobotStatePassive:
		robotBuildQuery := &robotv1alpha1.RobotBuild{}
		err := r.Get(ctx, *instance.GetRobotBuildMetadata(), robotBuildQuery)
		if err != nil && errors.IsNotFound(err) {
			instance.Status.BuildStatus = robotv1alpha1.BuildStatus{}
		} else if err != nil {
			return err
		} else {
			err = r.Delete(ctx, robotBuildQuery)
			if err != nil {
				return err
			}

			r.Recorder.Event(
				instance,
				string(robotEventTypeNormal),
				string(robotEventReasonDeleted),
				fmt.Sprintf(
					"Deleted RobotBuild %s/%s",
					instance.GetRobotBuildMetadata().Namespace,
					instance.GetRobotBuildMetadata().Name,
				),
			)
		}
	}

	return nil
}

func (r *RobotReconciler) checkRobotRuntime(ctx context.Context, instance *robotv1alpha1.Robot) error {

	switch instance.Spec.Robot.State {
	case robotv1alpha1.RobotStateLaunched:

		robotRuntimeQuery := &robotv1alpha1.RobotRuntime{}
		err := r.Get(ctx, *instance.GetRobotRuntimeMetadata(), robotRuntimeQuery)
		if err != nil && errors.IsNotFound(err) {
			instance.Status.RuntimeStatus = robotv1alpha1.RuntimeStatus{}
		} else if err != nil {
			return err
		} else {

			// if !reflect.DeepEqual(instance.Spec.Robot, robotRuntimeQuery.Spec.Robot) {
			// 	robotRuntimeQuery.Spec.Robot = instance.Spec.Robot
			// 	err = r.Update(ctx, robotRuntimeQuery)
			// 	if err != nil {
			// 		return err
			// 	}

			// 	r.Recorder.Event(
			// 		instance,
			// 		string(robotEventTypeNormal),
			// 		string(robotEventReasonUpdated),
			// 		fmt.Sprintf(
			// 			"Updated RobotRuntime %s/%s",
			// 			instance.GetRobotRuntimeMetadata().Namespace,
			// 			instance.GetRobotRuntimeMetadata().Name,
			// 		),
			// 	)
			// }

			if robotRuntimeQuery.Status.Phase == robotv1alpha1.RobotRuntimePhaseMalfunctioned {
				err := r.deleteMalfunctionedRobotRuntime(ctx, instance, robotRuntimeQuery)
				if err != nil {
					return err
				}
			}

			instance.Status.RuntimeStatus.Status = robotRuntimeQuery.Status
		}

	case robotv1alpha1.RobotStateBuilt, robotv1alpha1.RobotStatePassive:

		robotRuntimeQuery := &robotv1alpha1.RobotRuntime{}
		err := r.Get(ctx, *instance.GetRobotRuntimeMetadata(), robotRuntimeQuery)
		if err != nil && errors.IsNotFound(err) {
			instance.Status.RuntimeStatus = robotv1alpha1.RuntimeStatus{}
		} else if err != nil {
			return err
		} else {
			err = r.Delete(ctx, robotRuntimeQuery)
			if err != nil {
				return err
			}

			r.Recorder.Event(
				instance,
				string(robotEventTypeNormal),
				string(robotEventReasonDeleted),
				fmt.Sprintf(
					"Deleted RobotRuntime %s/%s",
					instance.GetRobotRuntimeMetadata().Namespace,
					instance.GetRobotRuntimeMetadata().Name,
				),
			)
		}

	}

	return nil
}

func (r *RobotReconciler) checkRobotTools(ctx context.Context, instance *robotv1alpha1.Robot) error {

	robotToolsQuery := &robotv1alpha1.RobotTools{}
	err := r.Get(ctx, *instance.GetRobotToolsMetadata(), robotToolsQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.ToolsStatus = robotv1alpha1.ToolsStatus{}
	} else if err != nil {
		return err
	} else {
		if !reflect.DeepEqual(instance.Spec.Robot.Tools, robotToolsQuery.Spec.Tools) {
			robotToolsQuery.Spec.Tools = instance.Spec.Robot.Tools
			err = r.Update(ctx, robotToolsQuery)
			if err != nil {
				return err
			}

			r.Recorder.Event(
				instance,
				string(robotEventTypeNormal),
				string(robotEventReasonUpdated),
				fmt.Sprintf(
					"Updated RobotTools %s/%s",
					instance.GetRobotToolsMetadata().Namespace,
					instance.GetRobotToolsMetadata().Name,
				),
			)
		}
		instance.Status.ToolsStatus.Status = robotToolsQuery.Status
	}

	return nil
}

func (r *RobotReconciler) checkRobotData(ctx context.Context, instance *robotv1alpha1.Robot) error {

	robotDataQuery := &robotv1alpha1.RobotData{}
	err := r.Get(ctx, *instance.GetRobotDataMetadata(), robotDataQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.DataStatus = robotv1alpha1.DataStatus{}
	} else if err != nil {
		return err
	} else {
		instance.Status.DataStatus.Created = true
	}

	return nil
}
