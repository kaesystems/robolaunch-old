package robotlaunch

import (
	"context"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robotlaunch/pkg/spawn"
	ctrl "sigs.k8s.io/controller-runtime"
)

func (r *RobotLaunchReconciler) createLaunchPod(
	ctx context.Context,
	instance *robotv1alpha1.RobotLaunch,
) error {

	instance.Status.Phase = robotv1alpha1.RobotLaunchPhaseCreatingPod

	robotConfig, err := r.reconcileGetRobotConfig(ctx, instance)
	if err != nil {
		return err
	}

	robotClone, err := r.reconcileGetRobotClone(ctx, instance)
	if err != nil {
		return err
	}

	robotBuild, err := r.reconcileGetRobotBuild(ctx, instance)
	if err != nil {
		return err
	}

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	robot, err := r.reconcileGetRobot(ctx, instance)
	if err != nil {
		return err
	}

	rosPod, err := spawn.GetLaunchPod(
		instance,
		robot,
		robotArtifact,
		robotConfig,
		robotClone,
		robotBuild,
	)
	if err != nil {
		instance.Status.Phase = robotv1alpha1.RobotLaunchPhaseNodesNotFound
		logger.Info("STATUS: Nodes not found.")
		return nil
	}

	err = ctrl.SetControllerReference(instance, rosPod, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, rosPod)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Launch pod is created.")
	instance.Status.LaunchPodStatus.Created = true

	return nil
}
