package robotlaunch

import (
	"context"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/pkg/reconciler"
	"github.com/robolaunch/robot-operator/controllers/robotlaunch/pkg/manage"
)

type RobotLaunchReconciler struct {
	client.Client
	RESTClient rest.Interface
	RESTConfig *rest.Config
	Scheme     *runtime.Scheme
}

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotlaunches,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotlaunches/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotlaunches/finalizers,verbs=update

//+kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=pods/exec,verbs=get;list;watch;create;update;patch;delete

var logger logr.Logger
var defaultReturnResult ctrl.Result

func (r *RobotLaunchReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger = log.FromContext(ctx)
	defaultReturnResult = ctrl.Result{}

	instance, err := r.reconcileGetInstance(ctx, req.NamespacedName)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckOwnedResources(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *RobotLaunchReconciler) reconcileGetInstance(ctx context.Context, meta types.NamespacedName) (*robotv1alpha1.RobotLaunch, error) {
	robotlaunch := &robotv1alpha1.RobotLaunch{}
	err := r.Get(ctx, meta, robotlaunch)
	if err != nil {
		return &robotv1alpha1.RobotLaunch{}, err
	}
	return robotlaunch, nil
}

func (r *RobotLaunchReconciler) reconcileCheckStatus(ctx context.Context, instance *robotv1alpha1.RobotLaunch) error {
	if instance.Status.LastLaunchTimestamp != instance.Spec.LastLaunchTimestamp {

		robot, err := r.reconcileGetRobot(ctx, instance)
		if err != nil {
			return err
		}

		robotTools, err := r.reconcileGetRobotTools(ctx, instance)
		if err != nil {
			return err
		}

		robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
		if err != nil {
			return err
		}

		response, err := manage.InspectROSNodes(
			instance,
			*robot,
			robotArtifact.Artifacts,
			robotTools.GetTrackerPodMetadata(),
			"tracker",
			r.RESTClient,
			r.RESTConfig,
			r.Scheme,
		)
		if err != nil {
			logger.Info("RESOURCE: Cannot get node inspection response.")
			reconciler.Requeue(&defaultReturnResult)
		} else {
			if response.Status == "success" {
				instance.Status.NodeRequest.Successful = true
				instance.Status.NodeRequest.Nodes = response.Nodes
				instance.Status.LastLaunchTimestamp = instance.Spec.LastLaunchTimestamp
			} else {
				reconciler.Requeue(&defaultReturnResult)
			}
		}

	} else {

		if instance.Status.NodeRequest.Successful {
			switch instance.Status.LaunchPodStatus.Created {
			case true:

				switch instance.Status.LaunchPodStatus.Phase {
				case corev1.PodRunning:
					instance.Status.Phase = robotv1alpha1.RobotLaunchPhaseReady
				}

			case false:

				err := r.createLaunchPod(ctx, instance)
				if err != nil {
					return err
				}

			}
		} else {
			reconciler.Requeue(&defaultReturnResult)
		}

	}

	return nil
}

func (r *RobotLaunchReconciler) reconcileCheckOwnedResources(ctx context.Context, instance *robotv1alpha1.RobotLaunch) error {

	launchPodQuery := &corev1.Pod{}

	err := r.Get(ctx, *instance.GetLaunchPodMetadata(), launchPodQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.LaunchPodStatus = robotv1alpha1.LaunchPodStatus{}
	} else if err != nil {
		return err
	} else {
		instance.Status.LaunchPodStatus.Created = true
		instance.Status.LaunchPodStatus.IP = launchPodQuery.Status.PodIP
		instance.Status.LaunchPodStatus.Phase = launchPodQuery.Status.Phase
		manage.UpdateContainerStates(instance, *launchPodQuery)
	}

	return nil
}

func (r *RobotLaunchReconciler) reconcileGetOwner(ctx context.Context, instance *robotv1alpha1.RobotLaunch) (*robotv1alpha1.RobotRuntime, error) {
	robotruntime := &robotv1alpha1.RobotRuntime{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robotruntime)
	if err != nil {
		return nil, err
	}

	return robotruntime, nil
}

func (r *RobotLaunchReconciler) reconcileUpdateInstanceStatus(ctx context.Context, instance *robotv1alpha1.RobotLaunch) error {
	return retry.RetryOnConflict(retry.DefaultRetry, func() error {
		instanceLV := &robotv1alpha1.RobotLaunch{}
		err := r.Get(ctx, types.NamespacedName{
			Name:      instance.Name,
			Namespace: instance.Namespace,
		}, instanceLV)

		if err == nil {
			instance.ResourceVersion = instanceLV.ResourceVersion
		}

		err1 := r.Status().Update(ctx, instance)
		return err1
	})
}

func (r *RobotLaunchReconciler) reconcileGetRobot(ctx context.Context, instance *robotv1alpha1.RobotLaunch) (*robotv1alpha1.Robot, error) {
	robotruntime, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return nil, err
	}

	robot := &robotv1alpha1.Robot{}
	err = r.Get(ctx, *robotruntime.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	return robot, nil
}

func (r *RobotLaunchReconciler) reconcileGetArtifact(ctx context.Context, instance *robotv1alpha1.RobotLaunch) (*robotv1alpha1.RobotArtifact, error) {
	robot, err := r.reconcileGetRobot(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotartifact := &robotv1alpha1.RobotArtifact{}
	err = r.Get(ctx, types.NamespacedName{
		Namespace: robot.GetRobotArtifactMetadata().Namespace,
		Name:      robot.GetRobotArtifactMetadata().Name,
	}, robotartifact)
	if err != nil {
		return nil, err
	}

	return robotartifact, nil
}

func (r *RobotLaunchReconciler) reconcileGetRobotConfig(ctx context.Context, instance *robotv1alpha1.RobotLaunch) (*robotv1alpha1.RobotConfig, error) {

	robot, err := r.reconcileGetRobot(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotconfig := &robotv1alpha1.RobotConfig{}
	err = r.Get(ctx, *robot.GetRobotConfigMetadata(), robotconfig)
	if err != nil {
		return nil, err
	}

	return robotconfig, nil
}

func (r *RobotLaunchReconciler) reconcileGetRobotTools(ctx context.Context, instance *robotv1alpha1.RobotLaunch) (*robotv1alpha1.RobotTools, error) {

	robot, err := r.reconcileGetRobot(ctx, instance)
	if err != nil {
		return nil, err
	}

	robottools := &robotv1alpha1.RobotTools{}
	err = r.Get(ctx, *robot.GetRobotToolsMetadata(), robottools)
	if err != nil {
		return nil, err
	}

	return robottools, nil
}

func (r *RobotLaunchReconciler) reconcileGetRobotClone(ctx context.Context, instance *robotv1alpha1.RobotLaunch) (*robotv1alpha1.RobotClone, error) {

	robot, err := r.reconcileGetRobot(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotclone := &robotv1alpha1.RobotClone{}
	err = r.Get(ctx, *robot.GetRobotCloneMetadata(), robotclone)
	if err != nil {
		return nil, err
	}

	return robotclone, nil
}

func (r *RobotLaunchReconciler) reconcileGetRobotBuild(ctx context.Context, instance *robotv1alpha1.RobotLaunch) (*robotv1alpha1.RobotBuild, error) {

	robot, err := r.reconcileGetRobot(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotbuild := &robotv1alpha1.RobotBuild{}
	err = r.Get(ctx, *robot.GetRobotBuildMetadata(), robotbuild)
	if err != nil {
		return nil, err
	}

	return robotbuild, nil
}

func (r *RobotLaunchReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&robotv1alpha1.RobotLaunch{}).
		Owns(&corev1.Pod{}).
		Complete(r)
}
