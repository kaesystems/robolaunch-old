package spawn

import (
	"errors"
	"path/filepath"
	"strconv"
	"strings"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/pkg/helpers"
	"github.com/robolaunch/robot-operator/controllers/pkg/volume"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetLaunchPod(
	cr *robotv1alpha1.RobotLaunch,
	robot *robotv1alpha1.Robot,
	robotArtifact *robotv1alpha1.RobotArtifact,
	robotConfig *robotv1alpha1.RobotConfig,
	robotClone *robotv1alpha1.RobotClone,
	robotBuild *robotv1alpha1.RobotBuild,
) (*corev1.Pod, error) {

	artifacts := robotArtifact.Artifacts
	robotDef := robot.Spec.Robot

	shareProcessNamespace := false

	// resourceLimits := corev1.ResourceRequirements{
	// 	Limits: corev1.ResourceList{
	// 		"cpu":    resource.MustParse(cr.Spec.Robot.Resources.CPUPerContainer),
	// 		"memory": resource.MustParse(cr.Spec.Robot.Resources.MemoryPerContainer),
	// 	},
	// }

	containers := []corev1.Container{}

	// derive configuration for every ros node
	for key, node := range cr.Status.NodeRequest.Nodes {

		if robotDef.Mode == robotv1alpha1.RobotModeHybrid && !node.Activated {
			continue
		}

		runCommand := robotv1alpha1.GenerateRunCommandAsEnv(
			*robot,
			node,
		)

		prelaunchCommand := robotv1alpha1.GeneratePrelaunchCommandAsEnv(
			node,
			robotDef,
			artifacts,
		)

		env := robotv1alpha1.GetNodeContainerEnvironmentVariables(
			robotDef,
			node.WorkspaceKey,
		)
		env = append(env, runCommand)
		env = append(env, prelaunchCommand)
		env = append(env, node.Env...)

		if robotDef.DiscoveryServer.Enabled {

			connectionString := helpers.GetDiscoveryServerDNS(*robot)

			discoveryServerConfig := []corev1.EnvVar{
				helpers.Env("FASTRTPS_DEFAULT_PROFILES_FILE", "/etc/super_client_configuration_file.xml"),
				helpers.Env("ROS_DISCOVERY_SERVER", connectionString+":11811"),
			}

			env = append(env, discoveryServerConfig...)
		}

		sleepTime := strconv.Itoa(key)

		var cmdBuilder strings.Builder
		cmdBuilder.WriteString("sleep " + sleepTime + " && ")
		cmdBuilder.WriteString("source " + filepath.Join("/opt", "ros", "$DISTRO", "setup.bash") + " && ")
		cmdBuilder.WriteString("source " + filepath.Join("$WORKSPACE", "$WSSUBPATH", "$WSSUBDIR", "setup.bash") + " && ")
		cmdBuilder.WriteString("$PRELAUNCH; ")
		cmdBuilder.WriteString("$COMMAND")

		cmd := helpers.Bash(cmdBuilder.String())

		ports := []corev1.ContainerPort{}
		if strings.Contains(node.Executable, "gzserver") || strings.Contains(node.Package, "gzserver") {
			ports = append(ports, corev1.ContainerPort{
				Protocol:      corev1.ProtocolTCP,
				ContainerPort: 11345,
			})
		}

		containers = append(containers, corev1.Container{
			Name:    node.NodeID,
			Image:   artifacts.NodeContainer.Image,
			Command: cmd,
			Stdin:   true,
			TTY:     true,
			VolumeMounts: []corev1.VolumeMount{
				volume.GetVolumeMount("", volume.GetVolumeVar(robotConfig)),
				volume.GetVolumeMount("", volume.GetVolumeUsr(robotConfig)),
				volume.GetVolumeMount("", volume.GetVolumeOpt(robotConfig)),
				volume.GetVolumeMount("", volume.GetVolumeEtc(robotConfig)),
				volume.GetVolumeMount(robotv1alpha1.X11UnixPath, volume.GetVolumeX11Unix(robotConfig)),
				volume.GetVolumeMount(robotDef.WorkspacesPath, volume.GetVolumeWorkspace(robotClone)),
				volume.GetVolumeMount(robotv1alpha1.CustomScriptsPath, volume.GetVolumeConfigMaps(robotBuild)),
			},
			Env:                      env,
			ImagePullPolicy:          corev1.PullAlways,
			TerminationMessagePolicy: corev1.TerminationMessageReadFile,
			Ports:                    ports,
			// Resources:                resourceLimits,
		})
	}

	if len(containers) == 0 {
		return nil, errors.New("no nodes to add")
	}

	launchPod := corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetLaunchPodMetadata().Name,
			Namespace: cr.GetLaunchPodMetadata().Namespace,
		},
		Spec: corev1.PodSpec{
			ShareProcessNamespace: &shareProcessNamespace,
			Containers:            containers,
			Volumes: []corev1.Volume{
				volume.GetVolumeVar(robotConfig),
				volume.GetVolumeUsr(robotConfig),
				volume.GetVolumeOpt(robotConfig),
				volume.GetVolumeEtc(robotConfig),
				volume.GetVolumeX11Unix(robotConfig),
				volume.GetVolumeWorkspace(robotClone),
				volume.GetVolumeConfigMaps(robotBuild),
			},
			RestartPolicy: corev1.RestartPolicyNever,
			NodeSelector:  robotDef.NodeSelector,
		},
	}

	return &launchPod, nil
}
