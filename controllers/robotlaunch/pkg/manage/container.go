package manage

import (
	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	corev1 "k8s.io/api/core/v1"
)

func UpdateContainerStates(
	cr *robotv1alpha1.RobotLaunch,
	pod corev1.Pod,
) {

	for _, c := range pod.Status.ContainerStatuses {

		var state robotv1alpha1.ContainerState

		switch true {
		case c.State.Running != nil:
			state = robotv1alpha1.ContainerStateRunning
		case c.State.Terminated != nil:
			state = robotv1alpha1.ContainerStateTerminated
		case c.State.Waiting != nil:
			state = robotv1alpha1.ContainerStateWaiting
		}

		for key, node := range cr.Status.NodeRequest.Nodes {
			if node.NodeID == c.Name {
				node := cr.Status.NodeRequest.Nodes[key]
				node.Container = robotv1alpha1.Container{
					Name:  c.Name,
					State: state,
				}
				cr.Status.NodeRequest.Nodes[key] = node
			}
		}
	}
}
