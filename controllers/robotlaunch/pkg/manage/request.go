package manage

import (
	"bytes"
	"encoding/json"
	"strings"

	"os"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/pkg/helpers"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/remotecommand"
)

type NodeRequirementResponse struct {
	Status string               `json:"status,omitempty"`
	Nodes  []robotv1alpha1.Node `json:"info,omitempty"`
}

func InspectROSNodes(
	cr *robotv1alpha1.RobotLaunch,
	robot robotv1alpha1.Robot,
	artifacts robotv1alpha1.Artifacts,
	toolsPodMetadata *types.NamespacedName,
	container string,
	restInterface rest.Interface,
	restConfig *rest.Config,
	scheme *runtime.Scheme) (NodeRequirementResponse, error) {

	robotDef := robot.Spec.Robot
	nodeSlice := []robotv1alpha1.Node{}

	for wsKey, ws := range robotDef.Workspaces {
		for repoKey, repo := range ws.Repositories {
			if repo.Launch.LaunchFilePath != "" {

				var envVarAdderStrBuilder strings.Builder
				for _, env := range repo.Launch.Env {
					envVarAdderStrBuilder.WriteString("export " + env.Name + "=" + env.Value + " && ")
				}
				var parameterAdderStrBuilder strings.Builder
				for paramKey, paramVal := range repo.Launch.Parameters {
					parameterAdderStrBuilder.WriteString(paramKey + ":=" + paramVal + " ")
				}

				var cmdBuilder strings.Builder
				cmdBuilder.WriteString("source /opt/ros/$DISTRO/setup.bash && ")
				cmdBuilder.WriteString("source " + robotv1alpha1.GetWorkspaceSourceFilePath(robotDef.WorkspacesPath, ws.Name, robotDef.Distro) + " && ")
				cmdBuilder.WriteString(envVarAdderStrBuilder.String())
				cmdBuilder.WriteString(robotv1alpha1.HelpersPath + "robolaunch.py inspect --launch-file " + robotv1alpha1.GetLaunchfilePathAbsolute(robotDef.WorkspacesPath, ws.Name, repo.Name, repo.Launch.LaunchFilePath) + " " + parameterAdderStrBuilder.String())

				inspectCommand := restInterface.
					Post().
					Namespace(toolsPodMetadata.Namespace).
					Resource("pods").
					Name(toolsPodMetadata.Name).
					SubResource("exec").
					VersionedParams(&corev1.PodExecOptions{
						Container: container,
						Command:   helpers.Bash(cmdBuilder.String()),
						Stdin:     true,
						Stdout:    true,
						Stderr:    true,
					}, runtime.NewParameterCodec(scheme))

				exec, err := remotecommand.NewSPDYExecutor(restConfig, "POST", inspectCommand.URL())
				if err != nil {
					return NodeRequirementResponse{}, err
				}

				var buff bytes.Buffer
				err = exec.Stream(remotecommand.StreamOptions{
					Stdin:  os.Stdin,
					Stdout: &buff,
					Stderr: os.Stderr,
					Tty:    false,
				})

				if err != nil {
					return NodeRequirementResponse{}, err
				}

				nodes := []robotv1alpha1.Node{}
				err = json.Unmarshal(buff.Bytes(), &nodes)
				if err != nil {
					return NodeRequirementResponse{}, err
				}

				for key := range nodes {
					nodes[key].WorkspaceKey = wsKey
					nodes[key].RepositoryKey = repoKey
					nodes[key].Env = repo.Launch.Env
					nodes[key].LaunchParameters = repo.Launch.Parameters
					if robotDef.Mode == robotv1alpha1.RobotModeHybrid {
						nodes[key].Activated = false
					} else {
						nodes[key].Activated = true
					}
				}

				nodeSlice = append(nodeSlice, nodes...)
			}
		}
	}

	if robotDef.Mode == robotv1alpha1.RobotModeHybrid {
		nodeSlice = filterNodesAsActivated(*cr, robot, nodeSlice)
	}

	nodeSlice = filterNodesAsMuted(nodeSlice)

	response := NodeRequirementResponse{
		Status: "success",
		Nodes:  nodeSlice,
	}

	return response, nil
}

func filterNodesAsActivated(robotLaunch robotv1alpha1.RobotLaunch, robot robotv1alpha1.Robot, nodes []robotv1alpha1.Node) []robotv1alpha1.Node {

	robotDef := robot.Spec.Robot

	for packageName, cluster := range robotDef.PackageClusterSelection {
		for k, n := range nodes {
			if cluster == robot.Status.Cluster {
				if n.Package == packageName || strings.Contains(n.Executable, packageName) {
					nodes[k].Activated = true
				}
			}
		}
	}

	return nodes
}

func filterNodesAsMuted(nodes []robotv1alpha1.Node) []robotv1alpha1.Node {
	mutedNodeKeywords := []string{
		"gzclient",
	}

	validNodes := []robotv1alpha1.Node{}

	for _, n := range nodes {

		isMuted := false
		for _, keyword := range mutedNodeKeywords {
			if strings.Contains(n.Package, keyword) || strings.Contains(n.Executable, keyword) {
				isMuted = true
				break
			}
		}

		if isMuted {
			continue
		}

		validNodes = append(validNodes, n)
	}

	return validNodes
}
