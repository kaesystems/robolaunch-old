package robottools

import (
	"context"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/pkg/helpers"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
)

func (r *RobotToolsReconciler) checkTracker(ctx context.Context, instance *robotv1alpha1.RobotTools, robot *robotv1alpha1.Robot) error {

	switch robot.Spec.Robot.Tools.Tracker.Enabled {
	case true:

		trackerPodQuery := &corev1.Pod{}
		err := r.Get(ctx, *instance.GetTrackerPodMetadata(), trackerPodQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.TrackerPodStatus = robotv1alpha1.ToolsPodStatus{}
			} else {
				return err
			}
		} else {
			instance.Status.TrackerPodStatus.Created = true
			instance.Status.TrackerPodStatus.Phase = trackerPodQuery.Status.Phase
			instance.Status.TrackerPodStatus.IP = trackerPodQuery.Status.PodIP
		}

		trackerServiceQuery := &corev1.Service{}
		err = r.Get(ctx, *instance.GetTrackerServiceMetadata(), trackerServiceQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.TrackerServiceStatus = robotv1alpha1.ToolsServiceStatus{}
			} else {
				return err
			}
		} else {
			instance.Status.TrackerServiceStatus.Created = true
			instance.Status.TrackerServiceStatus.Ports = helpers.GetNodePortMap(*trackerServiceQuery)
		}

	case false:

		trackerServiceQuery := &corev1.Service{}
		err := r.Get(ctx, *instance.GetTrackerServiceMetadata(), trackerServiceQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.TrackerServiceStatus.Created = false
			} else {
				return err
			}
		} else {
			err = r.Delete(ctx, trackerServiceQuery)
			if err != nil {
				return err
			}
		}

		trackerPodQuery := &corev1.Pod{}
		err = r.Get(ctx, *instance.GetTrackerPodMetadata(), trackerPodQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.TrackerPodStatus = robotv1alpha1.ToolsPodStatus{}
			} else {
				return err
			}
		} else {
			err = r.Delete(ctx, trackerPodQuery)
			if err != nil {
				return err
			}
		}

	}

	return nil
}

func (r *RobotToolsReconciler) checkCloudIDE(ctx context.Context, instance *robotv1alpha1.RobotTools, robot *robotv1alpha1.Robot) error {

	switch robot.Spec.Robot.Tools.CloudIDE.Enabled {
	case true:

		cloudIDEPodQuery := &corev1.Pod{}
		err := r.Get(ctx, *instance.GetCloudIDEPodMetadata(), cloudIDEPodQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.CloudIDEPodStatus = robotv1alpha1.ToolsPodStatus{}
			} else {
				return err
			}
		} else {
			instance.Status.CloudIDEPodStatus.Created = true
			instance.Status.CloudIDEPodStatus.Phase = cloudIDEPodQuery.Status.Phase
			instance.Status.CloudIDEPodStatus.IP = cloudIDEPodQuery.Status.PodIP
		}

		cloudIDEServiceQuery := &corev1.Service{}
		err = r.Get(ctx, *instance.GetCloudIDEServiceMetadata(), cloudIDEServiceQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.CloudIDEServiceStatus = robotv1alpha1.ToolsServiceStatus{}
			} else {
				return err
			}
		} else {
			instance.Status.CloudIDEServiceStatus.Created = true
			instance.Status.CloudIDEServiceStatus.Ports = helpers.GetNodePortMap(*cloudIDEServiceQuery)

		}

	case false:

		cloudIDEPodQuery := &corev1.Pod{}
		err := r.Get(ctx, *instance.GetCloudIDEPodMetadata(), cloudIDEPodQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.CloudIDEPodStatus = robotv1alpha1.ToolsPodStatus{}
			} else {
				return err
			}
		} else {
			err = r.Delete(ctx, cloudIDEPodQuery)
			if err != nil {
				return err
			}
		}

		cloudIDEServiceQuery := &corev1.Service{}
		err = r.Get(ctx, *instance.GetCloudIDEServiceMetadata(), cloudIDEServiceQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.CloudIDEServiceStatus.Created = false
			} else {
				return err
			}
		} else {
			err = r.Delete(ctx, cloudIDEServiceQuery)
			if err != nil {
				return err
			}
		}

	}

	return nil
}

func (r *RobotToolsReconciler) checkBridge(ctx context.Context, instance *robotv1alpha1.RobotTools, robot *robotv1alpha1.Robot) error {

	switch robot.Spec.Robot.Tools.Bridge.Enabled {
	case true:

		bridgePodQuery := &corev1.Pod{}
		err := r.Get(ctx, *instance.GetBridgePodMetadata(), bridgePodQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.BridgePodStatus = robotv1alpha1.ToolsPodStatus{}
			} else {
				return err
			}
		} else {
			instance.Status.BridgePodStatus.Created = true
			instance.Status.BridgePodStatus.Phase = bridgePodQuery.Status.Phase
			instance.Status.BridgePodStatus.IP = bridgePodQuery.Status.PodIP
		}

		bridgeServiceQuery := &corev1.Service{}
		err = r.Get(ctx, *instance.GetBridgeServiceMetadata(), bridgeServiceQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.BridgeServiceStatus = robotv1alpha1.ToolsServiceStatus{}
			} else {
				return err
			}
		} else {
			instance.Status.BridgeServiceStatus.Created = true
			instance.Status.BridgeServiceStatus.Ports = helpers.GetNodePortMap(*bridgeServiceQuery)
		}

	case false:

		bridgePodQuery := &corev1.Pod{}
		err := r.Get(ctx, *instance.GetBridgePodMetadata(), bridgePodQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.BridgePodStatus = robotv1alpha1.ToolsPodStatus{}
			} else {
				return err
			}
		} else {
			err = r.Delete(ctx, bridgePodQuery)
			if err != nil {
				return err
			}
		}

		bridgeServiceQuery := &corev1.Service{}
		err = r.Get(ctx, *instance.GetBridgeServiceMetadata(), bridgeServiceQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.BridgeServiceStatus.Created = false
			} else {
				return err
			}
		} else {
			err = r.Delete(ctx, bridgeServiceQuery)
			if err != nil {
				return err
			}
		}

	}

	return nil
}

func (r *RobotToolsReconciler) checkFoxglove(ctx context.Context, instance *robotv1alpha1.RobotTools, robot *robotv1alpha1.Robot) error {

	switch robot.Spec.Robot.Tools.Foxglove.Enabled {
	case true:

		foxglovePodQuery := &corev1.Pod{}
		err := r.Get(ctx, *instance.GetFoxglovePodMetadata(), foxglovePodQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.FoxglovePodStatus = robotv1alpha1.ToolsPodStatus{}
			} else {
				return err
			}
		} else {
			instance.Status.FoxglovePodStatus.Created = true
			instance.Status.FoxglovePodStatus.Phase = foxglovePodQuery.Status.Phase
			instance.Status.FoxglovePodStatus.IP = foxglovePodQuery.Status.PodIP
		}

		foxgloveServiceQuery := &corev1.Service{}
		err = r.Get(ctx, *instance.GetFoxgloveServiceMetadata(), foxgloveServiceQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.FoxgloveServiceStatus = robotv1alpha1.ToolsServiceStatus{}
			} else {
				return err
			}
		} else {
			instance.Status.FoxgloveServiceStatus.Created = true
			instance.Status.FoxgloveServiceStatus.Ports = helpers.GetNodePortMap(*foxgloveServiceQuery)
		}

	case false:

		foxglovePodQuery := &corev1.Pod{}
		err := r.Get(ctx, *instance.GetFoxglovePodMetadata(), foxglovePodQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.FoxglovePodStatus = robotv1alpha1.ToolsPodStatus{}
			} else {
				return err
			}
		} else {
			err = r.Delete(ctx, foxglovePodQuery)
			if err != nil {
				return err
			}
		}

		foxgloveServiceQuery := &corev1.Service{}
		err = r.Get(ctx, *instance.GetFoxgloveServiceMetadata(), foxgloveServiceQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				instance.Status.FoxgloveServiceStatus.Created = false
			} else {
				return err
			}
		} else {
			err = r.Delete(ctx, foxgloveServiceQuery)
			if err != nil {
				return err
			}
		}

	}

	return nil
}

func (r *RobotToolsReconciler) checkVDI(ctx context.Context, instance *robotv1alpha1.RobotTools, robot *robotv1alpha1.Robot) error {

	switch robot.Spec.Robot.State {
	case robotv1alpha1.RobotStateLaunched, robotv1alpha1.RobotStateBuilt:

		vdiQuery := &robotv1alpha1.VDI{}
		err := r.Get(ctx, *instance.GetVDIMetadata(), vdiQuery)
		if err != nil && errors.IsNotFound(err) {
			instance.Status.VDIInstanceStatus = robotv1alpha1.VDIStatusForRobot{}
		} else if err != nil {
			return err
		} else {

			switch robot.Spec.Robot.Tools.VDI.Enabled {
			case true:
				instance.Status.VDIInstanceStatus.Status = vdiQuery.Status
			case false:
				err = r.Delete(ctx, vdiQuery)
				if err != nil {
					return err
				}

				// r.Recorder.Event(
				// 	instance,
				// 	string(robotEventTypeNormal),
				// 	string(robotEventReasonDeleted),
				// 	fmt.Sprintf(
				// 		"Deleted VDI %s/%s",
				// 		instance.GetVDIMetadata().Namespace,
				// 		instance.GetVDIMetadata().Name,
				// 	),
				// )
			}
		}

	case robotv1alpha1.RobotStatePassive:

		vdiQuery := &robotv1alpha1.VDI{}
		err := r.Get(ctx, *instance.GetVDIMetadata(), vdiQuery)
		if err != nil && errors.IsNotFound(err) {
			instance.Status.VDIInstanceStatus = robotv1alpha1.VDIStatusForRobot{}
		} else if err != nil {
			return err
		} else {
			err = r.Delete(ctx, vdiQuery)
			if err != nil {
				return err
			}

			// r.Recorder.Event(
			// 	instance,
			// 	string(robotEventTypeNormal),
			// 	string(robotEventReasonDeleted),
			// 	fmt.Sprintf(
			// 		"Deleted VDI %s/%s",
			// 		instance.GetVDIMetadata().Namespace,
			// 		instance.GetVDIMetadata().Name,
			// 	),
			// )
		}
	}

	return nil
}
