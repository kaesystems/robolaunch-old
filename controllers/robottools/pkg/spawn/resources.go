package spawn

import (
	"strconv"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/pkg/helpers"
	"github.com/robolaunch/robot-operator/controllers/pkg/volume"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

var trackerSelector = map[string]string{
	"tools": "tracker",
}

var cloudIDESelector = map[string]string{
	"tools": "cloud-ide",
}

var bridgeSelector = map[string]string{
	"tools": "bridge",
}

var foxgloveSelector = map[string]string{
	"tools": "foxglove",
}

func GetTrackerPod(
	cr *robotv1alpha1.RobotTools,
	robot *robotv1alpha1.Robot,
	robotArtifact *robotv1alpha1.RobotArtifact,
	robotConfig *robotv1alpha1.RobotConfig,
	robotClone *robotv1alpha1.RobotClone,
	trackerRBAC *robotv1alpha1.RobotRBAC,
) (*corev1.Pod, error) {

	robotDef := robot.Spec.Robot
	artifacts := robotArtifact.Artifacts

	shareProcessNamespace := false

	trackerEnv := artifacts.TrackerContainer.Env

	if robotDef.DiscoveryServer.Enabled {

		connectionString := helpers.GetDiscoveryServerDNS(*robot)

		discoveryServerConfig := []corev1.EnvVar{
			helpers.Env("FASTRTPS_DEFAULT_PROFILES_FILE", "/etc/super_client_configuration_file.xml"),
			helpers.Env("ROS_DISCOVERY_SERVER", connectionString+":11811"),
		}

		trackerEnv = append(trackerEnv, discoveryServerConfig...)
	}

	containers := []corev1.Container{
		{
			Name:    artifacts.TrackerContainer.Name,
			Image:   artifacts.TrackerContainer.Image,
			Command: artifacts.TrackerContainer.Entrypoint,
			Env:     trackerEnv,
			VolumeMounts: []corev1.VolumeMount{
				volume.GetVolumeMount("", volume.GetVolumeVar(robotConfig)),
				volume.GetVolumeMount("", volume.GetVolumeUsr(robotConfig)),
				volume.GetVolumeMount("", volume.GetVolumeOpt(robotConfig)),
				volume.GetVolumeMount("", volume.GetVolumeEtc(robotConfig)),
				volume.GetVolumeMount(robotDef.WorkspacesPath, volume.GetVolumeWorkspace(robotClone)),
			},
			Ports: helpers.PortsFromMapToSlice(artifacts.TrackerContainer.Ports),
			Resources: corev1.ResourceRequirements{
				Limits: getToolResourceLimits(robotDef.Tools.Tracker),
			},
		},
	}

	toolsPod := corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetTrackerPodMetadata().Name,
			Namespace: cr.GetTrackerPodMetadata().Namespace,
			Labels:    trackerSelector,
		},
		Spec: corev1.PodSpec{
			ServiceAccountName:    trackerRBAC.GetServiceAccountMetadata().Name,
			ShareProcessNamespace: &shareProcessNamespace,
			Containers:            containers,
			Volumes: []corev1.Volume{
				volume.GetVolumeVar(robotConfig),
				volume.GetVolumeUsr(robotConfig),
				volume.GetVolumeOpt(robotConfig),
				volume.GetVolumeEtc(robotConfig),
				volume.GetVolumeWorkspace(robotClone),
			},
			RestartPolicy: corev1.RestartPolicyNever,
			NodeSelector:  robotDef.NodeSelector,
		},
	}

	return &toolsPod, nil
}

func GetTrackerService(
	cr *robotv1alpha1.RobotTools,
	artifacts robotv1alpha1.Artifacts,
) *corev1.Service {

	serviceSpec := corev1.ServiceSpec{
		Type:                  corev1.ServiceTypeNodePort,
		ExternalTrafficPolicy: corev1.ServiceExternalTrafficPolicyTypeLocal,
		Selector:              trackerSelector,
		Ports: []corev1.ServicePort{
			{
				Port: artifacts.TrackerContainer.Ports["tracker"].ContainerPort,
				TargetPort: intstr.IntOrString{
					IntVal: artifacts.TrackerContainer.Ports["tracker"].ContainerPort,
				},
				Protocol: corev1.ProtocolTCP,
				Name:     "tracker",
			},
		},
	}

	service := corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetTrackerServiceMetadata().Name,
			Namespace: cr.GetTrackerServiceMetadata().Namespace,
		},
		Spec: serviceSpec,
	}

	return &service
}

func GetCloudIDEPod(
	cr *robotv1alpha1.RobotTools,
	robot *robotv1alpha1.Robot,
	artifacts robotv1alpha1.Artifacts,
	robotConfig *robotv1alpha1.RobotConfig,
	robotClone *robotv1alpha1.RobotClone,
	cloudIDERBAC *robotv1alpha1.RobotRBAC,
) (*corev1.Pod, error) {

	robotDef := robot.Spec.Robot

	shareProcessNamespace := false

	containers := []corev1.Container{
		{
			Name:    artifacts.CloudIDEContainer.Name,
			Image:   artifacts.CloudIDEContainer.Image,
			Command: artifacts.CloudIDEContainer.Entrypoint,
			Env:     artifacts.CloudIDEContainer.Env,
			VolumeMounts: []corev1.VolumeMount{
				volume.GetVolumeMount("", volume.GetVolumeVar(robotConfig)),
				volume.GetVolumeMount("", volume.GetVolumeUsr(robotConfig)),
				volume.GetVolumeMount("", volume.GetVolumeOpt(robotConfig)),
				volume.GetVolumeMount("", volume.GetVolumeEtc(robotConfig)),
				volume.GetVolumeMount(robotDef.WorkspacesPath, volume.GetVolumeWorkspace(robotClone)),
			},
			Ports: helpers.PortsFromMapToSlice(artifacts.CloudIDEContainer.Ports),
			Resources: corev1.ResourceRequirements{
				Limits: getToolResourceLimits(robotDef.Tools.CloudIDE),
			},
		},
	}

	cloudIDEPod := corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetCloudIDEPodMetadata().Name,
			Namespace: cr.GetCloudIDEPodMetadata().Namespace,
			Labels:    cloudIDESelector,
		},
		Spec: corev1.PodSpec{
			ServiceAccountName:    cloudIDERBAC.GetServiceAccountMetadata().Name,
			ShareProcessNamespace: &shareProcessNamespace,
			Containers:            containers,
			Volumes: []corev1.Volume{
				volume.GetVolumeVar(robotConfig),
				volume.GetVolumeUsr(robotConfig),
				volume.GetVolumeOpt(robotConfig),
				volume.GetVolumeEtc(robotConfig),
				volume.GetVolumeWorkspace(robotClone),
			},
			RestartPolicy: corev1.RestartPolicyNever,
			NodeSelector:  robotDef.NodeSelector,
		},
	}

	return &cloudIDEPod, nil
}

func GetCloudIDEService(
	cr *robotv1alpha1.RobotTools,
	artifacts robotv1alpha1.Artifacts,
) *corev1.Service {

	serviceSpec := corev1.ServiceSpec{
		Type:                  corev1.ServiceTypeNodePort,
		ExternalTrafficPolicy: corev1.ServiceExternalTrafficPolicyTypeLocal,
		Selector:              cloudIDESelector,
		Ports: []corev1.ServicePort{
			{
				Port: artifacts.CloudIDEContainer.Ports["cloud-ide"].ContainerPort,
				TargetPort: intstr.IntOrString{
					IntVal: artifacts.CloudIDEContainer.Ports["cloud-ide"].ContainerPort,
				},
				Protocol: corev1.ProtocolTCP,
				Name:     "cloud-ide",
			},
		},
	}

	service := corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetCloudIDEServiceMetadata().Name,
			Namespace: cr.GetCloudIDEServiceMetadata().Namespace,
		},
		Spec: serviceSpec,
	}

	return &service
}

func GetBridgePod(
	cr *robotv1alpha1.RobotTools,
	robot *robotv1alpha1.Robot,
	robotArtifact *robotv1alpha1.RobotArtifact,
	robotConfig *robotv1alpha1.RobotConfig,
	robotClone *robotv1alpha1.RobotClone,
) (*corev1.Pod, error) {

	robotDef := robot.Spec.Robot
	artifacts := robotArtifact.Artifacts

	shareProcessNamespace := false

	bridgeEnv := artifacts.BridgeContainer.Env

	if robotDef.DiscoveryServer.Enabled {

		connectionString := helpers.GetDiscoveryServerDNS(*robot)

		discoveryServerConfig := []corev1.EnvVar{
			helpers.Env("FASTRTPS_DEFAULT_PROFILES_FILE", "/etc/super_client_configuration_file.xml"),
			helpers.Env("ROS_DISCOVERY_SERVER", connectionString+":11811"),
		}

		bridgeEnv = append(bridgeEnv, discoveryServerConfig...)
	}

	containers := []corev1.Container{
		{
			Name:    artifacts.BridgeContainer.Name,
			Image:   artifacts.BridgeContainer.Image,
			Command: artifacts.BridgeContainer.Entrypoint,
			Env:     bridgeEnv,
			VolumeMounts: []corev1.VolumeMount{
				volume.GetVolumeMount("", volume.GetVolumeVar(robotConfig)),
				volume.GetVolumeMount("", volume.GetVolumeUsr(robotConfig)),
				volume.GetVolumeMount("", volume.GetVolumeOpt(robotConfig)),
				volume.GetVolumeMount("", volume.GetVolumeEtc(robotConfig)),
				volume.GetVolumeMount(robotDef.WorkspacesPath, volume.GetVolumeWorkspace(robotClone)),
			},
			Ports: helpers.PortsFromMapToSlice(artifacts.BridgeContainer.Ports),
			Resources: corev1.ResourceRequirements{
				Limits: getToolResourceLimits(robotDef.Tools.Bridge),
			},
		},
	}

	bridgePod := corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetBridgePodMetadata().Name,
			Namespace: cr.GetBridgePodMetadata().Namespace,
			Labels:    bridgeSelector,
		},
		Spec: corev1.PodSpec{
			ShareProcessNamespace: &shareProcessNamespace,
			Containers:            containers,
			Volumes: []corev1.Volume{
				volume.GetVolumeVar(robotConfig),
				volume.GetVolumeUsr(robotConfig),
				volume.GetVolumeOpt(robotConfig),
				volume.GetVolumeEtc(robotConfig),
				volume.GetVolumeWorkspace(robotClone),
			},
			RestartPolicy: corev1.RestartPolicyNever,
			NodeSelector:  robotDef.NodeSelector,
		},
	}

	return &bridgePod, nil
}

func GetBridgeService(
	cr *robotv1alpha1.RobotTools,
	artifacts robotv1alpha1.Artifacts,
) *corev1.Service {

	serviceSpec := corev1.ServiceSpec{
		Type:                  corev1.ServiceTypeNodePort,
		ExternalTrafficPolicy: corev1.ServiceExternalTrafficPolicyTypeLocal,
		Selector:              bridgeSelector,
		Ports: []corev1.ServicePort{
			{
				Port: artifacts.BridgeContainer.Ports["bridge-ws"].ContainerPort,
				TargetPort: intstr.IntOrString{
					IntVal: artifacts.BridgeContainer.Ports["bridge-ws"].ContainerPort,
				},
				Protocol: corev1.ProtocolTCP,
				Name:     "bridge-ws",
			},
		},
	}

	service := corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetBridgeServiceMetadata().Name,
			Namespace: cr.GetBridgeServiceMetadata().Namespace,
		},
		Spec: serviceSpec,
	}

	return &service
}

func GetFoxglovePod(
	cr *robotv1alpha1.RobotTools,
	robot *robotv1alpha1.Robot,
	robotArtifact *robotv1alpha1.RobotArtifact,
) (*corev1.Pod, error) {

	robotDef := robot.Spec.Robot
	artifacts := robotArtifact.Artifacts

	shareProcessNamespace := false

	foxgloveEnv := artifacts.FoxgloveContainer.Env

	containers := []corev1.Container{
		{
			Name:  artifacts.FoxgloveContainer.Name,
			Image: artifacts.FoxgloveContainer.Image,
			Env:   foxgloveEnv,
			Ports: helpers.PortsFromMapToSlice(artifacts.FoxgloveContainer.Ports),
			Resources: corev1.ResourceRequirements{
				Limits: getToolResourceLimits(robotDef.Tools.Foxglove),
			},
		},
	}

	foxglovePod := corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetFoxglovePodMetadata().Name,
			Namespace: cr.GetFoxglovePodMetadata().Namespace,
			Labels:    foxgloveSelector,
		},
		Spec: corev1.PodSpec{
			ShareProcessNamespace: &shareProcessNamespace,
			Containers:            containers,
			RestartPolicy:         corev1.RestartPolicyNever,
			NodeSelector:          robotDef.NodeSelector,
		},
	}

	return &foxglovePod, nil
}

func GetFoxgloveService(
	cr *robotv1alpha1.RobotTools,
	artifacts robotv1alpha1.Artifacts,
) *corev1.Service {

	serviceSpec := corev1.ServiceSpec{
		Type:                  corev1.ServiceTypeNodePort,
		ExternalTrafficPolicy: corev1.ServiceExternalTrafficPolicyTypeLocal,
		Selector:              foxgloveSelector,
		Ports: []corev1.ServicePort{
			{
				Port: artifacts.FoxgloveContainer.Ports["foxglove"].ContainerPort,
				TargetPort: intstr.IntOrString{
					IntVal: artifacts.FoxgloveContainer.Ports["foxglove"].ContainerPort,
				},
				Protocol: corev1.ProtocolTCP,
				Name:     "foxglove",
			},
		},
	}

	service := corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetFoxgloveServiceMetadata().Name,
			Namespace: cr.GetFoxgloveServiceMetadata().Namespace,
		},
		Spec: serviceSpec,
	}

	return &service
}

func GetVDI(
	cr *robotv1alpha1.RobotTools,
	robot *robotv1alpha1.Robot,
) *robotv1alpha1.VDI {

	meta := cr.GetVDIMetadata()
	// robotDef := robot.Spec.Robot

	// ip := robot.Status.RuntimeStatus.Status.LaunchStatus.Status.LaunchPodStatus.IP
	// neko := robotDef.VDI.Neko
	// neko.Gazebo.MasterURI = "http://" + ip + ":11345"

	vdiSpec := robotv1alpha1.VDISpec{
		Config: cr.Spec.Tools.VDI,
	}

	vdi := robotv1alpha1.VDI{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: vdiSpec,
	}

	return &vdi
}

func getToolResourceLimits(tc robotv1alpha1.ToolConfig) corev1.ResourceList {
	resourceLimits := corev1.ResourceList{}
	if tc.Resources.GPUCore != 0 {
		resourceLimits["nvidia.com/gpu"] = resource.MustParse(strconv.Itoa(tc.Resources.GPUCore))
	}
	if tc.Resources.CPU != "" {
		resourceLimits["cpu"] = resource.MustParse(tc.Resources.CPU)
	}
	if tc.Resources.Memory != "" {
		resourceLimits["memory"] = resource.MustParse(tc.Resources.Memory)
	}

	return resourceLimits
}
