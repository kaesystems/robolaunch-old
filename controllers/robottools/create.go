package robottools

import (
	"context"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robottools/pkg/spawn"
	ctrl "sigs.k8s.io/controller-runtime"
)

func (r *RobotToolsReconciler) createTrackerPod(
	ctx context.Context,
	instance *robotv1alpha1.RobotTools,
) error {

	robotConfig, err := r.reconcileGetRobotConfig(ctx, instance)
	if err != nil {
		return err
	}

	robotClone, err := r.reconcileGetRobotClone(ctx, instance)
	if err != nil {
		return err
	}

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return err
	}

	trackerRBAC, err := r.reconcileGetTrackerRBAC(ctx, instance)
	if err != nil {
		return err
	}

	toolsPod, err := spawn.GetTrackerPod(
		instance,
		robot,
		robotArtifact,
		robotConfig,
		robotClone,
		trackerRBAC,
	)
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, toolsPod, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, toolsPod)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Tools pod is created.")
	instance.Status.TrackerPodStatus.Created = true

	return nil
}

func (r *RobotToolsReconciler) createTrackerService(
	ctx context.Context,
	instance *robotv1alpha1.RobotTools,
) error {

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	service := spawn.GetTrackerService(instance, robotArtifact.Artifacts)
	err = ctrl.SetControllerReference(instance, service, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, service)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Tools service is created.")
	instance.Status.TrackerServiceStatus.Created = true

	return nil
}

func (r *RobotToolsReconciler) createCloudIDEPod(
	ctx context.Context,
	instance *robotv1alpha1.RobotTools,
) error {

	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return err
	}

	robotConfig, err := r.reconcileGetRobotConfig(ctx, instance)
	if err != nil {
		return err
	}

	robotClone, err := r.reconcileGetRobotClone(ctx, instance)
	if err != nil {
		return err
	}

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	cloudIDERBAC, err := r.reconcileGetCloudIDERBAC(ctx, instance)
	if err != nil {
		return err
	}

	cloudIDEPod, err := spawn.GetCloudIDEPod(
		instance,
		robot,
		robotArtifact.Artifacts,
		robotConfig,
		robotClone,
		cloudIDERBAC,
	)
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, cloudIDEPod, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, cloudIDEPod)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Cloud IDE pod is created.")
	instance.Status.CloudIDEPodStatus.Created = true

	return nil
}

func (r *RobotToolsReconciler) createCloudIDEService(
	ctx context.Context,
	instance *robotv1alpha1.RobotTools,
) error {

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	service := spawn.GetCloudIDEService(instance, robotArtifact.Artifacts)
	err = ctrl.SetControllerReference(instance, service, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, service)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Cloud IDE service is created.")
	instance.Status.CloudIDEServiceStatus.Created = true

	return nil
}

func (r *RobotToolsReconciler) createBridgePod(
	ctx context.Context,
	instance *robotv1alpha1.RobotTools,
) error {

	robotConfig, err := r.reconcileGetRobotConfig(ctx, instance)
	if err != nil {
		return err
	}

	robotClone, err := r.reconcileGetRobotClone(ctx, instance)
	if err != nil {
		return err
	}

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return err
	}

	bridgePod, err := spawn.GetBridgePod(
		instance,
		robot,
		robotArtifact,
		robotConfig,
		robotClone,
	)
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, bridgePod, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, bridgePod)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Bridge pod is created.")
	instance.Status.BridgePodStatus.Created = true

	return nil
}

func (r *RobotToolsReconciler) createBridgeService(
	ctx context.Context,
	instance *robotv1alpha1.RobotTools,
) error {

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	service := spawn.GetBridgeService(instance, robotArtifact.Artifacts)
	err = ctrl.SetControllerReference(instance, service, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, service)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Bridge service is created.")
	instance.Status.BridgeServiceStatus.Created = true

	return nil
}

func (r *RobotToolsReconciler) createFoxglovePod(
	ctx context.Context,
	instance *robotv1alpha1.RobotTools,
) error {

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return err
	}

	foxglovePod, err := spawn.GetFoxglovePod(
		instance,
		robot,
		robotArtifact,
	)
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, foxglovePod, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, foxglovePod)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Foxglove pod is created.")
	instance.Status.FoxglovePodStatus.Created = true

	return nil
}

func (r *RobotToolsReconciler) createFoxgloveService(
	ctx context.Context,
	instance *robotv1alpha1.RobotTools,
) error {

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	service := spawn.GetFoxgloveService(instance, robotArtifact.Artifacts)
	err = ctrl.SetControllerReference(instance, service, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, service)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Foxglove service is created.")
	instance.Status.FoxgloveServiceStatus.Created = true

	return nil
}

func (r *RobotToolsReconciler) createVDI(ctx context.Context, instance *robotv1alpha1.RobotTools) error {
	// launchPodCreated := instance.Status.RuntimeStatus.Status.LaunchStatus.Status.LaunchPodStatus.Created
	// launchPodIP := instance.Status.RuntimeStatus.Status.LaunchStatus.Status.LaunchPodStatus.IP

	// if launchPodCreated && launchPodIP != "" {

	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return err
	}

	vdi := spawn.GetVDI(instance, robot)

	err = ctrl.SetControllerReference(instance, vdi, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, vdi)
	if err != nil {
		return err
	}

	logger.Info("STATUS: VDI is created.")
	instance.Status.VDIInstanceStatus.Created = true

	// r.Recorder.Event(
	// 	instance,
	// 	string(robotEventTypeNormal),
	// 	string(robotEventReasonCreated),
	// 	fmt.Sprintf(
	// 		"Created VDI %s/%s",
	// 		instance.GetVDIMetadata().Namespace,
	// 		instance.GetVDIMetadata().Name,
	// 	),
	// )
	// }

	return nil
}
