package robottools

import (
	"context"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
)

type RobotToolsReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robottools,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robottools/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robottools/finalizers,verbs=update

//+kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot,resources=vdis,verbs=get;list;watch;create;update;patch;delete

var logger logr.Logger

func (r *RobotToolsReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger = log.FromContext(ctx)

	instance, err := r.reconcileGetInstance(ctx, req.NamespacedName)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckStatus(ctx, instance, robot)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckOwnedResources(ctx, instance, robot)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *RobotToolsReconciler) reconcileGetInstance(ctx context.Context, meta types.NamespacedName) (*robotv1alpha1.RobotTools, error) {
	robottools := &robotv1alpha1.RobotTools{}
	err := r.Get(ctx, meta, robottools)
	if err != nil {
		return &robotv1alpha1.RobotTools{}, err
	}
	return robottools, nil
}

func (r *RobotToolsReconciler) reconcileCheckStatus(ctx context.Context, instance *robotv1alpha1.RobotTools, robot *robotv1alpha1.Robot) error {

	robotDef := robot.Spec.Robot

	if robotDef.Tools.Tracker.Enabled {

		switch instance.Status.TrackerPodStatus.Created {
		case false:
			err := r.createTrackerPod(ctx, instance)
			if err != nil {
				return err
			}
		}

		switch instance.Status.TrackerServiceStatus.Created {
		case false:
			err := r.createTrackerService(ctx, instance)
			if err != nil {
				return err
			}
		}

	}

	if robotDef.Tools.CloudIDE.Enabled {

		switch instance.Status.CloudIDEServiceStatus.Created {
		case false:
			err := r.createCloudIDEService(ctx, instance)
			if err != nil {
				return err
			}
		}

		switch instance.Status.CloudIDEPodStatus.Created {
		case false:
			err := r.createCloudIDEPod(ctx, instance)
			if err != nil {
				return err
			}
		}

	}

	if robotDef.Tools.Bridge.Enabled {

		switch instance.Status.BridgePodStatus.Created {
		case false:
			err := r.createBridgePod(ctx, instance)
			if err != nil {
				return err
			}
		}

		switch instance.Status.BridgeServiceStatus.Created {
		case false:
			err := r.createBridgeService(ctx, instance)
			if err != nil {
				return err
			}
		}

	}

	if robotDef.Tools.Foxglove.Enabled {

		switch instance.Status.FoxglovePodStatus.Created {
		case false:
			err := r.createFoxglovePod(ctx, instance)
			if err != nil {
				return err
			}
		}

		switch instance.Status.FoxgloveServiceStatus.Created {
		case false:
			err := r.createFoxgloveService(ctx, instance)
			if err != nil {
				return err
			}
		}

	}

	if robotDef.Tools.VDI.Enabled {

		switch instance.Status.VDIInstanceStatus.Created {
		case false:
			err := r.createVDI(ctx, instance)
			if err != nil {
				return err
			}
		}

	}

	// ASSIGN ROBOT TOOLS PHASE

	// if instance.Spec.Robot.Tools.CloudIDE == robotv1alpha1.ToolSelectionEnabled {
	// 	if instance.Status.CloudIDEPodStatus.Phase == corev1.PodRunning && instance.Status.CloudIDEServiceStatus.Created &&
	// 		instance.Status.TrackerPodStatus.Phase == corev1.PodRunning && instance.Status.TrackerServiceStatus.Created {
	// 		instance.Status.Phase = robotv1alpha1.RobotToolsPhaseReady
	// 	} else {
	// 		instance.Status.Phase = robotv1alpha1.RobotToolsPhaseConfiguringTools
	// 	}
	// } else {
	// 	if instance.Status.TrackerPodStatus.Phase == corev1.PodRunning && instance.Status.TrackerServiceStatus.Created {
	// 		instance.Status.Phase = robotv1alpha1.RobotToolsPhaseReady
	// 	} else {
	// 		instance.Status.Phase = robotv1alpha1.RobotToolsPhaseConfiguringTools
	// 	}
	// }

	return nil
}

func (r *RobotToolsReconciler) reconcileCheckOwnedResources(ctx context.Context, instance *robotv1alpha1.RobotTools, robot *robotv1alpha1.Robot) error {

	err := r.checkTracker(ctx, instance, robot)
	if err != nil {
		return err
	}

	err = r.checkCloudIDE(ctx, instance, robot)
	if err != nil {
		return err
	}

	err = r.checkBridge(ctx, instance, robot)
	if err != nil {
		return err
	}

	err = r.checkFoxglove(ctx, instance, robot)
	if err != nil {
		return err
	}

	err = r.checkVDI(ctx, instance, robot)
	if err != nil {
		return err
	}

	return nil
}

func (r *RobotToolsReconciler) reconcileGetOwner(ctx context.Context, instance *robotv1alpha1.RobotTools) (*robotv1alpha1.Robot, error) {
	robot := &robotv1alpha1.Robot{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	return robot, nil
}

func (r *RobotToolsReconciler) reconcileGetArtifact(ctx context.Context, instance *robotv1alpha1.RobotTools) (*robotv1alpha1.RobotArtifact, error) {
	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotartifact := &robotv1alpha1.RobotArtifact{}
	err = r.Get(ctx, types.NamespacedName{
		Namespace: robot.GetRobotArtifactMetadata().Namespace,
		Name:      robot.GetRobotArtifactMetadata().Name,
	}, robotartifact)
	if err != nil {
		return nil, err
	}

	return robotartifact, nil
}

func (r *RobotToolsReconciler) reconcileUpdateInstanceStatus(ctx context.Context, instance *robotv1alpha1.RobotTools) error {
	return retry.RetryOnConflict(retry.DefaultRetry, func() error {
		instanceLV := &robotv1alpha1.RobotTools{}
		err := r.Get(ctx, types.NamespacedName{
			Name:      instance.Name,
			Namespace: instance.Namespace,
		}, instanceLV)

		if err == nil {
			instance.ResourceVersion = instanceLV.ResourceVersion
		}

		err1 := r.Status().Update(ctx, instance)
		return err1
	})
}

func (r *RobotToolsReconciler) reconcileGetRobotConfig(ctx context.Context, instance *robotv1alpha1.RobotTools) (*robotv1alpha1.RobotConfig, error) {
	robot := &robotv1alpha1.Robot{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	robotconfig := &robotv1alpha1.RobotConfig{}
	err = r.Get(ctx, *robot.GetRobotConfigMetadata(), robotconfig)
	if err != nil {
		return nil, err
	}

	return robotconfig, nil
}

func (r *RobotToolsReconciler) reconcileGetTrackerRBAC(ctx context.Context, instance *robotv1alpha1.RobotTools) (*robotv1alpha1.RobotRBAC, error) {
	robotconfig, err := r.reconcileGetRobotConfig(ctx, instance)
	if err != nil {
		return nil, err
	}

	trackerRBAC := &robotv1alpha1.RobotRBAC{}
	err = r.Get(ctx, *robotconfig.GetTrackerRBACMetadata(), trackerRBAC)
	if err != nil {
		return nil, err
	}

	return trackerRBAC, nil
}

func (r *RobotToolsReconciler) reconcileGetCloudIDERBAC(ctx context.Context, instance *robotv1alpha1.RobotTools) (*robotv1alpha1.RobotRBAC, error) {
	robotconfig, err := r.reconcileGetRobotConfig(ctx, instance)
	if err != nil {
		return nil, err
	}

	cloudIDERBAC := &robotv1alpha1.RobotRBAC{}
	err = r.Get(ctx, *robotconfig.GetCloudIDERBACMetadata(), cloudIDERBAC)
	if err != nil {
		return nil, err
	}

	return cloudIDERBAC, nil
}

func (r *RobotToolsReconciler) reconcileGetRobotClone(ctx context.Context, instance *robotv1alpha1.RobotTools) (*robotv1alpha1.RobotClone, error) {
	robot := &robotv1alpha1.Robot{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	robotclone := &robotv1alpha1.RobotClone{}
	err = r.Get(ctx, *robot.GetRobotCloneMetadata(), robotclone)
	if err != nil {
		return nil, err
	}

	return robotclone, nil
}

func (r *RobotToolsReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&robotv1alpha1.RobotTools{}).
		Owns(&corev1.Pod{}).
		Owns(&corev1.Service{}).
		Owns(&robotv1alpha1.VDI{}).
		Complete(r)
}
