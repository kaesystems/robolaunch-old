package spawn

import (
	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetServiceAccount(cr *robotv1alpha1.RobotRBAC) *corev1.ServiceAccount {

	serviceAccount := corev1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetServiceAccountMetadata().Name,
			Namespace: cr.GetServiceAccountMetadata().Namespace,
		},
	}

	return &serviceAccount
}

func GetRole(cr *robotv1alpha1.RobotRBAC) *rbacv1.Role {

	role := rbacv1.Role{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetRoleMetadata().Name,
			Namespace: cr.GetRoleMetadata().Namespace,
		},
		Rules: cr.Spec.Rules,
	}

	return &role
}

func GetRoleBinding(
	cr *robotv1alpha1.RobotRBAC,
	role rbacv1.Role,
	serviceAccount corev1.ServiceAccount,
) *rbacv1.RoleBinding {

	roleBinding := rbacv1.RoleBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetRoleBindingMetadata().Name,
			Namespace: cr.GetRoleBindingMetadata().Namespace,
		},
		Subjects: []rbacv1.Subject{
			{
				Kind:      serviceAccount.GroupVersionKind().Kind,
				Name:      serviceAccount.Name,
				Namespace: serviceAccount.Namespace,
			},
		},
		RoleRef: rbacv1.RoleRef{
			Kind:     role.GroupVersionKind().Kind,
			Name:     role.Name,
			APIGroup: role.GroupVersionKind().Group,
		},
	}

	return &roleBinding
}
