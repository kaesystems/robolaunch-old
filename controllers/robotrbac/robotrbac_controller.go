package robotrbac

import (
	"context"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
)

// RobotRBACReconciler reconciles a RobotRBAC object
type RobotRBACReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotrbacs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotrbacs/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotrbacs/finalizers,verbs=update

//+kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=roles,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=rolebindings,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=serviceaccounts,verbs=get;list;watch;create;update;patch;delete

//+kubebuilder:rbac:groups=core,resources=pods/log,verbs=get;list

var logger logr.Logger

func (r *RobotRBACReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger = log.FromContext(ctx)

	instance, err := r.reconcileGetInstance(ctx, req.NamespacedName)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckOwnedResources(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *RobotRBACReconciler) reconcileCheckStatus(ctx context.Context, instance *robotv1alpha1.RobotRBAC) error {

	switch instance.Status.ServiceAccountStatus.Created {
	case true:

		switch instance.Status.RoleStatus.Created {
		case true:

			switch instance.Status.RoleBindingStatus.Created {
			case true:

				instance.Status.Phase = robotv1alpha1.RobotRBACPhaseReady

			case false:
				err := r.createRoleBinding(ctx, instance)
				if err != nil {
					return err
				}
			}

		case false:
			err := r.createRole(ctx, instance)
			if err != nil {
				return err
			}
		}

	case false:
		err := r.createServiceAccount(ctx, instance)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *RobotRBACReconciler) reconcileCheckOwnedResources(ctx context.Context, instance *robotv1alpha1.RobotRBAC) error {

	serviceAccountQuery := &corev1.ServiceAccount{}
	err := r.Get(ctx, *instance.GetServiceAccountMetadata(), serviceAccountQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.ServiceAccountStatus = robotv1alpha1.RBACResourceStatus{}
	} else if err != nil {
		return err
	} else {
		instance.Status.ServiceAccountStatus.Created = true
	}

	roleQuery := &rbacv1.Role{}
	err = r.Get(ctx, *instance.GetRoleMetadata(), roleQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.RoleStatus = robotv1alpha1.RBACResourceStatus{}
	} else if err != nil {
		return err
	} else {
		instance.Status.RoleStatus.Created = true
	}

	roleBindingQuery := &rbacv1.RoleBinding{}
	err = r.Get(ctx, *instance.GetRoleBindingMetadata(), roleBindingQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.RoleBindingStatus = robotv1alpha1.RBACResourceStatus{}
	} else if err != nil {
		return err
	} else {
		instance.Status.RoleBindingStatus.Created = true
	}

	return nil
}

func (r *RobotRBACReconciler) reconcileUpdateInstanceStatus(ctx context.Context, instance *robotv1alpha1.RobotRBAC) error {
	return retry.RetryOnConflict(retry.DefaultRetry, func() error {
		instanceLV := &robotv1alpha1.RobotRBAC{}
		err := r.Get(ctx, types.NamespacedName{
			Name:      instance.Name,
			Namespace: instance.Namespace,
		}, instanceLV)

		if err == nil {
			instance.ResourceVersion = instanceLV.ResourceVersion
		}

		err1 := r.Status().Update(ctx, instance)
		return err1
	})
}

func (r *RobotRBACReconciler) reconcileGetInstance(ctx context.Context, meta types.NamespacedName) (*robotv1alpha1.RobotRBAC, error) {
	instance := &robotv1alpha1.RobotRBAC{}
	err := r.Get(ctx, meta, instance)
	if err != nil {
		return &robotv1alpha1.RobotRBAC{}, err
	}
	return instance, nil
}

func (r *RobotRBACReconciler) reconcileGetOwner(ctx context.Context, instance *robotv1alpha1.RobotRBAC) (*robotv1alpha1.RobotConfig, error) {
	robotconfig := &robotv1alpha1.RobotConfig{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robotconfig)
	if err != nil {
		return nil, err
	}

	return robotconfig, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *RobotRBACReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&robotv1alpha1.RobotRBAC{}).
		Owns(&corev1.ServiceAccount{}).
		Owns(&rbacv1.Role{}).
		Owns(&rbacv1.RoleBinding{}).
		Complete(r)
}
