package robotrbac

import (
	"context"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robotrbac/pkg/spawn"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	ctrl "sigs.k8s.io/controller-runtime"
)

func (r *RobotRBACReconciler) createServiceAccount(ctx context.Context, instance *robotv1alpha1.RobotRBAC) error {

	instance.Status.Phase = robotv1alpha1.RobotRBACPhaseCreatingServiceAccount

	serviceAccount := spawn.GetServiceAccount(instance)

	err := ctrl.SetControllerReference(instance, serviceAccount, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, serviceAccount)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Service account is created.")
	instance.Status.ServiceAccountStatus.Created = true

	return nil
}

func (r *RobotRBACReconciler) createRole(ctx context.Context, instance *robotv1alpha1.RobotRBAC) error {

	instance.Status.Phase = robotv1alpha1.RobotRBACPhaseCreatingRole

	role := spawn.GetRole(instance)

	err := ctrl.SetControllerReference(instance, role, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, role)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Role is created.")
	instance.Status.RoleStatus.Created = true

	return nil
}

func (r *RobotRBACReconciler) createRoleBinding(ctx context.Context, instance *robotv1alpha1.RobotRBAC) error {

	serviceAccount := &corev1.ServiceAccount{}
	err := r.Get(ctx, *instance.GetServiceAccountMetadata(), serviceAccount)
	if err != nil {
		return err
	}

	role := &rbacv1.Role{}
	err = r.Get(ctx, *instance.GetRoleMetadata(), role)
	if err != nil {
		return err
	}

	instance.Status.Phase = robotv1alpha1.RobotRBACPhaseCreatingRoleBinding

	roleBinding := spawn.GetRoleBinding(instance, *role, *serviceAccount)

	err = ctrl.SetControllerReference(instance, roleBinding, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, roleBinding)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Role binding is created.")
	instance.Status.RoleBindingStatus.Created = true

	return nil
}
