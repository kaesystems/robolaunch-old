package spawn

import (
	"strconv"
	"strings"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/pkg/helpers"
	"github.com/robolaunch/robot-operator/controllers/pkg/volume"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

var vdiSelector = map[string]string{
	"tools": "vdi",
}

func GetVDIPod(
	cr *robotv1alpha1.VDI,
	robot *robotv1alpha1.Robot,
	robotConfig *robotv1alpha1.RobotConfig,
	artifacts robotv1alpha1.Artifacts,
	robotClone *robotv1alpha1.RobotClone,
	robotBuild *robotv1alpha1.RobotBuild,
) *corev1.Pod {

	shareProcessNamespace := false

	robotDef := robot.Spec.Robot

	// add tcp port
	ports := []corev1.ContainerPort{
		{
			Name:          "http",
			ContainerPort: 8055,
			Protocol:      corev1.ProtocolTCP,
		},
	}

	// add udp ports
	rangeLimits := strings.Split(cr.Spec.Config.Ports.UDP.Range, "-")
	rangeStart, _ := strconv.Atoi(rangeLimits[0])
	rangeEnd, _ := strconv.Atoi(rangeLimits[1])

	counter := 0
	for i := rangeStart; i <= rangeEnd; i++ {
		counter++
		ports = append(ports, corev1.ContainerPort{
			Name:          "webrtc-" + strconv.Itoa(counter),
			ContainerPort: int32(i),
			Protocol:      corev1.ProtocolUDP,
		})
	}

	vdiPod := corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetVDIPodMetadata().Name,
			Namespace: cr.GetVDIPodMetadata().Namespace,
			Labels:    vdiSelector,
		},
		Spec: corev1.PodSpec{
			ShareProcessNamespace: &shareProcessNamespace,
			Containers: []corev1.Container{
				{
					Name:    artifacts.VDIContainer.Name,
					Image:   artifacts.VDIContainer.Image,
					Command: artifacts.VDIContainer.Entrypoint,
					Env: append(artifacts.VDIContainer.Env, []corev1.EnvVar{
						helpers.Env("GAZEBO_MASTER_URI", ""),
						helpers.Env("NEKO_BIND", "8055"),
						helpers.Env("NEKO_UDP_PORT", cr.Spec.Config.Ports.UDP.Range),
					}...),
					Stdin: true,
					TTY:   true,
					Ports: ports,
					VolumeMounts: []corev1.VolumeMount{
						volume.GetVolumeMount("", volume.GetVolumeVar(robotConfig)),
						volume.GetVolumeMount("", volume.GetVolumeUsr(robotConfig)),
						volume.GetVolumeMount("", volume.GetVolumeOpt(robotConfig)),
						volume.GetVolumeMount("", volume.GetVolumeEtc(robotConfig)),
						volume.GetVolumeMount(robotv1alpha1.X11UnixPath, volume.GetVolumeEtc(robotConfig)),
						volume.GetVolumeMount(robotDef.WorkspacesPath, volume.GetVolumeWorkspace(robotClone)),
						volume.GetVolumeMount(robotv1alpha1.CustomScriptsPath, volume.GetVolumeConfigMaps(robotBuild)),
						volume.GetVolumeMount("/dev/shm", volume.GetVolumeDshm()),
						volume.GetVolumeMount("/cache", volume.GetVolumeXglCache()),
					},
					Resources: corev1.ResourceRequirements{
						Limits: getToolResourceLimits(cr.Spec.Config),
					},
					ImagePullPolicy:          corev1.PullAlways,
					TerminationMessagePolicy: corev1.TerminationMessageReadFile,
				},
			},
			Volumes: []corev1.Volume{
				volume.GetVolumeVar(robotConfig),
				volume.GetVolumeUsr(robotConfig),
				volume.GetVolumeOpt(robotConfig),
				volume.GetVolumeEtc(robotConfig),
				volume.GetVolumeX11Unix(robotConfig),
				volume.GetVolumeWorkspace(robotClone),
				volume.GetVolumeConfigMaps(robotBuild),
				volume.GetVolumeDshm(),
				volume.GetVolumeXglCache(),
			},
			RestartPolicy: corev1.RestartPolicyNever,
			NodeSelector:  robotDef.NodeSelector,
		},
	}

	return &vdiPod
}

func GetVDIService(cr *robotv1alpha1.VDI) *corev1.Service {

	ports := []corev1.ServicePort{
		{
			Port: 8055,
			TargetPort: intstr.IntOrString{
				IntVal: int32(8055),
			},
			NodePort: int32(cr.Spec.Config.Ports.TCP),
			Protocol: corev1.ProtocolTCP,
			Name:     "http",
		},
	}

	// add udp ports
	rangeLimits := strings.Split(cr.Spec.Config.Ports.UDP.Range, "-")
	rangeStart, _ := strconv.Atoi(rangeLimits[0])
	rangeEnd, _ := strconv.Atoi(rangeLimits[1])

	counter := 0
	for i := rangeStart; i <= rangeEnd; i++ {
		counter++
		ports = append(ports, corev1.ServicePort{
			Name: "webrtc-" + strconv.Itoa(counter),
			Port: int32(i),
			TargetPort: intstr.IntOrString{
				IntVal: int32(i),
			},
			NodePort: int32(i),
			Protocol: corev1.ProtocolUDP,
		})
	}

	serviceSpec := corev1.ServiceSpec{
		Type:                  corev1.ServiceTypeNodePort,
		ExternalTrafficPolicy: corev1.ServiceExternalTrafficPolicyTypeLocal,
		Selector:              vdiSelector,
		Ports:                 ports,
	}

	service := corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetVDIServiceMetadata().Name,
			Namespace: cr.GetVDIServiceMetadata().Namespace,
		},
		Spec: serviceSpec,
	}

	return &service
}

func getToolResourceLimits(tc robotv1alpha1.ToolConfig) corev1.ResourceList {
	resourceLimits := corev1.ResourceList{}
	if tc.Resources.GPUCore != 0 {
		resourceLimits["nvidia.com/gpu"] = resource.MustParse(strconv.Itoa(tc.Resources.GPUCore))
	}
	if tc.Resources.CPU != "" {
		resourceLimits["cpu"] = resource.MustParse(tc.Resources.CPU)
	}
	if tc.Resources.Memory != "" {
		resourceLimits["memory"] = resource.MustParse(tc.Resources.Memory)
	}

	return resourceLimits
}
