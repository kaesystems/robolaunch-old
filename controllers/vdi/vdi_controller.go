package vdi

import (
	"context"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/vdi/pkg/spawn"
)

type VDIReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=vdis,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=vdis/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=vdis/finalizers,verbs=update

//+kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete

var logger logr.Logger

func (r *VDIReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger = log.FromContext(ctx)

	instance, err := r.reconcileGetInstance(ctx, req.NamespacedName)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckOwnedResources(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *VDIReconciler) reconcileGetInstance(ctx context.Context, meta types.NamespacedName) (*robotv1alpha1.VDI, error) {
	instance := &robotv1alpha1.VDI{}
	err := r.Get(ctx, meta, instance)
	if err != nil {
		return &robotv1alpha1.VDI{}, err
	}

	return instance, nil
}

func (r *VDIReconciler) reconcileCheckStatus(ctx context.Context, instance *robotv1alpha1.VDI) error {

	switch instance.Status.VDIPodStatus.Created {
	case false:

		robot, err := r.reconcileGetRobot(ctx, instance)
		if err != nil {
			return err
		}

		robotConfig, err := r.reconcileGetRobotConfig(ctx, instance)
		if err != nil {
			return err
		}

		robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
		if err != nil {
			return err
		}

		robotClone, err := r.reconcileGetRobotClone(ctx, instance)
		if err != nil {
			return err
		}

		robotBuild, err := r.reconcileGetRobotBuild(ctx, instance)
		if err != nil {
			return err
		}

		vdiPod := spawn.GetVDIPod(
			instance,
			robot,
			robotConfig,
			robotArtifact.Artifacts,
			robotClone,
			robotBuild,
		)

		err = ctrl.SetControllerReference(instance, vdiPod, r.Scheme)
		if err != nil {
			return err
		}

		err = r.Create(ctx, vdiPod)
		if err != nil {
			return err
		}

		logger.Info("STATUS: VDI pod is created.")
		instance.Status.VDIPodStatus.Created = true
	}

	switch instance.Status.VDIServiceStatus.Created {
	case false:

		vdiService := spawn.GetVDIService(instance)

		err := ctrl.SetControllerReference(instance, vdiService, r.Scheme)
		if err != nil {
			return err
		}

		err = r.Create(ctx, vdiService)
		if err != nil {
			return err
		}

		logger.Info("STATUS: VDI service is created.")
		instance.Status.VDIServiceStatus.Created = true
	}

	if instance.Status.VDIPodStatus.Phase == corev1.PodRunning && instance.Status.VDIServiceStatus.Created {
		instance.Status.Phase = robotv1alpha1.VDIPhaseReady
	}

	return nil
}

func (r *VDIReconciler) reconcileCheckOwnedResources(ctx context.Context, instance *robotv1alpha1.VDI) error {

	instance.Status = robotv1alpha1.VDIStatus{}

	vdiPodQuery := &corev1.Pod{}
	err := r.Get(ctx, *instance.GetVDIPodMetadata(), vdiPodQuery)
	if err != nil {
		if errors.IsNotFound(err) {
			instance.Status.VDIPodStatus = robotv1alpha1.VDIPodStatus{}
		} else {
			return err
		}
	} else {
		instance.Status.VDIPodStatus.Created = true
		instance.Status.VDIPodStatus.Phase = vdiPodQuery.Status.Phase
		instance.Status.VDIPodStatus.IP = vdiPodQuery.Status.PodIP
	}

	serviceQuery := &corev1.Service{}
	err = r.Get(ctx, *instance.GetVDIServiceMetadata(), serviceQuery)
	if err != nil {
		if errors.IsNotFound(err) {
			instance.Status.VDIServiceStatus.Created = false
		} else {
			return err
		}
	} else {
		instance.Status.VDIServiceStatus.Created = true
	}

	return nil
}

func (r *VDIReconciler) reconcileUpdateInstanceStatus(ctx context.Context, instance *robotv1alpha1.VDI) error {
	return retry.RetryOnConflict(retry.DefaultRetry, func() error {
		instanceLV := &robotv1alpha1.VDI{}
		err := r.Get(ctx, types.NamespacedName{
			Name:      instance.Name,
			Namespace: instance.Namespace,
		}, instanceLV)

		if err == nil {
			instance.ResourceVersion = instanceLV.ResourceVersion
		}

		err1 := r.Status().Update(ctx, instance)
		return err1
	})
}

func (r *VDIReconciler) reconcileGetOwner(ctx context.Context, instance *robotv1alpha1.VDI) (*robotv1alpha1.RobotTools, error) {
	robottools := &robotv1alpha1.RobotTools{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robottools)
	if err != nil {
		return nil, err
	}

	return robottools, nil
}

func (r *VDIReconciler) reconcileGetArtifact(ctx context.Context, instance *robotv1alpha1.VDI) (*robotv1alpha1.RobotArtifact, error) {
	robot, err := r.reconcileGetRobot(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotartifact := &robotv1alpha1.RobotArtifact{}
	err = r.Get(ctx, types.NamespacedName{
		Namespace: robot.GetRobotArtifactMetadata().Namespace,
		Name:      robot.GetRobotArtifactMetadata().Name,
	}, robotartifact)
	if err != nil {
		return nil, err
	}

	return robotartifact, nil
}

func (r *VDIReconciler) reconcileGetRobot(ctx context.Context, instance *robotv1alpha1.VDI) (*robotv1alpha1.Robot, error) {
	robottools, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return nil, err
	}

	robot := &robotv1alpha1.Robot{}
	err = r.Get(ctx, *robottools.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	return robot, nil
}

func (r *VDIReconciler) reconcileGetRobotConfig(ctx context.Context, instance *robotv1alpha1.VDI) (*robotv1alpha1.RobotConfig, error) {

	robot, err := r.reconcileGetRobot(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotConfig := &robotv1alpha1.RobotConfig{}
	err = r.Get(ctx, *robot.GetRobotConfigMetadata(), robotConfig)
	if err != nil {
		return nil, err
	}

	return robotConfig, nil
}

func (r *VDIReconciler) reconcileGetRobotClone(ctx context.Context, instance *robotv1alpha1.VDI) (*robotv1alpha1.RobotClone, error) {

	robot, err := r.reconcileGetRobot(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotClone := &robotv1alpha1.RobotClone{}
	err = r.Get(ctx, *robot.GetRobotCloneMetadata(), robotClone)
	if err != nil {
		return nil, err
	}

	return robotClone, nil
}

func (r *VDIReconciler) reconcileGetRobotBuild(ctx context.Context, instance *robotv1alpha1.VDI) (*robotv1alpha1.RobotBuild, error) {

	robot, err := r.reconcileGetRobot(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotBuild := &robotv1alpha1.RobotBuild{}
	err = r.Get(ctx, *robot.GetRobotBuildMetadata(), robotBuild)
	if err != nil {
		return nil, err
	}

	return robotBuild, nil
}

func (r *VDIReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&robotv1alpha1.VDI{}).
		Owns(&corev1.Pod{}).
		Owns(&corev1.Service{}).
		Complete(r)
}
