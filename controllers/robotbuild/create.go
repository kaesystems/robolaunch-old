package robotbuild

import (
	"context"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robotbuild/pkg/spawn"
	ctrl "sigs.k8s.io/controller-runtime"
)

func (r *RobotBuildReconciler) createScriptConfigMap(
	ctx context.Context,
	instance *robotv1alpha1.RobotBuild,
) error {
	instance.Status.Phase = robotv1alpha1.RobotBuildPhaseCreatingConfigMap

	scriptConfigMap, err := spawn.GetConfigMap(instance)
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, scriptConfigMap, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, scriptConfigMap)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Script ConfigMap is created.")
	instance.Status.ScriptConfigMapStatus.Created = true
	return nil
}

func (r *RobotBuildReconciler) createBuilderJob(
	ctx context.Context,
	instance *robotv1alpha1.RobotBuild,
	jobKey string,
) error {

	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return err
	}

	robotConfig, err := r.reconcileGetRobotConfig(ctx, instance)
	if err != nil {
		return err
	}

	robotClone, err := r.reconcileGetRobotClone(ctx, instance)
	if err != nil {
		return err
	}

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	job := spawn.GetBuildJob(
		instance,
		robot,
		&robotArtifact.Artifacts,
		robotConfig,
		robotClone,
		instance.Status.Steps[jobKey].Step,
	)

	err = ctrl.SetControllerReference(instance, job, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, job)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Builder job " + instance.Status.Steps[jobKey].JobName + " is started.")

	step := instance.Status.Steps[jobKey]
	step.JobCreated = true
	instance.Status.Steps[jobKey] = step

	return nil
}
