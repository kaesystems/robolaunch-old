package spawn

import (
	"path/filepath"

	"github.com/robolaunch/robot-operator/controllers/pkg/helpers"
	"github.com/robolaunch/robot-operator/controllers/pkg/volume"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
)

func GetBuildJob(
	cr *robotv1alpha1.RobotBuild,
	robot *robotv1alpha1.Robot,
	artifacts *robotv1alpha1.Artifacts,
	robotConfig *robotv1alpha1.RobotConfig,
	robotClone *robotv1alpha1.RobotClone,
	step robotv1alpha1.Step,
) *batchv1.Job {

	robotDef := robot.Spec.Robot

	var cmd []string
	if step.Command != "" {
		cmd = helpers.Bash(step.Command)

	} else {
		cmd = helpers.Bash(filepath.Join(robotv1alpha1.CustomScriptsPath, "scripts", step.Name))
	}

	var backoffLimit int32 = 1

	job := batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      step.Name,
			Namespace: cr.Namespace,
		},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:    step.Name,
							Image:   string(artifacts.ROS.BuilderImage),
							Command: cmd,
							VolumeMounts: []corev1.VolumeMount{
								volume.GetVolumeMount("", volume.GetVolumeVar(robotConfig)),
								volume.GetVolumeMount("", volume.GetVolumeUsr(robotConfig)),
								volume.GetVolumeMount("", volume.GetVolumeOpt(robotConfig)),
								volume.GetVolumeMount("", volume.GetVolumeEtc(robotConfig)),
								volume.GetVolumeMount(robotDef.WorkspacesPath, volume.GetVolumeWorkspace(robotClone)),
								volume.GetVolumeMount(robotv1alpha1.CustomScriptsPath, volume.GetVolumeConfigMaps(cr)),
							},
							Env: append(artifacts.JobContainer.Env, step.Env...),
						},
					},
					Volumes: []corev1.Volume{
						volume.GetVolumeVar(robotConfig),
						volume.GetVolumeUsr(robotConfig),
						volume.GetVolumeOpt(robotConfig),
						volume.GetVolumeEtc(robotConfig),
						volume.GetVolumeWorkspace(robotClone),
						volume.GetVolumeConfigMaps(cr),
					},
					RestartPolicy: corev1.RestartPolicyNever,
					NodeSelector:  robot.Spec.Robot.NodeSelector,
				},
			},
			BackoffLimit: &backoffLimit,
		},
	}

	return &job
}

func GetConfigMap(cr *robotv1alpha1.RobotBuild) (*corev1.ConfigMap, error) {

	configScripts := func() map[string]string {
		configScripts := map[string]string{}

		for _, step := range cr.Spec.Steps {
			if step.Script != "" {
				configScripts[step.Name] = step.Script
			}
		}

		return configScripts
	}()

	configMap := corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetConfigMapMetadata().Name,
			Namespace: cr.GetConfigMapMetadata().Namespace,
		},
		Data: configScripts,
	}

	return &configMap, nil
}
