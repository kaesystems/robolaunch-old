package robotbuild

import (
	"context"
	"sort"

	"github.com/go-logr/logr"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/pkg/reconciler"
)

type RobotBuildReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotbuilds,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotbuilds/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotbuilds/finalizers,verbs=update

//+kubebuilder:rbac:groups=core,resources=persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=configmaps,verbs=get;list;watch;create;update;patch;delete

var logger logr.Logger
var defaultReturnResult ctrl.Result

func (r *RobotBuildReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger = log.FromContext(ctx)
	defaultReturnResult = ctrl.Result{}

	instance, err := r.reconcileGetInstance(ctx, req.NamespacedName)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckOwnedResources(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *RobotBuildReconciler) reconcileCheckStatus(ctx context.Context, instance *robotv1alpha1.RobotBuild) error {

	switch instance.Status.ScriptConfigMapStatus.Created {
	case true:

		instance.Status.Phase = robotv1alpha1.RobotBuildPhaseBuildingRobot

		keys := []string{}
		for key := range instance.Status.Steps {
			keys = append(keys, key)
		}

		sort.Strings(keys)

		for _, k := range keys {
			if instance.Status.Steps[k].JobCreated {

				if instance.Status.Steps[k].JobPhase == robotv1alpha1.JobSucceeded {
					continue
				} else if instance.Status.Steps[k].JobPhase == robotv1alpha1.JobActive {
					reconciler.Requeue(&defaultReturnResult)
					break
				} else if instance.Status.Steps[k].JobPhase == robotv1alpha1.JobFailed {
					reconciler.Requeue(&defaultReturnResult)
					break
				} else {
					reconciler.Requeue(&defaultReturnResult)
					break
				}

			} else {

				err := r.createBuilderJob(ctx, instance, k)
				if err != nil {
					return err
				}

				break
			}
		}

		areJobsSucceeded := false

		for _, key := range keys {
			if instance.Status.Steps[key].JobPhase == robotv1alpha1.JobSucceeded {
				areJobsSucceeded = true
			} else {
				areJobsSucceeded = false
				break
			}
		}

		if areJobsSucceeded {
			instance.Status.Phase = robotv1alpha1.RobotBuildPhaseReady
		}

	case false:
		err := r.createScriptConfigMap(ctx, instance)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *RobotBuildReconciler) reconcileCheckOwnedResources(ctx context.Context, instance *robotv1alpha1.RobotBuild) error {

	phase := instance.Status.Phase
	instance.Status = robotv1alpha1.RobotBuildStatus{}
	instance.Status.Phase = phase

	configMapQuery := &corev1.ConfigMap{}
	err := r.Get(ctx, *instance.GetConfigMapMetadata(), configMapQuery)
	if err != nil {
		if errors.IsNotFound(err) {
			instance.Status.ScriptConfigMapStatus.Created = false
		} else {
			return err
		}
	} else {
		instance.Status.ScriptConfigMapStatus.Created = true
	}

	stepStatuses := make(map[string]robotv1alpha1.StepStatus)
	for _, step := range instance.Spec.Steps {
		jobMetadata := types.NamespacedName{
			Namespace: instance.Namespace,
			Name:      step.Name,
		}

		jobQuery := &batchv1.Job{}
		err = r.Get(ctx, jobMetadata, jobQuery)
		if err != nil {
			if errors.IsNotFound(err) {
				stepStatus := robotv1alpha1.StepStatus{
					Step:       step,
					JobName:    jobMetadata.Name,
					JobCreated: false,
				}

				stepStatuses[step.Name] = stepStatus
			} else {
				return err
			}
		} else {
			var jobPhase robotv1alpha1.JobPhase
			switch 1 {
			case int(jobQuery.Status.Succeeded):
				jobPhase = robotv1alpha1.JobSucceeded
			case int(jobQuery.Status.Active):
				jobPhase = robotv1alpha1.JobActive
			case int(jobQuery.Status.Failed):
				jobPhase = robotv1alpha1.JobFailed
			}

			stepStatus := robotv1alpha1.StepStatus{
				Step:       step,
				JobName:    jobMetadata.Name,
				JobCreated: true,
				JobPhase:   jobPhase,
			}

			stepStatuses[step.Name] = stepStatus
		}
	}

	instance.Status.Steps = stepStatuses

	return nil
}

func (r *RobotBuildReconciler) reconcileGetInstance(ctx context.Context, meta types.NamespacedName) (*robotv1alpha1.RobotBuild, error) {
	instance := &robotv1alpha1.RobotBuild{}
	err := r.Get(ctx, meta, instance)
	if err != nil {
		return &robotv1alpha1.RobotBuild{}, err
	}
	return instance, nil
}

func (r *RobotBuildReconciler) reconcileUpdateInstanceStatus(ctx context.Context, instance *robotv1alpha1.RobotBuild) error {
	return retry.RetryOnConflict(retry.DefaultRetry, func() error {
		instanceLV := &robotv1alpha1.RobotBuild{}
		err := r.Get(ctx, types.NamespacedName{
			Name:      instance.Name,
			Namespace: instance.Namespace,
		}, instanceLV)

		if err == nil {
			instance.ResourceVersion = instanceLV.ResourceVersion
		}

		err1 := r.Status().Update(ctx, instance)
		return err1
	})
}

func (r *RobotBuildReconciler) reconcileGetRobotConfig(ctx context.Context, instance *robotv1alpha1.RobotBuild) (*robotv1alpha1.RobotConfig, error) {
	robot := &robotv1alpha1.Robot{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	robotconfig := &robotv1alpha1.RobotConfig{}
	err = r.Get(ctx, *robot.GetRobotConfigMetadata(), robotconfig)
	if err != nil {
		return nil, err
	}

	return robotconfig, nil
}

func (r *RobotBuildReconciler) reconcileGetRobotClone(ctx context.Context, instance *robotv1alpha1.RobotBuild) (*robotv1alpha1.RobotClone, error) {
	robot := &robotv1alpha1.Robot{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	robotclone := &robotv1alpha1.RobotClone{}
	err = r.Get(ctx, *robot.GetRobotCloneMetadata(), robotclone)
	if err != nil {
		return nil, err
	}

	return robotclone, nil
}

func (r *RobotBuildReconciler) reconcileGetOwner(ctx context.Context, instance *robotv1alpha1.RobotBuild) (*robotv1alpha1.Robot, error) {
	robot := &robotv1alpha1.Robot{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	return robot, nil
}

func (r *RobotBuildReconciler) reconcileGetArtifact(ctx context.Context, instance *robotv1alpha1.RobotBuild) (*robotv1alpha1.RobotArtifact, error) {
	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotartifact := &robotv1alpha1.RobotArtifact{}
	err = r.Get(ctx, types.NamespacedName{
		Namespace: robot.GetRobotArtifactMetadata().Namespace,
		Name:      robot.GetRobotArtifactMetadata().Name,
	}, robotartifact)
	if err != nil {
		return nil, err
	}

	return robotartifact, nil
}

func (r *RobotBuildReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&robotv1alpha1.RobotBuild{}).
		Owns(&batchv1.Job{}).
		Owns(&corev1.ConfigMap{}).
		Complete(r)
}
