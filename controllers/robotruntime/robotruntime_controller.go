package robotruntime

import (
	"context"
	"time"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robotruntime/pkg/check"
)

type RobotRuntimeReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotruntimes,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotruntimes/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotruntimes/finalizers,verbs=update

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotlaunchs,verbs=get;list;watch;create;update;patch;delete

var logger logr.Logger
var defaultReturnResult ctrl.Result

func (r *RobotRuntimeReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger = log.FromContext(ctx)
	defaultReturnResult = ctrl.Result{}

	instance, err := r.reconcileGetInstance(ctx, req.NamespacedName)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	if instance.Status.Phase == robotv1alpha1.RobotRuntimePhaseMalfunctioned {
		logger.Info("resource is malfunctioned, creating again")
		return ctrl.Result{
			Requeue:      true,
			RequeueAfter: 3 * time.Second,
		}, nil
	}

	err = r.reconcileCheckStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	if check.IsMalfunctioned(instance) {
		instance.Status.Phase = robotv1alpha1.RobotRuntimePhaseMalfunctioned
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckOwnedResources(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	if check.IsMalfunctioned(instance) {
		instance.Status.Phase = robotv1alpha1.RobotRuntimePhaseMalfunctioned
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	return defaultReturnResult, nil
}

func (r *RobotRuntimeReconciler) reconcileGetInstance(ctx context.Context, meta types.NamespacedName) (*robotv1alpha1.RobotRuntime, error) {
	instance := &robotv1alpha1.RobotRuntime{}
	err := r.Get(ctx, meta, instance)
	if err != nil {
		return &robotv1alpha1.RobotRuntime{}, err
	}

	return instance, nil
}

func (r *RobotRuntimeReconciler) reconcileGetOwner(ctx context.Context, instance *robotv1alpha1.RobotRuntime) (*robotv1alpha1.Robot, error) {
	robot := &robotv1alpha1.Robot{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	return robot, nil
}

func (r *RobotRuntimeReconciler) reconcileGetArtifact(ctx context.Context, instance *robotv1alpha1.RobotRuntime) (*robotv1alpha1.RobotArtifact, error) {
	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotartifact := &robotv1alpha1.RobotArtifact{}
	err = r.Get(ctx, types.NamespacedName{
		Namespace: robot.GetRobotArtifactMetadata().Namespace,
		Name:      robot.GetRobotArtifactMetadata().Name,
	}, robotartifact)
	if err != nil {
		return nil, err
	}

	return robotartifact, nil
}

func (r *RobotRuntimeReconciler) reconcileGetRobotTools(ctx context.Context, instance *robotv1alpha1.RobotRuntime) (*robotv1alpha1.RobotTools, error) {
	robot := &robotv1alpha1.Robot{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	robottools := &robotv1alpha1.RobotTools{}
	err = r.Get(ctx, *robot.GetRobotToolsMetadata(), robottools)
	if err != nil {
		return nil, err
	}

	return robottools, nil
}

func (r *RobotRuntimeReconciler) reconcileCheckStatus(ctx context.Context, instance *robotv1alpha1.RobotRuntime) error {

	switch instance.Status.LaunchStatus.Created {
	case true:

		// launch is created, robotruntime is ready
		instance.Status.Phase = robotv1alpha1.RobotRuntimePhaseReady

	case false:

		err := r.createRobotLaunch(ctx, instance)
		if err != nil {
			return err
		}

	}

	return nil
}

func (r *RobotRuntimeReconciler) reconcileCheckOwnedResources(ctx context.Context, instance *robotv1alpha1.RobotRuntime) error {

	robotLaunchQuery := &robotv1alpha1.RobotLaunch{}
	err := r.Get(ctx, *instance.GetRobotLaunchMetadata(), robotLaunchQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.LaunchStatus = robotv1alpha1.LaunchStatus{}
	} else if err != nil {
		return err
	} else {

		// if !reflect.DeepEqual(instance.Spec.Robot, robotLaunchQuery.Spec.Robot) {
		// 	robotLaunchQuery.Spec.Robot = instance.Spec.Robot
		// 	err = r.Update(ctx, robotLaunchQuery)
		// 	if err != nil {
		// 		return err
		// 	}
		// }

		instance.Status.LaunchStatus.Created = true
		instance.Status.LaunchStatus.Status = robotLaunchQuery.Status
	}

	return nil
}

func (r *RobotRuntimeReconciler) reconcileUpdateInstanceStatus(ctx context.Context, instance *robotv1alpha1.RobotRuntime) error {
	return retry.RetryOnConflict(retry.DefaultRetry, func() error {
		instanceLV := &robotv1alpha1.RobotRuntime{}
		err := r.Get(ctx, types.NamespacedName{
			Name:      instance.Name,
			Namespace: instance.Namespace,
		}, instanceLV)

		if err == nil {
			instance.ResourceVersion = instanceLV.ResourceVersion
		}

		err1 := r.Status().Update(ctx, instance)
		return err1
	})
}

func (r *RobotRuntimeReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&robotv1alpha1.RobotRuntime{}).
		Owns(&corev1.PersistentVolumeClaim{}).
		Owns(&corev1.Pod{}).
		Owns(&robotv1alpha1.RobotLaunch{}).
		Complete(r)
}
