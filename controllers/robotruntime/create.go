package robotruntime

import (
	"context"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robotruntime/pkg/spawn"
	corev1 "k8s.io/api/core/v1"
	ctrl "sigs.k8s.io/controller-runtime"
)

func (r *RobotRuntimeReconciler) createRobotLaunch(
	ctx context.Context,
	instance *robotv1alpha1.RobotRuntime,
) error {

	instance.Status.Phase = robotv1alpha1.RobotRuntimePhaseCreatingRobotLaunch

	robotTools, err := r.reconcileGetRobotTools(ctx, instance)
	if err != nil {
		return err
	}

	switch robotTools.Status.TrackerPodStatus.Phase {
	case corev1.PodRunning:

		robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
		if err != nil {
			return err
		}

		robotLaunch := spawn.GetLaunch(instance, robotArtifact.Artifacts, robotTools)
		err = ctrl.SetControllerReference(instance, robotLaunch, r.Scheme)
		if err != nil {
			return err
		}

		err = r.Create(ctx, robotLaunch)
		if err != nil {
			return err
		}

		logger.Info("STATUS: Robot launch is created.")
		instance.Status.LaunchStatus.Created = true

	default:
		// pod is not running
	}

	return nil
}
