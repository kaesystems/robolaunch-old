package spawn

import (
	"strconv"
	"time"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetLaunch(
	cr *robotv1alpha1.RobotRuntime,
	artifacts robotv1alpha1.Artifacts,
	robotTools *robotv1alpha1.RobotTools,
) *robotv1alpha1.RobotLaunch {

	launchSpec := robotv1alpha1.RobotLaunchSpec{
		LastLaunchTimestamp: time.Now().Format(time.RFC3339),
		TrackerInfo: robotv1alpha1.TrackerInfo{
			IP:   robotTools.Status.TrackerPodStatus.IP,
			Port: strconv.Itoa(int(artifacts.TrackerContainer.Ports["tracker"].ContainerPort)),
		},
	}

	robotLaunch := robotv1alpha1.RobotLaunch{
		ObjectMeta: metav1.ObjectMeta{
			Name:            cr.GetRobotLaunchMetadata().Name,
			Namespace:       cr.GetRobotLaunchMetadata().Namespace,
			ResourceVersion: "",
		},
		Spec: launchSpec,
	}

	return &robotLaunch
}
