package check

import (
	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
)

func IsMalfunctioned(cr *robotv1alpha1.RobotRuntime) bool {
	switch cr.Status.Phase {

	case robotv1alpha1.RobotRuntimePhaseReady:

		if !cr.Status.LaunchStatus.Created {
			return true
		}

	}

	return false
}
