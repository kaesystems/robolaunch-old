package robotclone

import (
	"context"

	"github.com/go-logr/logr"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
)

type RobotCloneReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotclones,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotclones/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotclones/finalizers,verbs=update

//+kubebuilder:rbac:groups=core,resources=persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete

var logger logr.Logger

func (r *RobotCloneReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger = log.FromContext(ctx)

	instance, err := r.reconcileGetInstance(ctx, req.NamespacedName)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckStatus(ctx, instance, robot)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckOwnedResources(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *RobotCloneReconciler) reconcileCheckStatus(ctx context.Context, instance *robotv1alpha1.RobotClone, robot *robotv1alpha1.Robot) error {

	switch instance.Status.Phase {
	case robotv1alpha1.RobotClonePhaseReady:
		cloneJobQuery := &batchv1.Job{}
		err := r.Get(ctx, *instance.GetCloneJobMetadata(), cloneJobQuery)
		if err != nil && errors.IsNotFound(err) {

		} else if err != nil {
			return err
		} else {
			foreground := metav1.DeletePropagationForeground
			err = r.Delete(ctx, cloneJobQuery, &client.DeleteOptions{
				PropagationPolicy: &foreground,
			})
			if err != nil {
				return err
			}
		}
	}

	switch instance.Status.WorkspaceStatus.Created {
	case true:

		switch instance.Status.CloneJobStatus.Created {
		case true:

			switch instance.Status.CloneJobStatus.Phase {
			case robotv1alpha1.JobSucceeded:
				instance.Status.Phase = robotv1alpha1.RobotClonePhaseReady
			case robotv1alpha1.JobActive:
				instance.Status.Phase = robotv1alpha1.RobotClonePhaseCloningRepositories
			case robotv1alpha1.JobFailed:
				instance.Status.Phase = robotv1alpha1.RobotClonePhaseFailed
			}

		case false:

			err := r.createCloneJob(ctx, instance, robot)
			if err != nil {
				return err
			}
		}

	case false:
		err := r.createWorkspacePVC(ctx, instance, robot)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *RobotCloneReconciler) reconcileCheckOwnedResources(ctx context.Context, instance *robotv1alpha1.RobotClone) error {

	pvcWorkspaceQuery := &corev1.PersistentVolumeClaim{}
	err := r.Get(ctx, *instance.GetPVCWorkspaceMetadata(), pvcWorkspaceQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.WorkspaceStatus.Created = false
	} else if err != nil {
		return err
	}

	if instance.Status.Phase != robotv1alpha1.RobotClonePhaseReady {
		cloneJobQuery := &batchv1.Job{}
		err = r.Get(ctx, *instance.GetCloneJobMetadata(), cloneJobQuery)
		if err != nil && errors.IsNotFound(err) {
			instance.Status.CloneJobStatus.Created = false
		} else if err != nil {
			return err
		} else {
			switch 1 {
			case int(cloneJobQuery.Status.Succeeded):
				instance.Status.CloneJobStatus.Phase = robotv1alpha1.JobSucceeded
			case int(cloneJobQuery.Status.Active):
				instance.Status.CloneJobStatus.Phase = robotv1alpha1.JobActive
			case int(cloneJobQuery.Status.Failed):
				instance.Status.CloneJobStatus.Phase = robotv1alpha1.JobFailed
			}
		}
	}

	return nil
}

func (r *RobotCloneReconciler) reconcileGetInstance(ctx context.Context, meta types.NamespacedName) (*robotv1alpha1.RobotClone, error) {
	instance := &robotv1alpha1.RobotClone{}
	err := r.Get(ctx, meta, instance)
	if err != nil {
		return &robotv1alpha1.RobotClone{}, err
	}
	return instance, nil
}

func (r *RobotCloneReconciler) reconcileGetOwner(ctx context.Context, instance *robotv1alpha1.RobotClone) (*robotv1alpha1.Robot, error) {
	robot := &robotv1alpha1.Robot{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	return robot, nil
}

func (r *RobotCloneReconciler) reconcileGetArtifact(ctx context.Context, instance *robotv1alpha1.RobotClone) (*robotv1alpha1.RobotArtifact, error) {
	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotartifact := &robotv1alpha1.RobotArtifact{}
	err = r.Get(ctx, types.NamespacedName{
		Namespace: robot.GetRobotArtifactMetadata().Namespace,
		Name:      robot.GetRobotArtifactMetadata().Name,
	}, robotartifact)
	if err != nil {
		return nil, err
	}

	return robotartifact, nil
}

func (r *RobotCloneReconciler) reconcileGetRobotConfig(ctx context.Context, instance *robotv1alpha1.RobotClone) (*robotv1alpha1.RobotConfig, error) {
	robot := &robotv1alpha1.Robot{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	robotconfig := &robotv1alpha1.RobotConfig{}
	err = r.Get(ctx, *robot.GetRobotConfigMetadata(), robotconfig)
	if err != nil {
		return nil, err
	}

	return robotconfig, nil
}

func (r *RobotCloneReconciler) reconcileUpdateInstanceStatus(ctx context.Context, instance *robotv1alpha1.RobotClone) error {
	return retry.RetryOnConflict(retry.DefaultRetry, func() error {
		instanceLV := &robotv1alpha1.RobotClone{}
		err := r.Get(ctx, types.NamespacedName{
			Name:      instance.Name,
			Namespace: instance.Namespace,
		}, instanceLV)

		if err == nil {
			instance.ResourceVersion = instanceLV.ResourceVersion
		}

		err1 := r.Status().Update(ctx, instance)
		return err1
	})
}

func (r *RobotCloneReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&robotv1alpha1.RobotClone{}).
		Owns(&corev1.PersistentVolumeClaim{}).
		Owns(&batchv1.Job{}).
		Complete(r)
}
