package spawn

import (
	"path/filepath"
	"strconv"
	"strings"

	"github.com/robolaunch/robot-operator/controllers/pkg/helpers"
	"github.com/robolaunch/robot-operator/controllers/pkg/volume"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
)

func GetWorkspacePersistentVolumeClaim(cr *robotv1alpha1.RobotClone, robot *robotv1alpha1.Robot, artifacts robotv1alpha1.Artifacts) (*corev1.PersistentVolumeClaim, error) {

	pvc := corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetPVCWorkspaceMetadata().Name,
			Namespace: cr.GetPVCWorkspaceMetadata().Namespace,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			StorageClassName: &artifacts.StorageClassConfigs.WorkspaceVolumes.Name,
			AccessModes: []corev1.PersistentVolumeAccessMode{
				artifacts.StorageClassConfigs.WorkspaceVolumes.AccessMode,
			},
			Resources: corev1.ResourceRequirements{
				Limits: corev1.ResourceList{
					corev1.ResourceName(corev1.ResourceStorage): resource.MustParse(getClaimStorage(robot.Spec.Robot.Resources.Storage)),
				},
				Requests: corev1.ResourceList{
					corev1.ResourceName(corev1.ResourceStorage): resource.MustParse(getClaimStorage(robot.Spec.Robot.Resources.Storage)),
				},
			},
		},
	}

	return &pvc, nil
}

func getClaimStorage(totalStorage int) string {
	storageInt := totalStorage / 10
	return strconv.Itoa(storageInt) + "M"

}

func GetCloneJob(
	cr *robotv1alpha1.RobotClone,
	robot *robotv1alpha1.Robot,
	artifacts robotv1alpha1.Artifacts,
	robotConfig *robotv1alpha1.RobotConfig,
	robotBuild *robotv1alpha1.RobotBuild,
) *batchv1.Job {

	robotDef := robot.Spec.Robot

	var backoffLimit int32 = 1

	step := getCloneStep(cr, robot)
	cmd := helpers.Bash(step.Command)

	job := batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetCloneJobMetadata().Name,
			Namespace: cr.GetCloneJobMetadata().Namespace,
		},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:    step.Name,
							Image:   artifacts.JobContainer.Image,
							Command: cmd,
							VolumeMounts: []corev1.VolumeMount{
								volume.GetVolumeMount("", volume.GetVolumeVar(robotConfig)),
								volume.GetVolumeMount("", volume.GetVolumeUsr(robotConfig)),
								volume.GetVolumeMount("", volume.GetVolumeOpt(robotConfig)),
								volume.GetVolumeMount("", volume.GetVolumeEtc(robotConfig)),
								volume.GetVolumeMount(robotDef.WorkspacesPath, volume.GetVolumeWorkspace(cr)),
							},
							Env: artifacts.JobContainer.Env,
						},
					},
					Volumes: []corev1.Volume{
						volume.GetVolumeVar(robotConfig),
						volume.GetVolumeUsr(robotConfig),
						volume.GetVolumeOpt(robotConfig),
						volume.GetVolumeEtc(robotConfig),
						volume.GetVolumeWorkspace(cr),
					},
					RestartPolicy: corev1.RestartPolicyNever,
					NodeSelector:  robot.Spec.Robot.NodeSelector,
				},
			},
			BackoffLimit: &backoffLimit,
		},
	}

	return &job
}

func getCloneStep(
	cr *robotv1alpha1.RobotClone,
	robot *robotv1alpha1.Robot,
) robotv1alpha1.Step {

	robotDef := robot.Spec.Robot

	stepsSlice := []robotv1alpha1.Step{}

	for wsKey, ws := range robotDef.Workspaces {
		wsKeyStr := strconv.Itoa(wsKey)

		var cmdBuilder strings.Builder
		cmdBuilder.WriteString("mkdir -p " + filepath.Join(robotDef.WorkspacesPath, ws.Name, "src") + " && ")
		cmdBuilder.WriteString("cd " + filepath.Join(robotDef.WorkspacesPath, ws.Name, "src") + " && ")
		cmdBuilder.WriteString(robotv1alpha1.GetCloneCommand(robotDef, wsKey))

		stepsSlice = append(stepsSlice, robotv1alpha1.Step{
			Name:    wsKeyStr + "-clone",
			Command: cmdBuilder.String(),
		})
	}

	singleStep := robotv1alpha1.Step{
		Name: "clone",
	}

	var cmdBuilder strings.Builder

	for index, step := range stepsSlice {

		cmdBuilder.WriteString(step.Command)

		if index != len(stepsSlice)-1 {
			cmdBuilder.WriteString(" && ")
		}
	}

	singleStep.Command = cmdBuilder.String()

	return singleStep
}
