package robotclone

import (
	"context"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robotclone/pkg/spawn"
	ctrl "sigs.k8s.io/controller-runtime"
)

func (r *RobotCloneReconciler) createWorkspacePVC(
	ctx context.Context,
	instance *robotv1alpha1.RobotClone,
	robot *robotv1alpha1.Robot,
) error {

	instance.Status.Phase = robotv1alpha1.RobotClonePhaseCreatingWorkspace

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	pvcWorkspace, err := spawn.GetWorkspacePersistentVolumeClaim(instance, robot, robotArtifact.Artifacts)
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, pvcWorkspace, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, pvcWorkspace)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Volume for workspace is created.")
	instance.Status.WorkspaceStatus.Created = true

	return nil
}

func (r *RobotCloneReconciler) createCloneJob(
	ctx context.Context,
	instance *robotv1alpha1.RobotClone,
	robot *robotv1alpha1.Robot,
) error {
	instance.Status.Phase = robotv1alpha1.RobotClonePhaseCloningRepositories

	robotConfig, err := r.reconcileGetRobotConfig(ctx, instance)
	if err != nil {
		return err
	}

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	cloneJob := spawn.GetCloneJob(instance, robot, robotArtifact.Artifacts, robotConfig, &robotv1alpha1.RobotBuild{})
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, cloneJob, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, cloneJob)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Clone job is started.")
	instance.Status.CloneJobStatus.Created = true

	return nil
}
