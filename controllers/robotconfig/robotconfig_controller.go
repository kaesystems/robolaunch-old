package robotconfig

import (
	"context"
	"time"

	"github.com/go-logr/logr"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robotconfig/pkg/check"
)

type RobotConfigReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotconfigs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotconfigs/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotconfigs/finalizers,verbs=update

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robotrbacs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=events,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete

var logger logr.Logger

func (r *RobotConfigReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger = log.FromContext(ctx)

	instance, err := r.reconcileGetInstance(ctx, req.NamespacedName)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	if instance.Status.Phase == robotv1alpha1.RobotConfigPhaseMalfunctioned {
		logger.Info("resource is malfunctioned, creating again")
		return ctrl.Result{
			Requeue:      true,
			RequeueAfter: 3 * time.Second,
		}, nil
	}

	err = r.reconcileCheckStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	if check.IsMalfunctioned(instance) {
		instance.Status.Phase = robotv1alpha1.RobotConfigPhaseMalfunctioned
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	err = r.reconcileCheckOwnedResources(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	if check.IsMalfunctioned(instance) {
		instance.Status.Phase = robotv1alpha1.RobotConfigPhaseMalfunctioned
	}

	err = r.reconcileUpdateInstanceStatus(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *RobotConfigReconciler) reconcileGetInstance(ctx context.Context, meta types.NamespacedName) (*robotv1alpha1.RobotConfig, error) {
	instance := &robotv1alpha1.RobotConfig{}
	err := r.Get(ctx, meta, instance)
	if err != nil {
		return &robotv1alpha1.RobotConfig{}, err
	}
	return instance, nil
}

func (r *RobotConfigReconciler) reconcileGetOwner(ctx context.Context, instance *robotv1alpha1.RobotConfig) (*robotv1alpha1.Robot, error) {
	robot := &robotv1alpha1.Robot{}
	err := r.Get(ctx, *instance.GetOwnerMetadata(), robot)
	if err != nil {
		return nil, err
	}

	return robot, nil
}

func (r *RobotConfigReconciler) reconcileGetArtifact(ctx context.Context, instance *robotv1alpha1.RobotConfig) (*robotv1alpha1.RobotArtifact, error) {
	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return nil, err
	}

	robotartifact := &robotv1alpha1.RobotArtifact{}
	err = r.Get(ctx, types.NamespacedName{
		Namespace: robot.GetRobotArtifactMetadata().Namespace,
		Name:      robot.GetRobotArtifactMetadata().Name,
	}, robotartifact)
	if err != nil {
		return nil, err
	}

	return robotartifact, nil
}

func (r *RobotConfigReconciler) reconcileCheckStatus(ctx context.Context, instance *robotv1alpha1.RobotConfig) error {
	// switch instance.Status.Phase {
	// case robotv1alpha1.RobotConfigPhaseReady:
	// 	loaderJobQuery := &batchv1.Job{}
	// 	err := r.Get(ctx, *instance.GetLoaderJobMetadata(), loaderJobQuery)
	// 	if err != nil && errors.IsNotFound(err) {

	// 	} else if err != nil {
	// 		return err
	// 	} else {
	// 		foreground := metav1.DeletePropagationForeground
	// 		err = r.Delete(ctx, loaderJobQuery, &client.DeleteOptions{
	// 			PropagationPolicy: &foreground,
	// 		})
	// 		if err != nil {
	// 			return err
	// 		}
	// 	}
	// }

	switch instance.Status.TrackerRBACStatus.Created &&
		instance.Status.CloudIDERBACStatus.Created {
	case true:

		switch instance.Status.VolumeStatuses.ROS.Var.Created &&
			instance.Status.VolumeStatuses.ROS.Opt.Created &&
			instance.Status.VolumeStatuses.ROS.Usr.Created &&
			instance.Status.VolumeStatuses.ROS.Etc.Created &&
			instance.Status.VolumeStatuses.ROS.X11Unix.Created {
		case true:
			switch instance.Status.LoaderJobStatus.Created {
			case true:
				switch instance.Status.LoaderJobStatus.Phase {
				case robotv1alpha1.JobSucceeded:
					// logger.Info("STATUS: Loader job is succeeded.")
					instance.Status.Phase = robotv1alpha1.RobotConfigPhaseReady
				case robotv1alpha1.JobActive:
					// logger.Info("STATUS: Loader job is active.")
					instance.Status.Phase = robotv1alpha1.RobotConfigPhaseConfiguringEnvironment
				case robotv1alpha1.JobFailed:
					logger.Info("STATUS: Loader job is failed.")
					instance.Status.Phase = robotv1alpha1.RobotConfigPhaseFailed
				}
			case false:
				err := r.createLoaderJob(ctx, instance)
				if err != nil {
					return err
				}
			}
		case false:
			instance.Status.Phase = robotv1alpha1.RobotConfigPhaseConfiguringEnvironment

			robot, err := r.reconcileGetOwner(ctx, instance)
			if err != nil {
				return err
			}

			if !instance.Status.VolumeStatuses.ROS.Var.Created {
				err := r.createLinuxPVCVar(ctx, instance, robot)
				if err != nil {
					return err
				}
			}

			if !instance.Status.VolumeStatuses.ROS.Opt.Created {
				err := r.createLinuxPVCOpt(ctx, instance, robot)
				if err != nil {
					return err
				}
			}

			if !instance.Status.VolumeStatuses.ROS.Usr.Created {
				err := r.createLinuxPVCUsr(ctx, instance, robot)
				if err != nil {
					return err
				}
			}

			if !instance.Status.VolumeStatuses.ROS.Etc.Created {
				err := r.createLinuxPVCEtc(ctx, instance, robot)
				if err != nil {
					return err
				}
			}

			if !instance.Status.VolumeStatuses.ROS.X11Unix.Created {
				err := r.createLinuxPVCX11Unix(ctx, instance, robot)
				if err != nil {
					return err
				}
			}
		}

	case false:
		if !instance.Status.TrackerRBACStatus.Created {
			err := r.createTrackerRBAC(ctx, instance)
			if err != nil {
				return err
			}
		}

		if !instance.Status.CloudIDERBACStatus.Created {
			err := r.createCloudIDERBAC(ctx, instance)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (r *RobotConfigReconciler) reconcileCheckOwnedResources(ctx context.Context, instance *robotv1alpha1.RobotConfig) error {
	trackerRBACQuery := &robotv1alpha1.RobotRBAC{}
	err := r.Get(ctx, *instance.GetTrackerRBACMetadata(), trackerRBACQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.TrackerRBACStatus = robotv1alpha1.RBACConfigStatus{}
	} else if err != nil {
		return err
	} else {
		instance.Status.TrackerRBACStatus.Created = true
		instance.Status.TrackerRBACStatus.Phase = trackerRBACQuery.Status.Phase
	}

	cloudIDERBACQuery := &robotv1alpha1.RobotRBAC{}
	err = r.Get(ctx, *instance.GetCloudIDERBACMetadata(), cloudIDERBACQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.CloudIDERBACStatus = robotv1alpha1.RBACConfigStatus{}
	} else if err != nil {
		return err
	} else {
		instance.Status.CloudIDERBACStatus.Created = true
		instance.Status.CloudIDERBACStatus.Phase = cloudIDERBACQuery.Status.Phase
	}

	pvcVarQuery := &corev1.PersistentVolumeClaim{}
	err = r.Get(ctx, *instance.GetPVCVarMetadata(), pvcVarQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.VolumeStatuses.ROS.Var.Created = false
	} else if err != nil {
		return err
	}

	pvcOptQuery := &corev1.PersistentVolumeClaim{}
	err = r.Get(ctx, *instance.GetPVCOptMetadata(), pvcOptQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.VolumeStatuses.ROS.Opt.Created = false
	} else if err != nil {
		return err
	}

	pvcUsrQuery := &corev1.PersistentVolumeClaim{}
	err = r.Get(ctx, *instance.GetPVCUsrMetadata(), pvcUsrQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.VolumeStatuses.ROS.Usr.Created = false
	} else if err != nil {
		return err
	}

	pvcEtcQuery := &corev1.PersistentVolumeClaim{}
	err = r.Get(ctx, *instance.GetPVCEtcMetadata(), pvcEtcQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.VolumeStatuses.ROS.Etc.Created = false
	} else if err != nil {
		return err
	}

	pvcX11UnixQuery := &corev1.PersistentVolumeClaim{}
	err = r.Get(ctx, *instance.GetPVCX11UnixMetadata(), pvcX11UnixQuery)
	if err != nil && errors.IsNotFound(err) {
		instance.Status.VolumeStatuses.ROS.X11Unix.Created = false
	} else if err != nil {
		return err
	}

	if instance.Status.Phase != robotv1alpha1.RobotConfigPhaseReady {
		loaderJobQuery := &batchv1.Job{}
		err = r.Get(ctx, *instance.GetLoaderJobMetadata(), loaderJobQuery)
		if err != nil && errors.IsNotFound(err) {
			instance.Status.LoaderJobStatus.Created = false
		} else if err != nil {
			return err
		} else {
			switch 1 {
			case int(loaderJobQuery.Status.Succeeded):
				instance.Status.LoaderJobStatus.Phase = robotv1alpha1.JobSucceeded
			case int(loaderJobQuery.Status.Active):
				instance.Status.LoaderJobStatus.Phase = robotv1alpha1.JobActive
			case int(loaderJobQuery.Status.Failed):
				instance.Status.LoaderJobStatus.Phase = robotv1alpha1.JobFailed
			}
		}
	}

	return nil
}

func (r *RobotConfigReconciler) reconcileUpdateInstanceStatus(ctx context.Context, instance *robotv1alpha1.RobotConfig) error {
	return retry.RetryOnConflict(retry.DefaultRetry, func() error {
		instanceLV := &robotv1alpha1.RobotConfig{}
		err := r.Get(ctx, types.NamespacedName{
			Name:      instance.Name,
			Namespace: instance.Namespace,
		}, instanceLV)

		if err == nil {
			instance.ResourceVersion = instanceLV.ResourceVersion
		}

		err1 := r.Status().Update(ctx, instance)
		return err1
	})
}

func (r *RobotConfigReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&robotv1alpha1.RobotConfig{}).
		Owns(&robotv1alpha1.RobotRBAC{}).
		Owns(&corev1.PersistentVolumeClaim{}).
		Owns(&batchv1.Job{}).
		Complete(r)
}
