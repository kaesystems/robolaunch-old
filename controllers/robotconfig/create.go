package robotconfig

import (
	"context"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/robotconfig/pkg/spawn"
	ctrl "sigs.k8s.io/controller-runtime"
)

func (r *RobotConfigReconciler) createTrackerRBAC(
	ctx context.Context,
	instance *robotv1alpha1.RobotConfig,
) error {

	instance.Status.Phase = robotv1alpha1.RobotConfigPhaseCreatingRBAC

	robotRBAC := spawn.GetTrackerRBAC(instance)

	err := ctrl.SetControllerReference(instance, robotRBAC, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotRBAC)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Tracker RBAC is created.")
	instance.Status.TrackerRBACStatus.Created = true

	return nil
}

func (r *RobotConfigReconciler) createCloudIDERBAC(
	ctx context.Context,
	instance *robotv1alpha1.RobotConfig,
) error {

	instance.Status.Phase = robotv1alpha1.RobotConfigPhaseCreatingRBAC

	robotRBAC := spawn.GetCloudIDERBAC(instance)

	err := ctrl.SetControllerReference(instance, robotRBAC, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, robotRBAC)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Cloud IDE RBAC is created.")
	instance.Status.CloudIDERBACStatus.Created = true

	return nil
}

func (r *RobotConfigReconciler) createLinuxPVCVar(
	ctx context.Context,
	instance *robotv1alpha1.RobotConfig,
	robot *robotv1alpha1.Robot,
) error {

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	pvcVar, err := spawn.GetPersistentVolumeClaim(instance, robot, robotArtifact.Artifacts, instance.GetPVCVarMetadata())
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, pvcVar, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, pvcVar)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Volume for /var is created.")
	instance.Status.VolumeStatuses.ROS.Var.Created = true
	return nil
}

func (r *RobotConfigReconciler) createLinuxPVCOpt(
	ctx context.Context,
	instance *robotv1alpha1.RobotConfig,
	robot *robotv1alpha1.Robot,
) error {

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	pvcOpt, err := spawn.GetPersistentVolumeClaim(instance, robot, robotArtifact.Artifacts, instance.GetPVCOptMetadata())
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, pvcOpt, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, pvcOpt)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Volume for /opt is created.")
	instance.Status.VolumeStatuses.ROS.Opt.Created = true

	return nil
}

func (r *RobotConfigReconciler) createLinuxPVCUsr(
	ctx context.Context,
	instance *robotv1alpha1.RobotConfig,
	robot *robotv1alpha1.Robot,
) error {

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	pvcUsr, err := spawn.GetPersistentVolumeClaim(instance, robot, robotArtifact.Artifacts, instance.GetPVCUsrMetadata())
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, pvcUsr, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, pvcUsr)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Volume for /usr is created.")
	instance.Status.VolumeStatuses.ROS.Usr.Created = true

	return nil
}

func (r *RobotConfigReconciler) createLinuxPVCEtc(
	ctx context.Context,
	instance *robotv1alpha1.RobotConfig,
	robot *robotv1alpha1.Robot,
) error {

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	pvcCert, err := spawn.GetPersistentVolumeClaim(instance, robot, robotArtifact.Artifacts, instance.GetPVCEtcMetadata())
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, pvcCert, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, pvcCert)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Volume for /etc is created.")
	instance.Status.VolumeStatuses.ROS.Etc.Created = true

	return nil
}

func (r *RobotConfigReconciler) createLinuxPVCX11Unix(
	ctx context.Context,
	instance *robotv1alpha1.RobotConfig,
	robot *robotv1alpha1.Robot,
) error {

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	pvcX11Unix, err := spawn.GetPersistentVolumeClaim(instance, robot, robotArtifact.Artifacts, instance.GetPVCX11UnixMetadata())
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, pvcX11Unix, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, pvcX11Unix)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Volume for /tmp/X11-Unix is created.")
	instance.Status.VolumeStatuses.ROS.X11Unix.Created = true

	return nil
}

func (r *RobotConfigReconciler) createLoaderJob(
	ctx context.Context,
	instance *robotv1alpha1.RobotConfig,
) error {

	instance.Status.Phase = robotv1alpha1.RobotConfigPhaseConfiguringEnvironment

	robot, err := r.reconcileGetOwner(ctx, instance)
	if err != nil {
		return err
	}

	robotArtifact, err := r.reconcileGetArtifact(ctx, instance)
	if err != nil {
		return err
	}

	loaderJob, err := spawn.GetLoaderJob(instance, *robot, *robotArtifact)
	if err != nil {
		return err
	}

	err = ctrl.SetControllerReference(instance, loaderJob, r.Scheme)
	if err != nil {
		return err
	}

	err = r.Create(ctx, loaderJob)
	if err != nil {
		return err
	}

	logger.Info("STATUS: Loader job is created.")
	instance.Status.LoaderJobStatus.Created = true

	return nil
}
