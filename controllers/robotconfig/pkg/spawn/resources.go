package spawn

import (
	"fmt"
	"path/filepath"
	"strconv"
	"strings"

	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
	"github.com/robolaunch/robot-operator/controllers/pkg/helpers"
	"github.com/robolaunch/robot-operator/controllers/pkg/volume"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

func GetTrackerRBAC(cr *robotv1alpha1.RobotConfig) *robotv1alpha1.RobotRBAC {

	meta := cr.GetTrackerRBACMetadata()

	trackerRBACSpec := robotv1alpha1.RobotRBACSpec{
		Rules: []rbacv1.PolicyRule{
			{
				APIGroups: []string{
					cr.GroupVersionKind().Group,
				},
				Resources: []string{
					"robotlaunchs",
					"robotnodes",
					"robottopics",
					"robotservices",
					"robotactions",
				},
				Verbs: []string{
					"get", "watch", "list", "update", "patch",
				},
			},
		},
	}

	trackerRBAC := robotv1alpha1.RobotRBAC{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: trackerRBACSpec,
	}

	return &trackerRBAC
}

func GetCloudIDERBAC(cr *robotv1alpha1.RobotConfig) *robotv1alpha1.RobotRBAC {

	meta := cr.GetCloudIDERBACMetadata()

	cloudIDERBACSpec := robotv1alpha1.RobotRBACSpec{
		Rules: []rbacv1.PolicyRule{
			{
				APIGroups: []string{
					cr.GroupVersionKind().Group,
				},
				Resources: []string{
					"robots",
				},
				Verbs: []string{
					"get", "watch", "list", "update", "patch",
				},
			},
			{
				APIGroups: []string{
					"",
				},
				Resources: []string{
					"pods",
				},
				Verbs: []string{
					"get", "watch", "list", "update", "patch",
				},
			},
			{
				APIGroups: []string{
					"",
				},
				Resources: []string{
					"pods/log",
				},
				Verbs: []string{
					"get", "list",
				},
			},
		},
	}

	cloudIDERBAC := robotv1alpha1.RobotRBAC{
		ObjectMeta: metav1.ObjectMeta{
			Name:            meta.Name,
			Namespace:       meta.Namespace,
			ResourceVersion: "",
		},
		Spec: cloudIDERBACSpec,
	}

	return &cloudIDERBAC
}

func GetPersistentVolumeClaim(
	cr *robotv1alpha1.RobotConfig,
	robot *robotv1alpha1.Robot,
	artifacts robotv1alpha1.Artifacts,
	pvcNamespacedName *types.NamespacedName,
) (*corev1.PersistentVolumeClaim, error) {

	robotDef := robot.Spec.Robot

	pvc := corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      pvcNamespacedName.Name,
			Namespace: pvcNamespacedName.Namespace,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			StorageClassName: &artifacts.StorageClassConfigs.LinuxVolumes.Name,
			AccessModes: []corev1.PersistentVolumeAccessMode{
				artifacts.StorageClassConfigs.LinuxVolumes.AccessMode,
			},
			Resources: corev1.ResourceRequirements{
				Limits: corev1.ResourceList{
					corev1.ResourceName(corev1.ResourceStorage): resource.MustParse(getClaimStorage(pvcNamespacedName, robotDef.Resources.Storage)),
				},
				Requests: corev1.ResourceList{
					corev1.ResourceName(corev1.ResourceStorage): resource.MustParse(getClaimStorage(pvcNamespacedName, robotDef.Resources.Storage)),
				},
			},
		},
	}

	return &pvc, nil
}

func getClaimStorage(pvc *types.NamespacedName, totalStorage int) string {
	storageInt := 0

	if strings.Contains(pvc.Name, "pvc-var") {
		storageInt = totalStorage / 20
	} else if strings.Contains(pvc.Name, "pvc-opt") {
		storageInt = 3 * totalStorage / 10
	} else if strings.Contains(pvc.Name, "pvc-usr") {
		storageInt = totalStorage * 5 / 10
	} else if strings.Contains(pvc.Name, "pvc-etc") {
		storageInt = totalStorage / 20
	} else if strings.Contains(pvc.Name, "pvc-x11") {
		storageInt = 100
	} else if strings.Contains(pvc.Name, "pvc-workspace") {
		storageInt = totalStorage / 10
	} else {
		storageInt = 0
	}
	return strconv.Itoa(storageInt) + "M"

}

func GetLoaderJob(
	cr *robotv1alpha1.RobotConfig,
	robot robotv1alpha1.Robot,
	robotArtifact robotv1alpha1.RobotArtifact,
) (*batchv1.Job, error) {

	robotDef := robot.Spec.Robot
	artifacts := robotArtifact.Artifacts

	var copierCmdBuilder strings.Builder
	copierCmdBuilder.WriteString("yes | cp -rf /var /ros/;")
	copierCmdBuilder.WriteString(" yes | cp -rf /usr /ros/;")
	copierCmdBuilder.WriteString(" yes | cp -rf /opt /ros/;")
	copierCmdBuilder.WriteString(" yes | cp -rf /etc /ros/;")
	copierCmdBuilder.WriteString(" echo \"DONE\"")

	if robotDef.DiscoveryServer.Enabled {

		dnsName := helpers.GetDiscoveryServerDNS(robot)
		superClientConfig := fmt.Sprintf(robotv1alpha1.SuperClientConfig, robotDef.DiscoveryServer.GUID, dnsName)

		var superClientCmdBuilder strings.Builder
		superClientCmdBuilder.WriteString(" && echo \"" + superClientConfig + "\" > " + filepath.Join("/ros", "etc", "super_client_configuration_file.xml"))
		superClientCmdBuilder.WriteString(" && sed -i \"s/" + dnsName + "/$(dig " + dnsName + " +short)/\" " + filepath.Join("/ros", "etc", "super_client_configuration_file.xml"))

		copierCmdBuilder.WriteString(superClientCmdBuilder.String())
	}

	var preparerCmdBuilder strings.Builder
	// observe source repositories
	// preparerCmdBuilder.WriteString("cat " + filepath.Join("/etc", "apt", "sources.list") + " && ")
	// preparerCmdBuilder.WriteString("cat " + filepath.Join("/etc", "apt", "sources.list.d", "ros2.list") + " && ")
	preparerCmdBuilder.WriteString("mv " + filepath.Join("/etc", "apt", "sources.list.d", "ros2.list") + " temp1 && ")
	preparerCmdBuilder.WriteString("mv " + filepath.Join("/etc", "apt", "sources.list") + " temp2 && ")
	preparerCmdBuilder.WriteString("apt-get update && ")
	preparerCmdBuilder.WriteString("mv temp1 " + filepath.Join("/etc", "apt", "sources.list.d", "ros2.list") + " && ")
	preparerCmdBuilder.WriteString("mv temp2 " + filepath.Join("/etc", "apt", "sources.list") + " && ")
	preparerCmdBuilder.WriteString("apt-get update && ")
	preparerCmdBuilder.WriteString("rosdep init")

	job := batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.GetLoaderJobMetadata().Name,
			Namespace: cr.GetLoaderJobMetadata().Namespace,
		},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					InitContainers: []corev1.Container{
						{
							Name:    "copier",
							Image:   string(artifacts.ROS.BuilderImage),
							Command: helpers.Bash(copierCmdBuilder.String()),
							VolumeMounts: []corev1.VolumeMount{
								volume.GetVolumeMount("/ros/", volume.GetVolumeVar(cr)),
								volume.GetVolumeMount("/ros/", volume.GetVolumeUsr(cr)),
								volume.GetVolumeMount("/ros/", volume.GetVolumeOpt(cr)),
								volume.GetVolumeMount("/ros/", volume.GetVolumeEtc(cr)),
							},
						},
					},
					Containers: []corev1.Container{
						{
							Name:    "preparer",
							Image:   "ubuntu:focal",
							Command: helpers.Bash(preparerCmdBuilder.String()),
							VolumeMounts: []corev1.VolumeMount{
								volume.GetVolumeMount("", volume.GetVolumeVar(cr)),
								volume.GetVolumeMount("", volume.GetVolumeUsr(cr)),
								volume.GetVolumeMount("", volume.GetVolumeOpt(cr)),
								volume.GetVolumeMount("", volume.GetVolumeEtc(cr)),
							},
						},
					},
					Volumes: []corev1.Volume{
						volume.GetVolumeVar(cr),
						volume.GetVolumeUsr(cr),
						volume.GetVolumeOpt(cr),
						volume.GetVolumeEtc(cr),
					},
					RestartPolicy: corev1.RestartPolicyNever,
					NodeSelector:  robotDef.NodeSelector,
				},
			},
		},
	}

	if robot.Status.NodeInfo.HasGPU {

		var driverInstallerCmdBuilder strings.Builder

		// run /etc/vdi/install-driver.sh
		driverInstallerCmdBuilder.WriteString(filepath.Join("/etc", "vdi", "install-driver.sh"))

		driverInstaller := corev1.Container{
			Name:    "driver-installer",
			Image:   "robolaunchio/robot:base-foxy-agnostic-xfce",
			Command: helpers.Bash(driverInstallerCmdBuilder.String()),
			VolumeMounts: []corev1.VolumeMount{
				volume.GetVolumeMount("", volume.GetVolumeVar(cr)),
				volume.GetVolumeMount("", volume.GetVolumeUsr(cr)),
				volume.GetVolumeMount("", volume.GetVolumeOpt(cr)),
				volume.GetVolumeMount("", volume.GetVolumeEtc(cr)),
			},
		}

		job.Spec.Template.Spec.InitContainers = append(job.Spec.Template.Spec.InitContainers, driverInstaller)
	}

	return &job, nil
}
