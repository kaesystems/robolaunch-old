package check

import (
	robotv1alpha1 "github.com/robolaunch/robot-operator/apis/roboscale.io/v1alpha1"
)

func IsMalfunctioned(cr *robotv1alpha1.RobotConfig) bool {
	switch cr.Status.Phase {
	case robotv1alpha1.RobotConfigPhaseReady:
		if !cr.Status.VolumeStatuses.ROS.Etc.Created ||
			!cr.Status.VolumeStatuses.ROS.Var.Created ||
			!cr.Status.VolumeStatuses.ROS.Opt.Created ||
			!cr.Status.VolumeStatuses.ROS.Usr.Created {
			return true
		}
	}

	return false
}
