module github.com/robolaunch/robot-operator

go 1.16

// replace k8s.io/api v0.24.2 => k8s.io/api v0.21.9

// replace k8s.io/apimachinery v0.24.2 => k8s.io/apimachinery v0.21.9

// replace k8s.io/client-go v0.24.2 => k8s.io/client-go v0.21.9

// replace k8s.io/code-generator v0.24.2 => k8s.io/code-generator v0.21.9

require (
	github.com/go-logr/logr v1.2.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	k8s.io/api v0.24.2
	k8s.io/apimachinery v0.24.2
	k8s.io/client-go v0.24.2
	k8s.io/code-generator v0.24.2
	sigs.k8s.io/controller-runtime v0.12.3
)
