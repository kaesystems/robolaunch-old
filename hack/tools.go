//go:build tools
// +build tools

package tools

// to import code generator without errors
// run `go get -d k8s.io/code-generator@v0.21.9`

import (
	_ "k8s.io/code-generator"
)
