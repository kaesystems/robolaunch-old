#!/bin/bash

crd-ref-docs \
    --source-path=./apis/roboscale.io/v1alpha1 \
    --renderer=markdown \
    --max-depth=10 \
    --output-path=./docs/reference/spec_reference.md \
    --config=./docs/reference/config/spec_gen.yaml

crd-ref-docs \
    --source-path=./apis/roboscale.io/v1alpha1 \
    --renderer=markdown \
    --max-depth=10 \
    --output-path=./docs/reference/status_reference.md \
    --config=./docs/reference/config/status_gen.yaml