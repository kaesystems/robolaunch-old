# API Reference

## Packages
- [robot.roboscale.io/v1alpha1](#robotroboscaleiov1alpha1)


## robot.roboscale.io/v1alpha1

Package v1alpha1 contains API Schema definitions for the robot v1alpha1 API group

### Resource Types
- [Robot](#robot)
- [RobotAction](#robotaction)
- [RobotActionList](#robotactionlist)
- [RobotArtifact](#robotartifact)
- [RobotArtifactList](#robotartifactlist)
- [RobotBuild](#robotbuild)
- [RobotClone](#robotclone)
- [RobotCloneList](#robotclonelist)
- [RobotConfig](#robotconfig)
- [RobotData](#robotdata)
- [RobotDataList](#robotdatalist)
- [RobotLaunch](#robotlaunch)
- [RobotNode](#robotnode)
- [RobotNodeList](#robotnodelist)
- [RobotRBAC](#robotrbac)
- [RobotRBACList](#robotrbaclist)
- [RobotRuntime](#robotruntime)
- [RobotService](#robotservice)
- [RobotServiceList](#robotservicelist)
- [RobotTools](#robottools)
- [RobotTopic](#robottopic)
- [RobotTopicList](#robottopiclist)
- [VDI](#vdi)



#### Action



Action information.

_Appears in:_
- [ActionData](#actiondata)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the action. |
| `type` _string array_ | Type(s) of the action. |
| `clientCount` _integer_ | Client count of action. |
| `clientNodes` _[ActionNodeInfo](#actionnodeinfo) array_ | Client nodes of action. |
| `serverCount` _integer_ | Server count of action. |
| `serverNodes` _[ActionNodeInfo](#actionnodeinfo) array_ | Server nodes of action. |


#### ActionData



Response of action request.

_Appears in:_
- [RobotAction](#robotaction)

| Field | Description |
| --- | --- |
| `message` _string_ | Response message from ROS client library. |
| `lastUpdateTimestamp` _string_ | Last time the action data is updated. |
| `actionList` _[Action](#action) array_ | Action list. |


#### ActionNodeInfo





_Appears in:_
- [Action](#action)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the node. |
| `type` _string_ | Type of the action. |


#### ActionStatus



Abstract status of owned resource RobotAction.

_Appears in:_
- [RobotDataStatus](#robotdatastatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ | Indicates whether RobotAction resource is created or not. |
| `reference` _[RobotDataTypeReference](#robotdatatypereference)_ | Reference to the RobotTopic resource. |




#### ArtifactStatus





_Appears in:_
- [RobotStatus](#robotstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


#### Artifacts



Artifacts of robot.

_Appears in:_
- [RobotArtifact](#robotartifact)

| Field | Description |
| --- | --- |
| `robot` _[ROS](#ros)_ | Image and package manager selection for ROS distro. |
| `storageClassConfigs` _[StorageClassConfigs](#storageclassconfigs)_ | Storage class selection for robot's volumes. |
| `rosContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of jobs such as cloning and building. |
| `cloudIDEContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of Cloud IDE container. |
| `trackerContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of ROS tracker container. |
| `bridgeContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of ROS bridge container. |
| `foxgloveContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of Foxglove container. |
| `vdiContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of VDI container. |
| `nodeContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of ROS node containers. |


#### BuildStatus





_Appears in:_
- [RobotStatus](#robotstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `status` _[RobotBuildStatus](#robotbuildstatus)_ |  |


#### CloneJobStatus





_Appears in:_
- [RobotCloneStatus](#robotclonestatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `phase` _[JobPhase](#jobphase)_ |  |


#### CloneStatus





_Appears in:_
- [RobotStatus](#robotstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `status` _[RobotCloneStatus](#robotclonestatus)_ |  |


#### ConfigStatus





_Appears in:_
- [RobotStatus](#robotstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `status` _[RobotConfigStatus](#robotconfigstatus)_ |  |


#### Container





_Appears in:_
- [Node](#node)

| Field | Description |
| --- | --- |
| `name` _string_ |  |
| `phase` _[ContainerState](#containerstate)_ |  |


#### ContainerArtifact



Interface for container artifacts of robolaunch. It's used to create containers for applications such as ROS tracker, Cloud IDE and ROS nodes.

_Appears in:_
- [Artifacts](#artifacts)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of container. |
| `image` _string_ | Image of container. |
| `ports` _object (keys:string, values:[ContainerPort](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#containerport-v1-core))_ | Open ports of container. Map keys should match the name of the container. |
| `entrypoint` _string array_ | Entrypoint of container. |
| `env` _[EnvVar](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#envvar-v1-core) array_ | Environment variables of container. |


#### ContainerState

_Underlying type:_ `string`



_Appears in:_
- [Container](#container)



#### DataExport



Data export configuration.

_Appears in:_
- [RobotDefinition](#robotdefinition)

| Field | Description |
| --- | --- |
| `nodes` _[TypeExport](#typeexport)_ | Robot's node data export configuration. |
| `topics` _[TypeExport](#typeexport)_ | Robot's topic data export configuration. |
| `services` _[TypeExport](#typeexport)_ | Robot's service data export configuration. |
| `actions` _[TypeExport](#typeexport)_ | Robot's action data export configuration. |


#### DataStatus





_Appears in:_
- [RobotStatus](#robotstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


#### DiscoveryServer



Configuration for discovery server connection of a robot.

_Appears in:_
- [RobotDefinition](#robotdefinition)

| Field | Description |
| --- | --- |
| `enabled` _boolean_ | Option for discovery server connection. |
| `guid` _string_ | GUID of discovery server. |
| `cluster` _string_ | Specifies the cluster that discovery server is located. Connection string will be mutated if discovery server is located to the same cluster with the robot or not. (because of DNS resolving) |
| `hostname` _string_ | Hostname of discovery server. |
| `subdomain` _string_ | Subdomain of discovery server. |


#### Etc





_Appears in:_
- [ROSVolumes](#rosvolumes)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


#### JobPhase

_Underlying type:_ `string`



_Appears in:_
- [CloneJobStatus](#clonejobstatus)
- [LoaderJobStatus](#loaderjobstatus)
- [StepStatus](#stepstatus)



#### K8sNodeInfo





_Appears in:_
- [RobotStatus](#robotstatus)

| Field | Description |
| --- | --- |
| `name` _string_ |  |
| `hasGPU` _boolean_ |  |


#### Launch



Launch description of a repository.

_Appears in:_
- [Repository](#repository)

| Field | Description |
| --- | --- |
| `env` _[EnvVar](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#envvar-v1-core) array_ | Additional environment variables to set when launching ROS nodes. |
| `launchFilePath` _string_ | Path to launchfile in repository. (eg. `linorobot/linorobot_gazebo/launch.py`) |
| `parameters` _object (keys:string, values:string)_ | Launch parameters. |
| `prelaunch` _[Prelaunch](#prelaunch)_ | Command or script to run just before node's execution. |


#### LaunchPodStatus





_Appears in:_
- [RobotLaunchStatus](#robotlaunchstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `phase` _[PodPhase](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#podphase-v1-core)_ |  |
| `ip` _string_ |  |


#### LaunchStatus





_Appears in:_
- [RobotRuntimeStatus](#robotruntimestatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `status` _[RobotLaunchStatus](#robotlaunchstatus)_ |  |


#### LoaderJobStatus





_Appears in:_
- [RobotConfigStatus](#robotconfigstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `phase` _[JobPhase](#jobphase)_ |  |


#### NameTypeInfo





_Appears in:_
- [ROSNode](#rosnode)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the object. |
| `type` _string array_ | Type(s) of the object. |


#### Node





_Appears in:_
- [NodeRequest](#noderequest)
- [NodeRequirements](#noderequirements)

| Field | Description |
| --- | --- |
| `nodeID` _string_ |  |
| `nodeType` _[NodeType](#nodetype)_ |  |
| `package` _string_ |  |
| `executable` _string_ |  |
| `index` _integer_ |  |
| `workspaceKey` _integer_ |  |
| `repositoryKey` _integer_ |  |
| `launchParameters` _object (keys:string, values:string)_ |  |
| `env` _[EnvVar](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#envvar-v1-core) array_ |  |
| `container` _[Container](#container)_ |  |
| `activated` _boolean_ |  |


#### NodeData



Response of node request.

_Appears in:_
- [RobotNode](#robotnode)

| Field | Description |
| --- | --- |
| `message` _string_ | Response message from ROS client library. |
| `lastUpdateTimestamp` _string_ | Last time the node data is updated. |
| `nodeList` _[ROSNode](#rosnode) array_ | Node list. |


#### NodeRequest





_Appears in:_
- [RobotLaunchStatus](#robotlaunchstatus)

| Field | Description |
| --- | --- |
| `successful` _boolean_ |  |
| `nodes` _[Node](#node) array_ |  |






#### NodeStatus



Abstract status of owned resource RobotNode.

_Appears in:_
- [RobotDataStatus](#robotdatastatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ | Indicates whether RobotNode resource is created or not. |
| `reference` _[RobotDataTypeReference](#robotdatatypereference)_ | Reference to the RobotTopic resource. |


#### NodeSummary



ROS node metadata.

_Appears in:_
- [Topic](#topic)

| Field | Description |
| --- | --- |
| `nodeName` _string_ | Name of the ROS node. |
| `nodeNamespace` _string_ | ROS namespace of the ROS node. |


#### NodeType

_Underlying type:_ `string`



_Appears in:_
- [Node](#node)



#### Opt





_Appears in:_
- [ROSVolumes](#rosvolumes)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


#### Prelaunch



Prelaunch command or script is applied just before the node is started.

_Appears in:_
- [Launch](#launch)

| Field | Description |
| --- | --- |
| `command` _string_ | Bash command to run before ROS node execution. |


#### RBACConfigStatus





_Appears in:_
- [RobotConfigStatus](#robotconfigstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `phase` _[RobotRBACPhase](#robotrbacphase)_ |  |


#### RBACResourceStatus



RBAC resource information.

_Appears in:_
- [RobotRBACStatus](#robotrbacstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ | Indicates if RBAC resource is created. |
| `name` _string_ | Name reference of the RBAC Resource. |


#### ROS



ROS builder image and package manager. (colcon or catkin)

_Appears in:_
- [Artifacts](#artifacts)

| Field | Description |
| --- | --- |
| `builderImage` _BuilderImage_ | Builder image of ROS environment. Image must contain a ROS distro and helper tools. |
| `rosPackageManager` _ROSPackageManager_ | ROS package manager. Use `catkin` for ROS and `colcon` for ROS 2 distros. |


#### ROSNode



Node information.

_Appears in:_
- [NodeData](#nodedata)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the node. |
| `namespace` _string_ | Namespace of the node. |
| `subscribers` _[NameTypeInfo](#nametypeinfo) array_ | Subscriber nodes. |
| `publishers` _[NameTypeInfo](#nametypeinfo) array_ | Publisher nodes. |
| `serviceServers` _[NameTypeInfo](#nametypeinfo) array_ | Service server nodes. |
| `serviceClients` _[NameTypeInfo](#nametypeinfo) array_ | Service client nodes. |
| `actionServers` _[NameTypeInfo](#nametypeinfo) array_ | Action server nodes. |
| `actionClients` _[NameTypeInfo](#nametypeinfo) array_ | Action client nodes. |


#### ROSVolumes





_Appears in:_
- [VolumeStatuses](#volumestatuses)

| Field | Description |
| --- | --- |
| `var` _[Var](#var)_ |  |
| `opt` _[Opt](#opt)_ |  |
| `usr` _[Usr](#usr)_ |  |
| `etc` _[Etc](#etc)_ |  |
| `x11Unix` _[X11Unix](#x11unix)_ |  |


#### Repository



Repository description.

_Appears in:_
- [Workspace](#workspace)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the repository. |
| `url` _string_ | Base URL of the repository. |
| `branch` _string_ | Branch of the repository to clone. |
| `launch` _[Launch](#launch)_ | [Optional] Launch description of the repository. |
| `path` _string_ | [Autofilled] Absolute path of repository |


#### Robot







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `Robot`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `status` _[RobotStatus](#robotstatus)_ |  |


#### RobotAction



RobotAction is the Schema for the robotactions API

_Appears in:_
- [RobotActionList](#robotactionlist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotAction`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `data` _[ActionData](#actiondata)_ |  |


#### RobotActionList



RobotActionList contains a list of RobotAction



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotActionList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotAction](#robotaction) array_ |  |


#### RobotArtifact



RobotArtifact is the Schema for the robotartifacts API

_Appears in:_
- [RobotArtifactList](#robotartifactlist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotArtifact`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `artifacts` _[Artifacts](#artifacts)_ |  |
| `robot` _[RobotDefinition](#robotdefinition)_ |  |


#### RobotArtifactList



RobotArtifactList contains a list of RobotArtifact



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotArtifactList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotArtifact](#robotartifact) array_ |  |


#### RobotBuild







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotBuild`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `status` _[RobotBuildStatus](#robotbuildstatus)_ |  |


#### RobotBuildPhase

_Underlying type:_ `string`



_Appears in:_
- [RobotBuildStatus](#robotbuildstatus)



#### RobotBuildStatus





_Appears in:_
- [BuildStatus](#buildstatus)
- [RobotBuild](#robotbuild)

| Field | Description |
| --- | --- |
| `scriptConfigMapStatus` _[ScriptConfigMapStatus](#scriptconfigmapstatus)_ |  |
| `steps` _object (keys:string, values:[StepStatus](#stepstatus))_ |  |
| `phase` _[RobotBuildPhase](#robotbuildphase)_ |  |


#### RobotClone





_Appears in:_
- [RobotCloneList](#robotclonelist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotClone`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `status` _[RobotCloneStatus](#robotclonestatus)_ |  |


#### RobotCloneList







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotCloneList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotClone](#robotclone) array_ |  |


#### RobotClonePhase

_Underlying type:_ `string`



_Appears in:_
- [RobotCloneStatus](#robotclonestatus)



#### RobotCloneStatus





_Appears in:_
- [CloneStatus](#clonestatus)
- [RobotClone](#robotclone)

| Field | Description |
| --- | --- |
| `workspaceStatus` _[WorkspaceStatus](#workspacestatus)_ |  |
| `cloneJobStatus` _[CloneJobStatus](#clonejobstatus)_ |  |
| `phase` _[RobotClonePhase](#robotclonephase)_ |  |


#### RobotConfig







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotConfig`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `status` _[RobotConfigStatus](#robotconfigstatus)_ |  |


#### RobotConfigPhase

_Underlying type:_ `string`



_Appears in:_
- [RobotConfigStatus](#robotconfigstatus)



#### RobotConfigStatus





_Appears in:_
- [ConfigStatus](#configstatus)
- [RobotConfig](#robotconfig)

| Field | Description |
| --- | --- |
| `trackerRBACStatus` _[RBACConfigStatus](#rbacconfigstatus)_ |  |
| `cloudIDERBACStatus` _[RBACConfigStatus](#rbacconfigstatus)_ |  |
| `volumeStatuses` _[VolumeStatuses](#volumestatuses)_ |  |
| `loaderJobStatus` _[LoaderJobStatus](#loaderjobstatus)_ |  |
| `phase` _[RobotConfigPhase](#robotconfigphase)_ |  |


#### RobotData



RobotData is the Schema for the robotdata API

_Appears in:_
- [RobotDataList](#robotdatalist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotData`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `status` _[RobotDataStatus](#robotdatastatus)_ |  |


#### RobotDataList



RobotDataList contains a list of RobotData



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotDataList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotData](#robotdata) array_ |  |


#### RobotDataStatus



RobotDataStatus defines the observed state of RobotData

_Appears in:_
- [RobotData](#robotdata)

| Field | Description |
| --- | --- |
| `nodeStatus` _[NodeStatus](#nodestatus)_ | RobotNode resource status. |
| `topicStatus` _[TopicStatus](#topicstatus)_ | RobotTopic resource status. |
| `serviceStatus` _[ServiceStatus](#servicestatus)_ | RobotService resource status. |
| `actionStatus` _[ActionStatus](#actionstatus)_ | RobotAction resource status. |


#### RobotDataTypeReference





_Appears in:_
- [ActionStatus](#actionstatus)
- [NodeStatus](#nodestatus)
- [ServiceStatus](#servicestatus)
- [TopicStatus](#topicstatus)

| Field | Description |
| --- | --- |
| `name` _string_ |  |
| `namespace` _string_ |  |


#### RobotDefinition



Robot's detailed configuration.

_Appears in:_
- [RobotArtifact](#robotartifact)

| Field | Description |
| --- | --- |
| `distro` _ROSDistro_ | ROS distro to be used. |
| `resources` _[RobotResources](#robotresources)_ | Resource limitations of robot containers. |
| `workspacesPath` _string_ | Global path of workspaces. It's fixed to `/home/workspaces` path. |
| `workspaces` _[Workspace](#workspace) array_ | Workspace definitions of robot. |
| `state` _RobotState_ | Robot's desired state. |
| `tools` _[Tools](#tools)_ | Tool selection for robot. |
| `dataExport` _[DataExport](#dataexport)_ | Data export configuration. |
| `namespacing` _boolean_ | Namespacing option. If `true`, nodes and topics will be namespaced by robot's name if possible. |
| `mode` _RobotMode_ | Robot's mode. If `Hybrid`, robot's nodes can be distributed across clusters. |
| `discoveryServer` _[DiscoveryServer](#discoveryserver)_ | Configuration for discovery server connection of a robot. |
| `clusterSelector` _string_ | Cluster selector key that is required to be found at every node as a label key. |
| `packageClusterSelection` _object (keys:string, values:string)_ | Package/Cluster matching for ROS nodes. Appliable for Hybrid robots. |
| `nodeSelector` _object (keys:string, values:string)_ | NodeSelector for scheduling robot pods. |


#### RobotLaunch







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotLaunch`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `status` _[RobotLaunchStatus](#robotlaunchstatus)_ |  |


#### RobotLaunchPhase

_Underlying type:_ `string`



_Appears in:_
- [RobotLaunchStatus](#robotlaunchstatus)



#### RobotLaunchStatus





_Appears in:_
- [LaunchStatus](#launchstatus)
- [RobotLaunch](#robotlaunch)

| Field | Description |
| --- | --- |
| `launchPodStatus` _[LaunchPodStatus](#launchpodstatus)_ |  |
| `lastLaunchTimestamp` _string_ |  |
| `nodeRequest` _[NodeRequest](#noderequest)_ |  |
| `phase` _[RobotLaunchPhase](#robotlaunchphase)_ |  |


#### RobotNode



RobotNode is the Schema for the robotnodes API

_Appears in:_
- [RobotNodeList](#robotnodelist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotNode`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `data` _[NodeData](#nodedata)_ |  |


#### RobotNodeList



RobotNodeList contains a list of RobotNode



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotNodeList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotNode](#robotnode) array_ |  |


#### RobotRBAC





_Appears in:_
- [RobotRBACList](#robotrbaclist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotRBAC`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `status` _[RobotRBACStatus](#robotrbacstatus)_ |  |


#### RobotRBACList



RobotRBACList contains a list of RobotRBAC



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotRBACList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotRBAC](#robotrbac) array_ |  |


#### RobotRBACPhase

_Underlying type:_ `string`



_Appears in:_
- [RBACConfigStatus](#rbacconfigstatus)
- [RobotRBACStatus](#robotrbacstatus)



#### RobotRBACStatus



RobotRBACStatus defines the observed state of RobotRBAC

_Appears in:_
- [RobotRBAC](#robotrbac)

| Field | Description |
| --- | --- |
| `roleStatus` _[RBACResourceStatus](#rbacresourcestatus)_ |  |
| `serviceAccountStatus` _[RBACResourceStatus](#rbacresourcestatus)_ |  |
| `roleBindingStatus` _[RBACResourceStatus](#rbacresourcestatus)_ |  |
| `phase` _[RobotRBACPhase](#robotrbacphase)_ |  |


#### RobotResources



Robot's resource limitations.

_Appears in:_
- [RobotDefinition](#robotdefinition)

| Field | Description |
| --- | --- |
| `cpuPerContainer` _string_ | Specifies how much CPU will be allocated per container. |
| `memoryPerContainer` _string_ | Specifies how much memory will be allocated per container. |
| `storage` _integer_ | Specifies how much storage will be allocated per container. |


#### RobotRuntime







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotRuntime`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `status` _[RobotRuntimeStatus](#robotruntimestatus)_ |  |


#### RobotRuntimePhase

_Underlying type:_ `string`



_Appears in:_
- [RobotRuntimeStatus](#robotruntimestatus)



#### RobotRuntimeStatus





_Appears in:_
- [RobotRuntime](#robotruntime)
- [RuntimeStatus](#runtimestatus)

| Field | Description |
| --- | --- |
| `launchStatus` _[LaunchStatus](#launchstatus)_ |  |
| `phase` _[RobotRuntimePhase](#robotruntimephase)_ |  |


#### RobotService



RobotService is the Schema for the robotservices API

_Appears in:_
- [RobotServiceList](#robotservicelist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotService`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `data` _[ServiceData](#servicedata)_ |  |


#### RobotServiceList



RobotServiceList contains a list of RobotService



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotServiceList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotService](#robotservice) array_ |  |


#### RobotStatus





_Appears in:_
- [Robot](#robot)

| Field | Description |
| --- | --- |
| `artifactStatus` _[ArtifactStatus](#artifactstatus)_ |  |
| `configStatus` _[ConfigStatus](#configstatus)_ |  |
| `cloneStatus` _[CloneStatus](#clonestatus)_ |  |
| `buildStatus` _[BuildStatus](#buildstatus)_ |  |
| `toolsStatus` _[ToolsStatus](#toolsstatus)_ |  |
| `runtimeStatus` _[RuntimeStatus](#runtimestatus)_ |  |
| `dataStatus` _[DataStatus](#datastatus)_ |  |
| `cluster` _string_ |  |
| `nodeInfo` _[K8sNodeInfo](#k8snodeinfo)_ |  |
| `phase` _RobotPhase_ |  |


#### RobotTools







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotTools`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `status` _[RobotToolsStatus](#robottoolsstatus)_ |  |


#### RobotToolsPhase

_Underlying type:_ `string`



_Appears in:_
- [RobotToolsStatus](#robottoolsstatus)



#### RobotToolsStatus





_Appears in:_
- [RobotTools](#robottools)
- [ToolsStatus](#toolsstatus)

| Field | Description |
| --- | --- |
| `toolsPodStatus` _[ToolsPodStatus](#toolspodstatus)_ |  |
| `toolsServiceStatus` _[ToolsServiceStatus](#toolsservicestatus)_ |  |
| `cloudIDEPodStatus` _[ToolsPodStatus](#toolspodstatus)_ |  |
| `cloudIDEServiceStatus` _[ToolsServiceStatus](#toolsservicestatus)_ |  |
| `bridgePodStatus` _[ToolsPodStatus](#toolspodstatus)_ |  |
| `bridgeServiceStatus` _[ToolsServiceStatus](#toolsservicestatus)_ |  |
| `foxglovePodStatus` _[ToolsPodStatus](#toolspodstatus)_ |  |
| `foxgloveServiceStatus` _[ToolsServiceStatus](#toolsservicestatus)_ |  |
| `vdiStatus` _[VDIStatusForRobot](#vdistatusforrobot)_ |  |
| `phase` _[RobotToolsPhase](#robottoolsphase)_ |  |


#### RobotTopic



RobotTopic is the Schema for the robottopics API

_Appears in:_
- [RobotTopicList](#robottopiclist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotTopic`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `data` _[TopicData](#topicdata)_ |  |


#### RobotTopicList



RobotTopicList contains a list of RobotTopic



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotTopicList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotTopic](#robottopic) array_ |  |


#### RuntimeStatus





_Appears in:_
- [RobotStatus](#robotstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `status` _[RobotRuntimeStatus](#robotruntimestatus)_ |  |


#### ScriptConfigMapStatus





_Appears in:_
- [RobotBuildStatus](#robotbuildstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


#### Service



Service information.

_Appears in:_
- [ServiceData](#servicedata)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the service. |
| `type` _string array_ | Type(s) of the service. |


#### ServiceData



Response of service request.

_Appears in:_
- [RobotService](#robotservice)

| Field | Description |
| --- | --- |
| `message` _string_ | Response message from ROS client library. |
| `lastUpdateTimestamp` _string_ | Last time the service data is updated. |
| `actionList` _[Service](#service) array_ | Service list. |


#### ServiceStatus



Abstract status of owned resource RobotService.

_Appears in:_
- [RobotDataStatus](#robotdatastatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ | Indicates whether RobotService resource is created or not. |
| `reference` _[RobotDataTypeReference](#robotdatatypereference)_ | Reference to the RobotTopic resource. |


#### Step



Step is a command or script to execute when building a robot. Either `command` or `script` should be specified for each step.

_Appears in:_
- [StepStatus](#stepstatus)
- [Workspace](#workspace)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the step. |
| `command` _string_ | Bash command to run. |
| `script` _string_ | Bash script to run. |
| `env` _[EnvVar](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#envvar-v1-core) array_ | Environment variables for step. |


#### StepStatus





_Appears in:_
- [RobotBuildStatus](#robotbuildstatus)

| Field | Description |
| --- | --- |
| `step` _[Step](#step)_ |  |
| `jobName` _string_ |  |
| `created` _boolean_ |  |
| `jobPhase` _[JobPhase](#jobphase)_ |  |


#### StorageClassConfig



Storage class configuration for a volume type.

_Appears in:_
- [StorageClassConfigs](#storageclassconfigs)

| Field | Description |
| --- | --- |
| `name` _string_ | Storage class name |
| `accessMode` _[PersistentVolumeAccessMode](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#persistentvolumeaccessmode-v1-core)_ | PVC access mode. |


#### StorageClassConfigs



Storage class selection for persistent volume claims.

_Appears in:_
- [Artifacts](#artifacts)

| Field | Description |
| --- | --- |
| `workspaceVolumes` _[StorageClassConfig](#storageclassconfig)_ | Storage class for workspace PVC. Since it is aimed to share workspace volume across multiple clusters in Hybrid robots, storage classes that supports `ReadWriteMany` feature should be selected. Defaulted to `obenebs-hostpath`. |
| `linuxVolumes` _[StorageClassConfig](#storageclassconfig)_ | Storage class for Linux PVCs. To make robot's data persistent, it's various directories are copied to PVCs and stored. Defaulted to `obenebs-hostpath`. |


#### ToolConfig



Tool configurations.

_Appears in:_
- [Tools](#tools)

| Field | Description |
| --- | --- |
| `enabled` _boolean_ | Indicates if the tool is enabled or disabled. |
| `resources` _[ToolResources](#toolresources)_ | Indicates tool's pod resource limits. |
| `ports` _[ToolPorts](#toolports)_ | Indicates application's (tool) node ports. |


#### ToolPorts





_Appears in:_
- [ToolConfig](#toolconfig)

| Field | Description |
| --- | --- |
| `tcp` _integer_ |  |
| `udp` _[ToolPortsUDP](#toolportsudp)_ |  |


#### ToolPortsUDP





_Appears in:_
- [ToolPorts](#toolports)

| Field | Description |
| --- | --- |
| `range` _string_ |  |


#### ToolResources



Tool resource limits.

_Appears in:_
- [ToolConfig](#toolconfig)

| Field | Description |
| --- | --- |
| `gpuCore` _integer_ |  |
| `cpu` _string_ |  |
| `memory` _string_ |  |


#### Tools





_Appears in:_
- [RobotDefinition](#robotdefinition)

| Field | Description |
| --- | --- |
| `tracker` _[ToolConfig](#toolconfig)_ | Configuration for ROS Tracker. |
| `cloudIDE` _[ToolConfig](#toolconfig)_ | Configuration for Cloud IDE. |
| `bridge` _[ToolConfig](#toolconfig)_ | Configuration for ROS Bridge Suite. |
| `foxglove` _[ToolConfig](#toolconfig)_ | Configuration for Foxglove. |
| `vdi` _[ToolConfig](#toolconfig)_ | Configuration for VDI. It can only be used in Kubernetes clusters which has GPU acceleration. |


#### ToolsPodStatus





_Appears in:_
- [RobotToolsStatus](#robottoolsstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `phase` _[PodPhase](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#podphase-v1-core)_ |  |
| `ip` _string_ |  |


#### ToolsServiceStatus





_Appears in:_
- [RobotToolsStatus](#robottoolsstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `ports` _object (keys:string, values:integer)_ |  |


#### ToolsStatus





_Appears in:_
- [RobotStatus](#robotstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `status` _[RobotToolsStatus](#robottoolsstatus)_ |  |


#### Topic



Topic information.

_Appears in:_
- [TopicData](#topicdata)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the topic. |
| `type` _string array_ | Type(s) of the topic. |
| `publisherCount` _integer_ | Number of publisher nodes. |
| `publisherNodes` _[NodeSummary](#nodesummary) array_ | Publisher nodes of topic. |
| `subscriberCount` _integer_ | Number of subscriber nodes. |
| `subscriberNodes` _[NodeSummary](#nodesummary) array_ | Subscriber nodes of topic. |


#### TopicData



Response of topic request.

_Appears in:_
- [RobotTopic](#robottopic)

| Field | Description |
| --- | --- |
| `message` _string_ | Response message from ROS client library. |
| `lastUpdateTimestamp` _string_ | Last time the topic data is updated. |
| `topicList` _[Topic](#topic) array_ | Topic list. |


#### TopicStatus



Abstract status of owned resource RobotTopic.

_Appears in:_
- [RobotDataStatus](#robotdatastatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ | Indicates whether RobotTopic resource is created or not. |
| `reference` _[RobotDataTypeReference](#robotdatatypereference)_ | Reference to the RobotTopic resource. |




#### TypeExport



Export configuration of relevant data

_Appears in:_
- [DataExport](#dataexport)

| Field | Description |
| --- | --- |
| `enabled` _boolean_ |  |
| `updateFrequency` _string_ |  |


#### Usr





_Appears in:_
- [ROSVolumes](#rosvolumes)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


#### VDI







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `VDI`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `status` _[VDIStatus](#vdistatus)_ |  |


#### VDIPhase

_Underlying type:_ `string`



_Appears in:_
- [VDIStatus](#vdistatus)



#### VDIPodStatus





_Appears in:_
- [VDIStatus](#vdistatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `phase` _[PodPhase](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#podphase-v1-core)_ |  |
| `ip` _string_ |  |


#### VDIServiceStatus





_Appears in:_
- [VDIStatus](#vdistatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


#### VDIStatus





_Appears in:_
- [VDI](#vdi)
- [VDIStatusForRobot](#vdistatusforrobot)

| Field | Description |
| --- | --- |
| `vdiPodStatus` _[VDIPodStatus](#vdipodstatus)_ |  |
| `vdiServiceStatus` _[VDIServiceStatus](#vdiservicestatus)_ |  |
| `phase` _[VDIPhase](#vdiphase)_ |  |


#### VDIStatusForRobot





_Appears in:_
- [RobotToolsStatus](#robottoolsstatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |
| `status` _[VDIStatus](#vdistatus)_ |  |


#### Var





_Appears in:_
- [ROSVolumes](#rosvolumes)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


#### VolumeStatuses





_Appears in:_
- [RobotConfigStatus](#robotconfigstatus)

| Field | Description |
| --- | --- |
| `robot` _[ROSVolumes](#rosvolumes)_ |  |


#### Workspace



Workspace description. Each robot should contain at least one workspace. A workspace should contain at least one repository in it. Building operations are executed seperately for each workspace. Global path of workspaces is `/home/workspaces`.

_Appears in:_
- [RobotDefinition](#robotdefinition)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of workspace. If a workspace's name is `my_ws`, it's absolute path is `/home/workspaces/my_ws`. |
| `repositories` _[Repository](#repository) array_ | Repositories to clone inside workspace's `src` directory. |
| `build` _WorkspaceBuildType_ | Build type of a workspace. If `Standard`, `rosdep install` and `colcon build` commands are executed in order. If `Custom`, steps should be defined as bash commands or scripts. |
| `buildSteps` _[Step](#step) array_ | Building steps of a workspace. If build type of a workspace is `Custom`, it's building steps can be defined as steps. |


#### WorkspaceStatus





_Appears in:_
- [RobotCloneStatus](#robotclonestatus)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


#### X11Unix





_Appears in:_
- [ROSVolumes](#rosvolumes)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


