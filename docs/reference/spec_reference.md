# API Reference

## Packages
- [robot.roboscale.io/v1alpha1](#robotroboscaleiov1alpha1)


## robot.roboscale.io/v1alpha1

Package v1alpha1 contains API Schema definitions for the robot v1alpha1 API group

### Resource Types
- [Robot](#robot)
- [RobotAction](#robotaction)
- [RobotActionList](#robotactionlist)
- [RobotArtifact](#robotartifact)
- [RobotArtifactList](#robotartifactlist)
- [RobotBuild](#robotbuild)
- [RobotClone](#robotclone)
- [RobotCloneList](#robotclonelist)
- [RobotConfig](#robotconfig)
- [RobotData](#robotdata)
- [RobotDataList](#robotdatalist)
- [RobotLaunch](#robotlaunch)
- [RobotNode](#robotnode)
- [RobotNodeList](#robotnodelist)
- [RobotRBAC](#robotrbac)
- [RobotRBACList](#robotrbaclist)
- [RobotRuntime](#robotruntime)
- [RobotService](#robotservice)
- [RobotServiceList](#robotservicelist)
- [RobotTools](#robottools)
- [RobotTopic](#robottopic)
- [RobotTopicList](#robottopiclist)
- [VDI](#vdi)



#### Action



Action information.

_Appears in:_
- [ActionData](#actiondata)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the action. |
| `type` _string array_ | Type(s) of the action. |
| `clientCount` _integer_ | Client count of action. |
| `clientNodes` _[ActionNodeInfo](#actionnodeinfo) array_ | Client nodes of action. |
| `serverCount` _integer_ | Server count of action. |
| `serverNodes` _[ActionNodeInfo](#actionnodeinfo) array_ | Server nodes of action. |


#### ActionData



Response of action request.

_Appears in:_
- [RobotAction](#robotaction)

| Field | Description |
| --- | --- |
| `message` _string_ | Response message from ROS client library. |
| `lastUpdateTimestamp` _string_ | Last time the action data is updated. |
| `actionList` _[Action](#action) array_ | Action list. |


#### ActionNodeInfo





_Appears in:_
- [Action](#action)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the node. |
| `type` _string_ | Type of the action. |


#### ArtifactReference





_Appears in:_
- [RobotSpec](#robotspec)

| Field | Description |
| --- | --- |
| `namespace` _string_ | Robot artifact object namespace. |
| `name` _string_ | Robot artifact object name. |


#### Artifacts



Artifacts of robot.

_Appears in:_
- [RobotArtifact](#robotartifact)

| Field | Description |
| --- | --- |
| `robot` _[ROS](#ros)_ | Image and package manager selection for ROS distro. |
| `storageClassConfigs` _[StorageClassConfigs](#storageclassconfigs)_ | Storage class selection for robot's volumes. |
| `rosContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of jobs such as cloning and building. |
| `cloudIDEContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of Cloud IDE container. |
| `trackerContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of ROS tracker container. |
| `bridgeContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of ROS bridge container. |
| `foxgloveContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of Foxglove container. |
| `vdiContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of VDI container. |
| `nodeContainer` _[ContainerArtifact](#containerartifact)_ | Container artifacts of ROS node containers. |


#### Container





_Appears in:_
- [Node](#node)

| Field | Description |
| --- | --- |
| `name` _string_ |  |
| `phase` _ContainerState_ |  |


#### ContainerArtifact



Interface for container artifacts of robolaunch. It's used to create containers for applications such as ROS tracker, Cloud IDE and ROS nodes.

_Appears in:_
- [Artifacts](#artifacts)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of container. |
| `image` _string_ | Image of container. |
| `ports` _object (keys:string, values:[ContainerPort](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#containerport-v1-core))_ | Open ports of container. Map keys should match the name of the container. |
| `entrypoint` _string array_ | Entrypoint of container. |
| `env` _[EnvVar](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#envvar-v1-core) array_ | Environment variables of container. |


#### DataExport



Data export configuration.

_Appears in:_
- [RobotDataSpec](#robotdataspec)
- [RobotDefinition](#robotdefinition)

| Field | Description |
| --- | --- |
| `nodes` _[TypeExport](#typeexport)_ | Robot's node data export configuration. |
| `topics` _[TypeExport](#typeexport)_ | Robot's topic data export configuration. |
| `services` _[TypeExport](#typeexport)_ | Robot's service data export configuration. |
| `actions` _[TypeExport](#typeexport)_ | Robot's action data export configuration. |


#### DiscoveryServer



Configuration for discovery server connection of a robot.

_Appears in:_
- [RobotDefinition](#robotdefinition)

| Field | Description |
| --- | --- |
| `enabled` _boolean_ | Option for discovery server connection. |
| `guid` _string_ | GUID of discovery server. |
| `cluster` _string_ | Specifies the cluster that discovery server is located. Connection string will be mutated if discovery server is located to the same cluster with the robot or not. (because of DNS resolving) |
| `hostname` _string_ | Hostname of discovery server. |
| `subdomain` _string_ | Subdomain of discovery server. |


#### Etc





_Appears in:_
- [ROSVolumes](#rosvolumes)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |




#### Launch



Launch description of a repository.

_Appears in:_
- [Repository](#repository)

| Field | Description |
| --- | --- |
| `env` _[EnvVar](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#envvar-v1-core) array_ | Additional environment variables to set when launching ROS nodes. |
| `launchFilePath` _string_ | Path to launchfile in repository. (eg. `linorobot/linorobot_gazebo/launch.py`) |
| `parameters` _object (keys:string, values:string)_ | Launch parameters. |
| `prelaunch` _[Prelaunch](#prelaunch)_ | Command or script to run just before node's execution. |


#### NameTypeInfo





_Appears in:_
- [ROSNode](#rosnode)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the object. |
| `type` _string array_ | Type(s) of the object. |


#### Node





_Appears in:_
- [NodeRequest](#noderequest)
- [NodeRequirements](#noderequirements)

| Field | Description |
| --- | --- |
| `nodeID` _string_ |  |
| `nodeType` _NodeType_ |  |
| `package` _string_ |  |
| `executable` _string_ |  |
| `index` _integer_ |  |
| `workspaceKey` _integer_ |  |
| `repositoryKey` _integer_ |  |
| `launchParameters` _object (keys:string, values:string)_ |  |
| `env` _[EnvVar](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#envvar-v1-core) array_ |  |
| `container` _[Container](#container)_ |  |
| `activated` _boolean_ |  |


#### NodeData



Response of node request.

_Appears in:_
- [RobotNode](#robotnode)

| Field | Description |
| --- | --- |
| `message` _string_ | Response message from ROS client library. |
| `lastUpdateTimestamp` _string_ | Last time the node data is updated. |
| `nodeList` _[ROSNode](#rosnode) array_ | Node list. |








#### NodeSummary



ROS node metadata.

_Appears in:_
- [Topic](#topic)

| Field | Description |
| --- | --- |
| `nodeName` _string_ | Name of the ROS node. |
| `nodeNamespace` _string_ | ROS namespace of the ROS node. |


#### Opt





_Appears in:_
- [ROSVolumes](#rosvolumes)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


#### Prelaunch



Prelaunch command or script is applied just before the node is started.

_Appears in:_
- [Launch](#launch)

| Field | Description |
| --- | --- |
| `command` _string_ | Bash command to run before ROS node execution. |


#### ROS



ROS builder image and package manager. (colcon or catkin)

_Appears in:_
- [Artifacts](#artifacts)

| Field | Description |
| --- | --- |
| `builderImage` _BuilderImage_ | Builder image of ROS environment. Image must contain a ROS distro and helper tools. |
| `rosPackageManager` _ROSPackageManager_ | ROS package manager. Use `catkin` for ROS and `colcon` for ROS 2 distros. |


#### ROSNode



Node information.

_Appears in:_
- [NodeData](#nodedata)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the node. |
| `namespace` _string_ | Namespace of the node. |
| `subscribers` _[NameTypeInfo](#nametypeinfo) array_ | Subscriber nodes. |
| `publishers` _[NameTypeInfo](#nametypeinfo) array_ | Publisher nodes. |
| `serviceServers` _[NameTypeInfo](#nametypeinfo) array_ | Service server nodes. |
| `serviceClients` _[NameTypeInfo](#nametypeinfo) array_ | Service client nodes. |
| `actionServers` _[NameTypeInfo](#nametypeinfo) array_ | Action server nodes. |
| `actionClients` _[NameTypeInfo](#nametypeinfo) array_ | Action client nodes. |


#### ROSVolumes





_Appears in:_
- [VolumeStatuses](#volumestatuses)

| Field | Description |
| --- | --- |
| `var` _[Var](#var)_ |  |
| `opt` _[Opt](#opt)_ |  |
| `usr` _[Usr](#usr)_ |  |
| `etc` _[Etc](#etc)_ |  |
| `x11Unix` _[X11Unix](#x11unix)_ |  |


#### Repository



Repository description.

_Appears in:_
- [Workspace](#workspace)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the repository. |
| `url` _string_ | Base URL of the repository. |
| `branch` _string_ | Branch of the repository to clone. |
| `launch` _[Launch](#launch)_ | [Optional] Launch description of the repository. |
| `path` _string_ | [Autofilled] Absolute path of repository |


#### Robot







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `Robot`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `spec` _[RobotSpec](#robotspec)_ |  |


#### RobotAction



RobotAction is the Schema for the robotactions API

_Appears in:_
- [RobotActionList](#robotactionlist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotAction`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `data` _[ActionData](#actiondata)_ |  |
| `spec` _[RobotActionSpec](#robotactionspec)_ |  |


#### RobotActionList



RobotActionList contains a list of RobotAction



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotActionList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotAction](#robotaction) array_ |  |


#### RobotActionSpec



RobotActionSpec defines the desired state of RobotAction

_Appears in:_
- [RobotAction](#robotaction)

| Field | Description |
| --- | --- |
| `updateFrequency` _string_ | Update frequency of action information. |


#### RobotArtifact



RobotArtifact is the Schema for the robotartifacts API

_Appears in:_
- [RobotArtifactList](#robotartifactlist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotArtifact`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `artifacts` _[Artifacts](#artifacts)_ |  |
| `robot` _[RobotDefinition](#robotdefinition)_ |  |


#### RobotArtifactList



RobotArtifactList contains a list of RobotArtifact



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotArtifactList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotArtifact](#robotartifact) array_ |  |


#### RobotBuild







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotBuild`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `spec` _[RobotBuildSpec](#robotbuildspec)_ |  |


#### RobotBuildSpec





_Appears in:_
- [RobotBuild](#robotbuild)

| Field | Description |
| --- | --- |
| `steps` _[Step](#step) array_ |  |


#### RobotClone





_Appears in:_
- [RobotCloneList](#robotclonelist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotClone`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `spec` _[RobotCloneSpec](#robotclonespec)_ |  |


#### RobotCloneList







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotCloneList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotClone](#robotclone) array_ |  |




#### RobotConfig







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotConfig`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `spec` _[RobotConfigSpec](#robotconfigspec)_ |  |




#### RobotData



RobotData is the Schema for the robotdata API

_Appears in:_
- [RobotDataList](#robotdatalist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotData`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `spec` _[RobotDataSpec](#robotdataspec)_ |  |


#### RobotDataList



RobotDataList contains a list of RobotData



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotDataList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotData](#robotdata) array_ |  |


#### RobotDataSpec



RobotDataSpec defines the desired state of RobotData

_Appears in:_
- [RobotData](#robotdata)

| Field | Description |
| --- | --- |
| `dataExport` _[DataExport](#dataexport)_ |  |




#### RobotDefinition



Robot's detailed configuration.

_Appears in:_
- [RobotArtifact](#robotartifact)
- [RobotSpec](#robotspec)

| Field | Description |
| --- | --- |
| `distro` _ROSDistro_ | ROS distro to be used. |
| `resources` _[RobotResources](#robotresources)_ | Resource limitations of robot containers. |
| `workspacesPath` _string_ | Global path of workspaces. It's fixed to `/home/workspaces` path. |
| `workspaces` _[Workspace](#workspace) array_ | Workspace definitions of robot. |
| `state` _RobotState_ | Robot's desired state. |
| `tools` _[Tools](#tools)_ | Tool selection for robot. |
| `dataExport` _[DataExport](#dataexport)_ | Data export configuration. |
| `namespacing` _boolean_ | Namespacing option. If `true`, nodes and topics will be namespaced by robot's name if possible. |
| `mode` _RobotMode_ | Robot's mode. If `Hybrid`, robot's nodes can be distributed across clusters. |
| `discoveryServer` _[DiscoveryServer](#discoveryserver)_ | Configuration for discovery server connection of a robot. |
| `clusterSelector` _string_ | Cluster selector key that is required to be found at every node as a label key. |
| `packageClusterSelection` _object (keys:string, values:string)_ | Package/Cluster matching for ROS nodes. Appliable for Hybrid robots. |
| `nodeSelector` _object (keys:string, values:string)_ | NodeSelector for scheduling robot pods. |


#### RobotLaunch







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotLaunch`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `spec` _[RobotLaunchSpec](#robotlaunchspec)_ |  |


#### RobotLaunchSpec





_Appears in:_
- [RobotLaunch](#robotlaunch)

| Field | Description |
| --- | --- |
| `lastLaunchTimestamp` _string_ |  |
| `trackerInfo` _[TrackerInfo](#trackerinfo)_ |  |


#### RobotNode



RobotNode is the Schema for the robotnodes API

_Appears in:_
- [RobotNodeList](#robotnodelist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotNode`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `data` _[NodeData](#nodedata)_ |  |
| `spec` _[RobotNodeSpec](#robotnodespec)_ |  |


#### RobotNodeList



RobotNodeList contains a list of RobotNode



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotNodeList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotNode](#robotnode) array_ |  |


#### RobotNodeSpec



RobotNodeSpec defines the desired state of RobotNode

_Appears in:_
- [RobotNode](#robotnode)

| Field | Description |
| --- | --- |
| `updateFrequency` _string_ | Update frequency of node information. |


#### RobotRBAC





_Appears in:_
- [RobotRBACList](#robotrbaclist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotRBAC`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `spec` _[RobotRBACSpec](#robotrbacspec)_ |  |


#### RobotRBACList



RobotRBACList contains a list of RobotRBAC



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotRBACList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotRBAC](#robotrbac) array_ |  |


#### RobotRBACSpec



RobotRBACSpec defines the desired state of RobotRBAC

_Appears in:_
- [RobotRBAC](#robotrbac)

| Field | Description |
| --- | --- |
| `rules` _[PolicyRule](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#policyrule-v1-rbac) array_ |  |


#### RobotResources



Robot's resource limitations.

_Appears in:_
- [RobotDefinition](#robotdefinition)

| Field | Description |
| --- | --- |
| `cpuPerContainer` _string_ | Specifies how much CPU will be allocated per container. |
| `memoryPerContainer` _string_ | Specifies how much memory will be allocated per container. |
| `storage` _integer_ | Specifies how much storage will be allocated per container. |


#### RobotRuntime







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotRuntime`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `spec` _[RobotRuntimeSpec](#robotruntimespec)_ |  |




#### RobotService



RobotService is the Schema for the robotservices API

_Appears in:_
- [RobotServiceList](#robotservicelist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotService`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `data` _[ServiceData](#servicedata)_ |  |
| `spec` _[RobotServiceSpec](#robotservicespec)_ |  |


#### RobotServiceList



RobotServiceList contains a list of RobotService



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotServiceList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotService](#robotservice) array_ |  |


#### RobotServiceSpec



RobotServiceSpec defines the desired state of RobotService

_Appears in:_
- [RobotService](#robotservice)

| Field | Description |
| --- | --- |
| `updateFrequency` _string_ | Update frequency of service information. |


#### RobotSpec



Desired state of a robot.

_Appears in:_
- [Robot](#robot)

| Field | Description |
| --- | --- |
| `robot` _[RobotDefinition](#robotdefinition)_ | Desired state of robot and ROS environment. |
| `artifacts` _[ArtifactReference](#artifactreference)_ | Desired values for Kubernetes resources of robot. |


#### RobotTools







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotTools`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `spec` _[RobotToolsSpec](#robottoolsspec)_ |  |


#### RobotToolsSpec





_Appears in:_
- [RobotTools](#robottools)

| Field | Description |
| --- | --- |
| `tools` _[Tools](#tools)_ |  |


#### RobotTopic



RobotTopic is the Schema for the robottopics API

_Appears in:_
- [RobotTopicList](#robottopiclist)

| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotTopic`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `data` _[TopicData](#topicdata)_ |  |
| `spec` _[RobotTopicSpec](#robottopicspec)_ |  |


#### RobotTopicList



RobotTopicList contains a list of RobotTopic



| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `RobotTopicList`
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `items` _[RobotTopic](#robottopic) array_ |  |


#### RobotTopicSpec



RobotTopicSpec defines the desired state of RobotTopic

_Appears in:_
- [RobotTopic](#robottopic)

| Field | Description |
| --- | --- |
| `updateFrequency` _string_ | Update frequency of topic information. |


#### Service



Service information.

_Appears in:_
- [ServiceData](#servicedata)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the service. |
| `type` _string array_ | Type(s) of the service. |


#### ServiceData



Response of service request.

_Appears in:_
- [RobotService](#robotservice)

| Field | Description |
| --- | --- |
| `message` _string_ | Response message from ROS client library. |
| `lastUpdateTimestamp` _string_ | Last time the service data is updated. |
| `actionList` _[Service](#service) array_ | Service list. |


#### Step



Step is a command or script to execute when building a robot. Either `command` or `script` should be specified for each step.

_Appears in:_
- [RobotBuildSpec](#robotbuildspec)
- [Workspace](#workspace)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the step. |
| `command` _string_ | Bash command to run. |
| `script` _string_ | Bash script to run. |
| `env` _[EnvVar](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#envvar-v1-core) array_ | Environment variables for step. |


#### StorageClassConfig



Storage class configuration for a volume type.

_Appears in:_
- [StorageClassConfigs](#storageclassconfigs)

| Field | Description |
| --- | --- |
| `name` _string_ | Storage class name |
| `accessMode` _[PersistentVolumeAccessMode](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#persistentvolumeaccessmode-v1-core)_ | PVC access mode. |


#### StorageClassConfigs



Storage class selection for persistent volume claims.

_Appears in:_
- [Artifacts](#artifacts)

| Field | Description |
| --- | --- |
| `workspaceVolumes` _[StorageClassConfig](#storageclassconfig)_ | Storage class for workspace PVC. Since it is aimed to share workspace volume across multiple clusters in Hybrid robots, storage classes that supports `ReadWriteMany` feature should be selected. Defaulted to `obenebs-hostpath`. |
| `linuxVolumes` _[StorageClassConfig](#storageclassconfig)_ | Storage class for Linux PVCs. To make robot's data persistent, it's various directories are copied to PVCs and stored. Defaulted to `obenebs-hostpath`. |


#### ToolConfig



Tool configurations.

_Appears in:_
- [Tools](#tools)
- [VDISpec](#vdispec)

| Field | Description |
| --- | --- |
| `enabled` _boolean_ | Indicates if the tool is enabled or disabled. |
| `resources` _[ToolResources](#toolresources)_ | Indicates tool's pod resource limits. |
| `ports` _[ToolPorts](#toolports)_ | Indicates application's (tool) node ports. |


#### ToolPorts





_Appears in:_
- [ToolConfig](#toolconfig)

| Field | Description |
| --- | --- |
| `tcp` _integer_ |  |
| `udp` _[ToolPortsUDP](#toolportsudp)_ |  |


#### ToolPortsUDP





_Appears in:_
- [ToolPorts](#toolports)

| Field | Description |
| --- | --- |
| `range` _string_ |  |


#### ToolResources



Tool resource limits.

_Appears in:_
- [ToolConfig](#toolconfig)

| Field | Description |
| --- | --- |
| `gpuCore` _integer_ |  |
| `cpu` _string_ |  |
| `memory` _string_ |  |


#### Tools





_Appears in:_
- [RobotDefinition](#robotdefinition)
- [RobotToolsSpec](#robottoolsspec)

| Field | Description |
| --- | --- |
| `tracker` _[ToolConfig](#toolconfig)_ | Configuration for ROS Tracker. |
| `cloudIDE` _[ToolConfig](#toolconfig)_ | Configuration for Cloud IDE. |
| `bridge` _[ToolConfig](#toolconfig)_ | Configuration for ROS Bridge Suite. |
| `foxglove` _[ToolConfig](#toolconfig)_ | Configuration for Foxglove. |
| `vdi` _[ToolConfig](#toolconfig)_ | Configuration for VDI. It can only be used in Kubernetes clusters which has GPU acceleration. |


#### Topic



Topic information.

_Appears in:_
- [TopicData](#topicdata)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of the topic. |
| `type` _string array_ | Type(s) of the topic. |
| `publisherCount` _integer_ | Number of publisher nodes. |
| `publisherNodes` _[NodeSummary](#nodesummary) array_ | Publisher nodes of topic. |
| `subscriberCount` _integer_ | Number of subscriber nodes. |
| `subscriberNodes` _[NodeSummary](#nodesummary) array_ | Subscriber nodes of topic. |


#### TopicData



Response of topic request.

_Appears in:_
- [RobotTopic](#robottopic)

| Field | Description |
| --- | --- |
| `message` _string_ | Response message from ROS client library. |
| `lastUpdateTimestamp` _string_ | Last time the topic data is updated. |
| `topicList` _[Topic](#topic) array_ | Topic list. |


#### TrackerInfo





_Appears in:_
- [RobotLaunchSpec](#robotlaunchspec)

| Field | Description |
| --- | --- |
| `ip` _string_ |  |
| `port` _string_ |  |


#### TypeExport



Export configuration of relevant data

_Appears in:_
- [DataExport](#dataexport)

| Field | Description |
| --- | --- |
| `enabled` _boolean_ |  |
| `updateFrequency` _string_ |  |


#### Usr





_Appears in:_
- [ROSVolumes](#rosvolumes)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


#### VDI







| Field | Description |
| --- | --- |
| `apiVersion` _string_ | `robot.roboscale.io/v1alpha1`
| `kind` _string_ | `VDI`
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |
| `spec` _[VDISpec](#vdispec)_ |  |


#### VDISpec





_Appears in:_
- [VDI](#vdi)

| Field | Description |
| --- | --- |
| `config` _[ToolConfig](#toolconfig)_ |  |




#### Var





_Appears in:_
- [ROSVolumes](#rosvolumes)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |




#### Workspace



Workspace description. Each robot should contain at least one workspace. A workspace should contain at least one repository in it. Building operations are executed seperately for each workspace. Global path of workspaces is `/home/workspaces`.

_Appears in:_
- [RobotDefinition](#robotdefinition)

| Field | Description |
| --- | --- |
| `name` _string_ | Name of workspace. If a workspace's name is `my_ws`, it's absolute path is `/home/workspaces/my_ws`. |
| `repositories` _[Repository](#repository) array_ | Repositories to clone inside workspace's `src` directory. |
| `build` _WorkspaceBuildType_ | Build type of a workspace. If `Standard`, `rosdep install` and `colcon build` commands are executed in order. If `Custom`, steps should be defined as bash commands or scripts. |
| `buildSteps` _[Step](#step) array_ | Building steps of a workspace. If build type of a workspace is `Custom`, it's building steps can be defined as steps. |


#### X11Unix





_Appears in:_
- [ROSVolumes](#rosvolumes)

| Field | Description |
| --- | --- |
| `created` _boolean_ |  |


