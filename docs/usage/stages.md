# Robot Stages

Robot's stages are defined in field `spec.ros.state`. Robot's stage represents the desired robot state in robot's lifecycle loop. Three options are supported:

- `Passive`
- `Built`
- `Launched`

## Stage/Action Matrice

| Stage/Action | [Configuration Operations](ros_configuration.md) | [Cloning Operations](workspace_configuration.md) | [Building Operations](building.md) | [Launching Operations](launching.md) |
|:------------:|:------------------------:|:------------------:|:-------------------:|:--------------------:|
|  **[Passive](#passive-stage)** |           **X**          |        **X**       |                     |                      |
|   **[Built](#built-stage)**  |           **X**          |        **X**       |        **X**        |                      |
| **[Launched](#launched-stage)** |           **X**          |        **X**       |        **X**        |         **X**        |

## Stages

### Passive Stage

If the field is set to `Passive` initially, only action that operator does is configuring robot's workspaces which means cloning the repositories inside workspaces' `src` folders. ([Configuring Workspaces](workspace_configuration.md)) After workspace configuration, robot's code can be developed from Cloud IDE and launching processes can be started/stopped from Cloud IDE terminal. To configure Cloud IDE, please refer [Development](tools.md#development) documentation.

### Built Stage

If the field is set to `Built` initially, operator configures workspace and build the workspaces as it was defined at field `spec.ros.workspaces[n].build`. ([Building Workspaces](building.md)) After that robot's code can be developed from Cloud IDE and launching processes can be started/stopped from Cloud IDE terminal.

### Launched Stage

It can be said that the `Launched` stage is also production stage for robots. If the field is set to `Launched` initially, operator configures workspace, builds the workspaces and launch the robot as it was defined at field `spec.ros.workspaces[n].repositories[m].launch`. ([Launching Robot](launching.md)) After that robot's code can be developed from Cloud IDE and launching processes can be started/stopped from Cloud IDE terminal.
