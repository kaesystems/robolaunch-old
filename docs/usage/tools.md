# Use-Cases and Tools

## Development

### Code Server

If you set `spec.robot.tools.cloudIDE.enabled` field to `true`, you can use Cloud IDE to develop your robot's code. To access Cloud IDE, run:

```bash
kubectl get robot linorobot2 -o jsonpath='{.status.toolsStatus.status.cloudIDEServiceStatus.ports.cloud-ide}' | yq -P
```

```bash
Output:
32054 # example
```

Open up a web browser and browse [http://cluster-hostname-or-external-ip:32054](-) to start developing your code.

## VDI and Simulation

***This feature is currently under active development.***

If you set `spec.tool.vdi.enabled` field to `true`, you can run applications that requires GUI such as Gazebo and rViz. Remember that enabling this field requires GPU acceleration in your Kubernetes cluster. To learn VDI connection port, run:

```bash
kubectl get robot linorobot2 -o jsonpath='{.spec.ros.vdi.neko.ports.tcp}' | yq -P
```

```bash
Output:
31040 # example
```

Open up a web browser and browse [http://cluster-hostname-or-external-ip:31040](-) to start using GUI applications to visualize your robot or it's data. GUI applications can either be started via `ros2 launch` or directly over VDI. If you run Gazebo, remember checking `GAZEBO_MASTER_URI` environment variable before opening up `gzclient`.

## Teleoperation

### ROS Bridge Suite

For teleoperation, robot has a feature to run ROS Bridge Suite package by default if field `spec.robot.tools.bridge.enabled` is set to `true`. To learn bridge's TCP websocket port, run:

```bash
kubectl get robot linorobot2 -o jsonpath='{.status.toolsStatus.status.bridgeServiceStatus.ports.bridge-ws}' | yq -P
```

```bash
Output:
32010 # example
```

You now can connect your teleoperation software (eg. roslibjs) [ws://cluster-hostname-or-external-ip:32010](-) to operate your robot remotely.

## Monitoring

### ROS Tracker

ROS Tracker is a REST service that collects realtime, robot-specific ROS data from robot, open-sourced by robolaunch. If you set `spec.robot.tools.tracker.enabled` to `true`, you can query your robot's data over HTTP. To learn the port that tracker listens, run:

```bash
kubectl get robot linorobot2 -o jsonpath='{.status.toolsStatus.status.trackerServiceStatus.ports.tracker}' | yq -P
```

```bash
Output:
31089 # example
```

Open up a web browser and try out ROS Tracker from address [http://cluster-hostname-or-external-ip:31089/docs](-). For more usage information, please check [ROS Tracker's Documentation](-).

### Foxglove Studio

[Foxglove Studio](https://foxglove.dev/) is an open source visualization and debugging tool for robotics. If you set `spec.robot.tools.foxglove.enabled` to `true`, you can visualize your data from Foxglove Studio. To learn that Foxglove Studio web app listens, run:

```bash
kubectl get robot linorobot2 -o jsonpath='{.status.toolsStatus.status.foxgloveServiceStatus.ports.foxglove}' | yq -P
```

```bash
Output:
31066 # example
```

Open up a web browser and try out Foxglove Studio from address [http://cluster-hostname-or-external-ip:31066](-). Add a connection from upper left "**+**" button and select connection with ROS Bridge. Enter ROS Bridge connection address ([ws://cluster-hostname-or-external-ip:ros-bridge-port](-)) to connect Foxglove Studio to your robot. Remember that to connect the robot with ROS Bridge, `spec.robot.tools.bridge.enabled` must be `true`. For more usage information, please check [Foxglove Studio's Documentation](https://foxglove.dev/docs/studio).
