# Dolly Deployment (Cloud Only)

GitHub: [alpylmz/dolly](https://github.com/alpylmz/dolly)

This document covers Dolly deployment using robolaunch Robot CRD.

## Declaration

Below there is the custom definition of Dolly. Copy it to a file named `dolly.yaml`. This robot is configured to start launching process since the field `spec.ros.state` is set to `Launched`. Change this field to disable building or launching process and directly start working on code.

Tools can be selected from the field `spec.ros.tools`. Remember that `spec.ros.tools.vdi` can only be enabled if there is GPU acceleration support in Kubernetes cluster.

**Important:** Field `spec.ros.clusterSelector` is required to deploy robot. If it's set to `abc`, then **all the Kubernetes nodes must be labeled before deployment as below**:

```yaml
# Kubernetes node's YAML
# In this case, robot's clusterSelector field's value must be `abc`. 
# ...
metadata:
    labels:
        abc: <CLUSTER-NAME>
# ...
```

```yaml
apiVersion: robot.roboscale.io/v1alpha1
kind: Robot
metadata:
  name: dolly
spec:
  robot:
    distro: foxy
    clusterSelector: cluster
    nodeSelector:
      robolaunch.io/platform: "true"
    state: Built
    tools:
      tracker:
        enabled: true
      cloudIDE:
        enabled: true
      bridge:
        enabled: false
      foxglove:
        enabled: false
      vdi:
        enabled: false
    mode: Single
    resources:
      storage: 15000
      cpuPerContainer: 800m
      memoryPerContainer: 512Mi
    namespacing: false
    workspaces:
    - name: dolly-ws
      repositories:
      - name: master_br
        url: https://github.com/alpylmz/dolly
        branch: foxy
        launch:
          launchFilePath: dolly_gazebo/launch/dolly.launch.py
          parameters:
            world: dolly_empty.world
      build: Standard
```

## Deployment

To deploy robot, run:

```bash
kubectl apply -f dolly.yaml
```

Now you can confirm deployment and watch robot's status by running:

```bash
watch kubectl get robot dolly
```

You can display configurational fields and events by running:

```bash
kubectl describe robot dolly
```

### Robot Stages

Please refer [Stages](../../usage/stages.md) documentation.

## Development and Monitoring

Please refer [Use-Cases and Tools](../../usage/tools.md) documentation.
