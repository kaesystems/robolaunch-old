# Linorobot Deployment (Cloud Only)

GitHub: [tunahanertekin/linorobot2](https://github.com/tunahanertekin/linorobot2)

This document covers Linorobot 2 deployment using robolaunch Robot CRD.

## Declaration

Below there is the custom definition of Linorobot 2. Copy it to a file named `linorobot2.yaml`. This robot is configured to start launching process since the field `spec.ros.state` is set to `Launched`. Change this field to disable building or launching process and directly start working on code.

Tools can be selected from the field `spec.ros.tools`. Remember that `spec.ros.tools.vdi` can only be enabled if there is GPU acceleration support in Kubernetes cluster.

**Important:** Field `spec.ros.clusterSelector` is required to deploy robot. If it's set to `abc`, then **all the Kubernetes nodes must be labeled before deployment as below**:

```yaml
# Kubernetes node's YAML
# In this case, robot's clusterSelector field's value must be `abc`. 
# ...
metadata:
    labels:
        abc: <CLUSTER-NAME>
# ...
```

```yaml
# linorobot2.yaml
apiVersion: robot.roboscale.io/v1alpha1
kind: Robot
metadata:
  name: linorobot2
spec:
  robot:
    clusterSelector: cluster
    nodeSelector:
      robolaunch.io/platform: "true"
    distro: foxy
    state: Launched
    tools:
      tracker:
        enabled: true
      cloudIDE:
        enabled: true
      bridge:
        enabled: false
      foxglove:
        enabled: false
      vdi:
        enabled: false
    mode: Single
    resources:
      storage: 15000
      cpuPerContainer: 800m
      memoryPerContainer: 512Mi
    namespacing: false
    workspaces:
    - name: linorobot-ws
      repositories:
      - name: master_br
        url: https://github.com/tunahanertekin/linorobot2
        branch: tuna
        launch:
          launchFilePath: linorobot2_gazebo/launch/gazebo.launch.py
          env:
          - name: LINOROBOT2_BASE
            value: 2wd
      build: Standard
```

## Deployment

To deploy robot, run:

```bash
kubectl apply -f linorobot2.yaml
```

Now you can confirm deployment and watch robot's status by running:

```bash
watch kubectl get robot linorobot2
```

You can display configurational fields and events by running:

```bash
kubectl describe robot linorobot2
```

### Robot Stages

Please refer [Stages](../../usage/stages.md) documentation.

## Development and Monitoring

Please refer [Use-Cases and Tools](../../usage/tools.md) documentation.
