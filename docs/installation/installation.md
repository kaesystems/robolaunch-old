# Installation

## Prerequisites

- Kubernetes cluster version >= `v1.18.X`
    - `kubectl` version >= `v1.18.X`
- Node label `robolaunch.io/platform=true`
    ```bash
    kubectl label node <NODE> robolaunch.io/platform=true
    ```
- Storage class
    - `rook-ceph-block` is required for now. Operator also supports `openebs` but it's not included in this version.
- `cert-manager` version >= `v1.8.X`
    - You can use appropriate `cert-manager` version under the directory `hack/deploy/` for every version of robot operator.
    - You can install it with:
        ```bash
        make get-cert-manager
        ```

## From Release

Add node selector to deployment and apply release YAMLs.

## From Repository

Clone the repository.

```bash
git clone https://github.com/robolaunch/robot-operator
```

Go inside the repository and apply the operator resources in [`hack/deploy/robot-operator`](../../hack/deploy/robot_operator.yaml).

```bash
cd robot-operator
make apply
```
