package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type NameTypeInfo struct {
	// Name of the object.
	Name string `json:"name,omitempty"`
	// Type(s) of the object.
	Type []string `json:"type,omitempty"`
}

// Node information.
type ROSNode struct {
	// Name of the node.
	Name string `json:"name,omitempty"`
	// Namespace of the node.
	Namespace string `json:"namespace,omitempty"`
	// Subscriber nodes.
	Subscribers []NameTypeInfo `json:"subscribers,omitempty"`
	// Publisher nodes.
	Publisher []NameTypeInfo `json:"publishers,omitempty"`
	// Service server nodes.
	ServiceServers []NameTypeInfo `json:"serviceServers,omitempty"`
	// Service client nodes.
	ServiceClients []NameTypeInfo `json:"serviceClients,omitempty"`
	// Action server nodes.
	ActionServers []NameTypeInfo `json:"actionServers,omitempty"`
	// Action client nodes.
	ActionClients []NameTypeInfo `json:"actionClients,omitempty"`
}

// Response of node request.
type NodeData struct {
	// Response message from ROS client library.
	Message string `json:"message,omitempty"`
	// Last time the node data is updated.
	LastUpdateTimestamp string `json:"lastUpdateTimestamp,omitempty"`
	// Node list.
	NodeList []ROSNode `json:"nodeList,omitempty"`
}

// RobotNodeSpec defines the desired state of RobotNode
type RobotNodeSpec struct {
	// Update frequency of node information.
	UpdateFrequency string `json:"updateFrequency,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// RobotNode is the Schema for the robotnodes API
type RobotNode struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Data NodeData      `json:"data,omitempty"`
	Spec RobotNodeSpec `json:"spec,omitempty"`
}

//+kubebuilder:object:root=true

// RobotNodeList contains a list of RobotNode
type RobotNodeList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotNode `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotNode{}, &RobotNodeList{})
}
