package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Interface for container artifacts of robolaunch. It's used to create containers
// for applications such as ROS tracker, Cloud IDE and ROS nodes.
type ContainerArtifact struct {
	// Name of container.
	Name string `json:"name,omitempty"`
	// Image of container.
	Image string `json:"image,omitempty"`
	// Open ports of container. Map keys should match the name of the container.
	Ports map[string]corev1.ContainerPort `json:"ports,omitempty"`
	// Entrypoint of container.
	Entrypoint []string `json:"entrypoint,omitempty"`
	// Environment variables of container.
	Env []corev1.EnvVar `json:"env,omitempty"`
}

// Storage class configuration for a volume type.
type StorageClassConfig struct {
	// Storage class name
	Name string `json:"name,omitempty"`
	// PVC access mode.
	AccessMode corev1.PersistentVolumeAccessMode `json:"accessMode,omitempty"`
}

// Storage class selection for persistent volume claims.
type StorageClassConfigs struct {
	// Storage class for workspace PVC. Since it is aimed to share workspace volume
	// across multiple clusters in Hybrid robots, storage classes that supports `ReadWriteMany`
	// feature should be selected. Defaulted to `obenebs-hostpath`.
	WorkspaceVolumes StorageClassConfig `json:"workspaceVolumes,omitempty"`
	// Storage class for Linux PVCs. To make robot's data persistent, it's various directories
	// are copied to PVCs and stored. Defaulted to `obenebs-hostpath`.
	LinuxVolumes StorageClassConfig `json:"linuxVolumes,omitempty"`
}

// Artifacts of robot.
type Artifacts struct {
	// Image and package manager selection for ROS distro.
	ROS ROS `json:"robot,omitempty"`
	// Storage class selection for robot's volumes.
	StorageClassConfigs StorageClassConfigs `json:"storageClassConfigs,omitempty"`
	// Container artifacts of jobs such as cloning and building.
	JobContainer ContainerArtifact `json:"rosContainer,omitempty"`
	// Container artifacts of Cloud IDE container.
	CloudIDEContainer ContainerArtifact `json:"cloudIDEContainer,omitempty"`
	// Container artifacts of ROS tracker container.
	TrackerContainer ContainerArtifact `json:"trackerContainer,omitempty"`
	// Container artifacts of ROS bridge container.
	BridgeContainer ContainerArtifact `json:"bridgeContainer,omitempty"`
	// Container artifacts of Foxglove container.
	FoxgloveContainer ContainerArtifact `json:"foxgloveContainer,omitempty"`
	// Container artifacts of VDI container.
	VDIContainer ContainerArtifact `json:"vdiContainer,omitempty"`
	// Container artifacts of ROS node containers.
	NodeContainer ContainerArtifact `json:"nodeContainer,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// RobotArtifact is the Schema for the robotartifacts API
type RobotArtifact struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Artifacts Artifacts       `json:"artifacts,omitempty"`
	Robot     RobotDefinition `json:"robot,omitempty"`
}

//+kubebuilder:object:root=true

// RobotArtifactList contains a list of RobotArtifact
type RobotArtifactList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotArtifact `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotArtifact{}, &RobotArtifactList{})
}
