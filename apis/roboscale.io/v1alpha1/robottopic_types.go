package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ROS node metadata.
type NodeSummary struct {
	// Name of the ROS node.
	NodeName string `json:"nodeName,omitempty"`
	// ROS namespace of the ROS node.
	NodeNamespace string `json:"nodeNamespace,omitempty"`
}

// Topic information.
type Topic struct {
	// Name of the topic.
	Name string `json:"name,omitempty"`
	// Type(s) of the topic.
	Type []string `json:"type,omitempty"`
	// Number of publisher nodes.
	PublisherCount int `json:"publisherCount,omitempty"`
	// Publisher nodes of topic.
	PublisherNodes []NodeSummary `json:"publisherNodes,omitempty"`
	// Number of subscriber nodes.
	SubscriberCount int `json:"subscriberCount,omitempty"`
	// Subscriber nodes of topic.
	SubscriberNodes []NodeSummary `json:"subscriberNodes,omitempty"`
}

// Response of topic request.
type TopicData struct {
	// Response message from ROS client library.
	Message string `json:"message,omitempty"`
	// Last time the topic data is updated.
	LastUpdateTimestamp string `json:"lastUpdateTimestamp,omitempty"`
	// Topic list.
	TopicList []Topic `json:"topicList,omitempty"`
}

// RobotTopicSpec defines the desired state of RobotTopic
type RobotTopicSpec struct {
	// Update frequency of topic information.
	UpdateFrequency string `json:"updateFrequency,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// RobotTopic is the Schema for the robottopics API
type RobotTopic struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Data TopicData      `json:"data,omitempty"`
	Spec RobotTopicSpec `json:"spec,omitempty"`
}

//+kubebuilder:object:root=true

// RobotTopicList contains a list of RobotTopic
type RobotTopicList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotTopic `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotTopic{}, &RobotTopicList{})
}
