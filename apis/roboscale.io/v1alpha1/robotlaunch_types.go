package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

type ContainerState string

const (
	ContainerStateRunning    ContainerState = "Running"
	ContainerStateTerminated ContainerState = "Terminated"
	ContainerStateWaiting    ContainerState = "Waiting"
)

type Container struct {
	Name  string         `json:"name,omitempty"`
	State ContainerState `json:"phase,omitempty"`
}

type NodeType string

const (
	NodeTypeProcess NodeType = "process"
	NodeTypeNode    NodeType = "node"
)

type Node struct {
	NodeID     string   `json:"nodeID,omitempty"`
	Type       NodeType `json:"nodeType,omitempty"`
	Package    string   `json:"package,omitempty"`
	Executable string   `json:"executable,omitempty"`
	Index      int      `json:"index,omitempty"`

	WorkspaceKey     int               `json:"workspaceKey,omitempty"`
	RepositoryKey    int               `json:"repositoryKey,omitempty"`
	LaunchParameters map[string]string `json:"launchParameters,omitempty"`

	Env       []corev1.EnvVar `json:"env,omitempty"`
	Container Container       `json:"container,omitempty"`
	Activated bool            `json:"activated,omitempty"`
}
type NodeRequirements struct {
	Fetched bool   `json:"fetched,omitempty"`
	Nodes   []Node `json:"nodes,omitempty"`
}

type TrackerInfo struct {
	IP   string `json:"ip,omitempty"`
	Port string `json:"port,omitempty"`
}

type RobotLaunchSpec struct {
	LastLaunchTimestamp string      `json:"lastLaunchTimestamp,omitempty"`
	TrackerInfo         TrackerInfo `json:"trackerInfo,omitempty"`
}

type LaunchPodStatus struct {
	Created bool            `json:"created,omitempty"`
	Phase   corev1.PodPhase `json:"phase,omitempty"`
	IP      string          `json:"ip,omitempty"`
}

type NodeRequest struct {
	Successful bool   `json:"successful,omitempty"`
	Nodes      []Node `json:"nodes,omitempty"`
}

type RobotLaunchPhase string

const (
	RobotLaunchPhaseCreatingPod   RobotLaunchPhase = "CreatingPod"
	RobotLaunchPhaseNodesNotFound RobotLaunchPhase = "NodesNotFound"
	RobotLaunchPhaseReady         RobotLaunchPhase = "Ready"
	RobotLaunchPhaseMalfunctioned RobotLaunchPhase = "Malfunctioned"
)

type RobotLaunchStatus struct {
	LaunchPodStatus     LaunchPodStatus  `json:"launchPodStatus,omitempty"`
	LastLaunchTimestamp string           `json:"lastLaunchTimestamp,omitempty"`
	NodeRequest         NodeRequest      `json:"nodeRequest,omitempty"`
	Phase               RobotLaunchPhase `json:"phase,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
type RobotLaunch struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RobotLaunchSpec   `json:"spec,omitempty"`
	Status RobotLaunchStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true
type RobotLaunchList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotLaunch `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotLaunch{}, &RobotLaunchList{})
}

func (robotlaunch *RobotLaunch) GetLaunchPodMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotlaunch.Name + launchPodPostfix,
		Namespace: robotlaunch.Namespace,
	}
}

func (robotlaunch *RobotLaunch) GetOwnerMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotlaunch.OwnerReferences[0].Name,
		Namespace: robotlaunch.Namespace,
	}
}
