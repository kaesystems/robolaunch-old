package v1alpha1

import (
	"errors"
	"reflect"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

var robotlog = logf.Log.WithName("robot-resource")

func (r *Robot) SetupWebhookWithManager(mgr ctrl.Manager) error {
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

//+kubebuilder:webhook:path=/mutate-robot-roboscale-io-v1alpha1-robot,mutating=true,failurePolicy=fail,sideEffects=None,groups=robot.roboscale.io,resources=robots,verbs=create;update,versions=v1alpha1,name=mrobot.kb.io,admissionReviewVersions=v1

var _ webhook.Defaulter = &Robot{}

func (r *Robot) Default() {
	robotlog.Info("default", "name", r.Name)

	r.Spec.Robot.WorkspacesPath = "/home/workspaces"

	if reflect.DeepEqual(r.Spec.Robot.Tools.VDI.Resources, ToolResources{}) {
		DefaultVDIResourceLimits(r)
	}

	if r.Spec.Robot.State == "" {
		r.Spec.Robot.State = RobotStatePassive
	}

	DefaultRepositoryPaths(r)
}

func DefaultVDIResourceLimits(r *Robot) {
	// r.Spec.Robot.Tools.VDI.Resources = ToolResources{
	// 	CPU:     "500m",
	// 	Memory:  "128Mi",
	// 	GPUCore: 2,
	// }
}

func DefaultRepositoryPaths(r *Robot) {
	for wsKey := range r.Spec.Robot.Workspaces {
		ws := r.Spec.Robot.Workspaces[wsKey]
		for repoKey := range ws.Repositories {
			repo := ws.Repositories[repoKey]
			repo.Path = r.Spec.Robot.WorkspacesPath + "/" + ws.Name + "/src/" + repo.Name
			ws.Repositories[repoKey] = repo
		}
		r.Spec.Robot.Workspaces[wsKey] = ws
	}
}

//+kubebuilder:webhook:path=/validate-robot-roboscale-io-v1alpha1-robot,mutating=false,failurePolicy=fail,sideEffects=None,groups=robot.roboscale.io,resources=robots,verbs=create;update,versions=v1alpha1,name=vrobot.kb.io,admissionReviewVersions=v1

var _ webhook.Validator = &Robot{}

func (r *Robot) ValidateCreate() error {
	robotlog.Info("validate create", "name", r.Name)

	err := r.checkNodeSelector()
	if err != nil {
		return err
	}
	err = r.checkClusterSelector()
	if err != nil {
		return err
	}
	err = r.checkMode()
	if err != nil {
		return err
	}
	err = r.checkTCPPorts()
	if err != nil {
		return err
	}
	err = r.checkUDPPortRange()
	if err != nil {
		return err
	}

	return nil
}

func (r *Robot) ValidateUpdate(old runtime.Object) error {
	robotlog.Info("validate update", "name", r.Name)

	// var robot *Robot
	// robot = old.DeepCopyObject().(*Robot)

	// fmt.Println("----------------------------------------------------")
	// fmt.Println(string(robot.Spec.Robot.Tools.VDI.Enabled))
	// fmt.Println("----------------------------------------------------")

	return nil
}

func (r *Robot) ValidateDelete() error {
	robotlog.Info("validate delete", "name", r.Name)

	return nil
}

func (r *Robot) checkNodeSelector() error {
	if reflect.DeepEqual(r.Spec.Robot.NodeSelector, map[string]string{}) {
		return errors.New(".spec.robot.nodeSelector cannot be empty")
	}
	return nil
}

func (r *Robot) checkClusterSelector() error {
	if reflect.DeepEqual(r.Spec.Robot.ClusterSelector, "") {
		return errors.New(".spec.robot.clusterSelector cannot be empty")
	}
	return nil
}

func (r *Robot) checkMode() error {
	if reflect.DeepEqual(r.Spec.Robot.Mode, "") {
		return errors.New(".spec.robot.mode cannot be empty")
	}
	return nil
}

// TO-DO: check nodeport availability
func (r *Robot) checkTCPPorts() error {
	return nil
}

// TO-DO: check nodeport availability
func (r *Robot) checkUDPPortRange() error {

	if !reflect.DeepEqual(r.Spec.Robot.Tools.Tracker.Ports.UDP.Range, "") {
		return errors.New(".spec.robot.tools.tracker.ports.udp.range should be empty")
	}

	if !reflect.DeepEqual(r.Spec.Robot.Tools.CloudIDE.Ports.UDP.Range, "") {
		return errors.New(".spec.robot.tools.cloudIDE.ports.udp.range should be empty")
	}

	if !reflect.DeepEqual(r.Spec.Robot.Tools.Bridge.Ports.UDP.Range, "") {
		return errors.New(".spec.robot.tools.bridge.ports.udp.range should be empty")
	}

	if !reflect.DeepEqual(r.Spec.Robot.Tools.Foxglove.Ports.UDP.Range, "") {
		return errors.New(".spec.robot.tools.foxglove.ports.udp.range should be empty")
	}

	return nil
}
