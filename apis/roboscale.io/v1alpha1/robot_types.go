package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

// ROS distro selection. Allowed distros are Foxy and Galactic. It is aimed to support Humble, Melodic and Noetic in further versions.
// +kubebuilder:validation:Enum=foxy;galactic
type ROSDistro string

const (
	// ROS Melodic Morenia
	ROSDistroMelodic ROSDistro = "melodic"
	// ROS Noetic Ninjemys
	ROSDistroNoetic ROSDistro = "noetic"
	// ROS Foxy Fitzroy
	ROSDistroFoxy ROSDistro = "foxy"
	// ROS Galactic Geochelone
	ROSDistroGalactic ROSDistro = "galactic"
)

type BuilderImage string

// +kubebuilder:validation:Enum=catkin;colcon
type ROSPackageManager string

const (
	ROSPackageManagerROS1 ROSPackageManager = "catkin"
	ROSPackageManagerROS2 ROSPackageManager = "colcon"
)

// ROS builder image and package manager. (colcon or catkin)
type ROS struct {
	// Builder image of ROS environment. Image must contain a ROS distro and helper tools.
	BuilderImage BuilderImage `json:"builderImage"`
	// ROS package manager.
	// Use `catkin` for ROS and `colcon` for ROS 2 distros.
	ROSPackageManager ROSPackageManager `json:"rosPackageManager,omitempty"`
}

// Prelaunch command or script is applied just before the node is started.
type Prelaunch struct {
	// Bash command to run before ROS node execution.
	// +kubebuilder:validation:Required
	Command string `json:"command"`
	// Script  string `json:"script,omitempty"`
}

// Launch description of a repository.
type Launch struct {
	// Additional environment variables to set when launching ROS nodes.
	Env []corev1.EnvVar `json:"env,omitempty"`
	// Path to launchfile in repository. (eg. `linorobot/linorobot_gazebo/launch.py`)
	// +kubebuilder:validation:Required
	LaunchFilePath string `json:"launchFilePath"`
	// Launch parameters.
	Parameters map[string]string `json:"parameters,omitempty"`
	// Command or script to run just before node's execution.
	Prelaunch Prelaunch `json:"prelaunch,omitempty"`
}

// Repository description.
type Repository struct {
	// Name of the repository.
	// +kubebuilder:validation:Required
	Name string `json:"name"`
	// Base URL of the repository.
	// +kubebuilder:validation:Required
	URL string `json:"url"`
	// Branch of the repository to clone.
	// +kubebuilder:validation:Required
	Branch string `json:"branch"`
	// [Optional] Launch description of the repository.
	Launch Launch `json:"launch,omitempty"`
	// [Autofilled] Absolute path of repository
	Path string `json:"path,omitempty"`
}

// Build type of a workspace. If `Standard`, `rosdep install` and `colcon build` commands are executed in order.
// If `Custom`, steps should be defined as bash commands or scripts.
// +kubebuilder:validation:Enum=Standard;Custom
type WorkspaceBuildType string

const (
	WorkspaceBuildTypeStandard WorkspaceBuildType = "Standard"
	WorkspaceBuildTypeCustom   WorkspaceBuildType = "Custom"
)

// Step is a command or script to execute when building a robot. Either `command` or `script` should be specified
// for each step.
type Step struct {
	// Name of the step.
	Name string `json:"name"`
	// Bash command to run.
	Command string `json:"command,omitempty"`
	// Bash script to run.
	Script string `json:"script,omitempty"`
	// Environment variables for step.
	Env []corev1.EnvVar `json:"env,omitempty"`
}

// Workspace description. Each robot should contain at least one workspace. A workspace should contain at least one
// repository in it. Building operations are executed seperately for each workspace. Global path of workspaces is
// `/home/workspaces`.
type Workspace struct {
	// Name of workspace. If a workspace's name is `my_ws`, it's absolute path is `/home/workspaces/my_ws`.
	// +kubebuilder:validation:Required
	Name string `json:"name"`
	// Repositories to clone inside workspace's `src` directory.
	// +kubebuilder:validation:MinItems=1
	Repositories []Repository `json:"repositories"`
	// Build type of a workspace. If `Standard`, `rosdep install` and `colcon build` commands are executed in order.
	// If `Custom`, steps should be defined as bash commands or scripts.
	// +kubebuilder:validation:Required
	Build WorkspaceBuildType `json:"build"`
	// Building steps of a workspace. If build type of a workspace is `Custom`, it's building steps can be defined as steps.
	BuildSteps []Step `json:"buildSteps,omitempty"`
}

// Robot's state. If `Passive`, only cloning operation will be made.
// +kubebuilder:validation:Enum=Launched;Built;Passive
type RobotState string

const (
	RobotStateLaunched RobotState = "Launched"
	RobotStateBuilt    RobotState = "Built"
	RobotStatePassive  RobotState = "Passive"
)

// Robot's resource limitations.
type RobotResources struct {
	// Specifies how much CPU will be allocated per container.
	// +kubebuilder:validation:Pattern=`^([0-9])+(m)$`
	// +kubebuilder:default="800m"
	CPUPerContainer string `json:"cpuPerContainer,omitempty"`
	// Specifies how much memory will be allocated per container.
	// +kubebuilder:validation:Pattern=`^([0-9])+(Mi|Gi)$`
	// +kubebuilder:default="512Mi"
	MemoryPerContainer string `json:"memoryPerContainer,omitempty"`
	// Specifies how much storage will be allocated per container.
	// +kubebuilder:default=10000
	Storage int `json:"storage,omitempty"`
}

// Robot's mode. If `Hybrid`, robot's nodes can be distributed across clusters.
// +kubebuilder:validation:Enum=Single;Hybrid
type RobotMode string

const (
	RobotModeSingle RobotMode = "Single"
	RobotModeHybrid RobotMode = "Hybrid"
)

// Configuration for discovery server connection of a robot.
type DiscoveryServer struct {
	// Option for discovery server connection.
	Enabled bool `json:"enabled,omitempty"`
	// GUID of discovery server.
	GUID string `json:"guid,omitempty"`
	// Specifies the cluster that discovery server is located. Connection string will be mutated if discovery server
	// is located to the same cluster with the robot or not. (because of DNS resolving)
	Cluster string `json:"cluster,omitempty"`
	// Hostname of discovery server.
	Hostname string `json:"hostname,omitempty"`
	// Subdomain of discovery server.
	Subdomain string `json:"subdomain,omitempty"`
}

// Export configuration of relevant data
type TypeExport struct {
	Enabled         bool   `json:"enabled,omitempty"`
	UpdateFrequency string `json:"updateFrequency,omitempty"`
}

// Data export configuration.
type DataExport struct {
	// Robot's node data export configuration.
	Nodes TypeExport `json:"nodes,omitempty"`
	// Robot's topic data export configuration.
	Topics TypeExport `json:"topics,omitempty"`
	// Robot's service data export configuration.
	Services TypeExport `json:"services,omitempty"`
	// Robot's action data export configuration.
	Actions TypeExport `json:"actions,omitempty"`
}

type ToolPorts struct {
	TCP int          `json:"tcp,omitempty"`
	UDP ToolPortsUDP `json:"udp,omitempty"`
}

type ToolPortsUDP struct {
	// +kubebuilder:validation:Pattern=`^([0-9])+-([0-9])+$`
	Range string `json:"range,omitempty"`
}

// Tool resource limits.
type ToolResources struct {
	GPUCore int `json:"gpuCore,omitempty"`
	// +kubebuilder:validation:Pattern=`^([0-9])+(m)$`
	CPU string `json:"cpu,omitempty"`
	// +kubebuilder:validation:Pattern=`^([0-9])+(Mi|Gi)$`
	Memory string `json:"memory,omitempty"`
}

// Tool configurations.
type ToolConfig struct {
	// Indicates if the tool is enabled or disabled.
	Enabled bool `json:"enabled,omitempty"`
	// Indicates tool's pod resource limits.
	Resources ToolResources `json:"resources,omitempty"`
	// Indicates application's (tool) node ports.
	Ports ToolPorts `json:"ports,omitempty"`
}

type Tools struct {
	// Configuration for ROS Tracker.
	Tracker ToolConfig `json:"tracker,omitempty"`
	// Configuration for Cloud IDE.
	CloudIDE ToolConfig `json:"cloudIDE,omitempty"`
	// Configuration for ROS Bridge Suite.
	Bridge ToolConfig `json:"bridge,omitempty"`
	// Configuration for Foxglove.
	Foxglove ToolConfig `json:"foxglove,omitempty"`
	// Configuration for VDI. It can only be used in Kubernetes clusters which has GPU acceleration.
	VDI ToolConfig `json:"vdi,omitempty"`
}

// Robot's detailed configuration.
type RobotDefinition struct {
	// ROS distro to be used.
	// +kubebuilder:validation:Required
	Distro ROSDistro `json:"distro"`
	// Resource limitations of robot containers.
	Resources RobotResources `json:"resources,omitempty"`
	// Global path of workspaces. It's fixed to `/home/workspaces` path.
	WorkspacesPath string `json:"workspacesPath,omitempty"`
	// Workspace definitions of robot.
	// +kubebuilder:validation:MinItems=1
	Workspaces []Workspace `json:"workspaces,omitempty"`
	// Robot's desired state.
	// +kubebuilder:validation:Required
	State RobotState `json:"state"`
	// Tool selection for robot.
	Tools Tools `json:"tools,omitempty"`
	// Data export configuration.
	DataExport DataExport `json:"dataExport,omitempty"`
	// Namespacing option. If `true`, nodes and topics will be namespaced by robot's name if possible.
	Namespacing bool `json:"namespacing,omitempty"`
	// Robot's mode. If `Hybrid`, robot's nodes can be distributed across clusters.
	// +kubebuilder:validation:Required
	Mode RobotMode `json:"mode"`
	// Configuration for discovery server connection of a robot.
	DiscoveryServer DiscoveryServer `json:"discoveryServer,omitempty"`
	// Cluster selector key that is required to be found at every node as a label key.
	// +kubebuilder:validation:Required
	ClusterSelector string `json:"clusterSelector"`
	// Package/Cluster matching for ROS nodes. Appliable for Hybrid robots.
	PackageClusterSelection map[string]string `json:"packageClusterSelection,omitempty"`
	// NodeSelector for scheduling robot pods.
	// +kubebuilder:validation:Required
	NodeSelector map[string]string `json:"nodeSelector"`
}

type ArtifactReference struct {
	// Robot artifact object namespace.
	Namespace string `json:"namespace,omitempty"`
	// Robot artifact object name.
	Name string `json:"name,omitempty"`
}

// Desired state of a robot.
type RobotSpec struct {
	// Desired state of robot and ROS environment.
	Robot RobotDefinition `json:"robot,omitempty"`
	// Desired values for Kubernetes resources of robot.
	ArtifactReference ArtifactReference `json:"artifacts,omitempty"`
}

type ArtifactStatus struct {
	Created bool `json:"created,omitempty"`
}

type ConfigStatus struct {
	Created bool              `json:"created,omitempty"`
	Status  RobotConfigStatus `json:"status,omitempty"`
}

type DataStatus struct {
	Created bool `json:"created,omitempty"`
}

type CloneStatus struct {
	Created bool             `json:"created,omitempty"`
	Status  RobotCloneStatus `json:"status,omitempty"`
}

type BuildStatus struct {
	Created bool             `json:"created,omitempty"`
	Status  RobotBuildStatus `json:"status,omitempty"`
}

type ToolsStatus struct {
	Created bool             `json:"created,omitempty"`
	Status  RobotToolsStatus `json:"status,omitempty"`
}

type RuntimeStatus struct {
	Created bool               `json:"created,omitempty"`
	Status  RobotRuntimeStatus `json:"status,omitempty"`
}

type K8sNodeInfo struct {
	Name   string `json:"name,omitempty"`
	HasGPU bool   `json:"hasGPU,omitempty"`
}

type RobotPhase string

const (
	RobotPhaseConfiguringEnvironment RobotPhase = "ConfiguringEnvironment"
	RobotPhaseCloningRepositories    RobotPhase = "CloningRepositories"
	RobotPhaseBuildingWorkspaces     RobotPhase = "BuildingWorkspaces"
	RobotPhaseLaunchingNodes         RobotPhase = "LaunchingNodes"
	RobotPhaseReady                  RobotPhase = "Ready"

	RobotPhaseFailedConfiguration RobotPhase = "FailedConfiguration"
	RobotPhaseFailedCloning       RobotPhase = "FailedCloning"
	RobotPhaseFailedBuilding      RobotPhase = "FailedBuilding"
	RobotPhaseFailedLaunching     RobotPhase = "FailedLaunching"

	RobotPhaseSleeping      RobotPhase = "Sleeping"
	RobotPhaseMalfunctioned RobotPhase = "Malfunctioned"
	RobotPhaseUnknown       RobotPhase = "Unknown"
)

type RobotStatus struct {
	ArtifactStatus ArtifactStatus `json:"artifactStatus,omitempty"`

	ConfigStatus  ConfigStatus  `json:"configStatus,omitempty"`
	CloneStatus   CloneStatus   `json:"cloneStatus,omitempty"`
	BuildStatus   BuildStatus   `json:"buildStatus,omitempty"`
	ToolsStatus   ToolsStatus   `json:"toolsStatus,omitempty"`
	RuntimeStatus RuntimeStatus `json:"runtimeStatus,omitempty"`
	DataStatus    DataStatus    `json:"dataStatus,omitempty"`

	Cluster  string      `json:"cluster,omitempty"`
	NodeInfo K8sNodeInfo `json:"nodeInfo,omitempty"`
	Phase    RobotPhase  `json:"phase,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:printcolumn:name="Distro",type=string,JSONPath=`.spec.robot.distro`
//+kubebuilder:printcolumn:name="Mode",type=string,JSONPath=`.spec.robot.mode`
//+kubebuilder:printcolumn:name="State",type=string,JSONPath=`.spec.robot.state`
//+kubebuilder:printcolumn:name="Phase",type=string,JSONPath=`.status.phase`
//+genclient
type Robot struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RobotSpec   `json:"spec,omitempty"`
	Status RobotStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true
type RobotList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Robot `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Robot{}, &RobotList{})
}

func (robot *Robot) GetRobotArtifactMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robot.Name + robotArtifactPostfix,
		Namespace: robot.Namespace,
	}
}

func (robot *Robot) GetRobotConfigMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robot.Name + robotConfigPostfix,
		Namespace: robot.Namespace,
	}
}

func (robot *Robot) GetRobotCloneMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robot.Name + robotClonePostfix,
		Namespace: robot.Namespace,
	}
}

func (robot *Robot) GetRobotBuildMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robot.Name + robotBuildPostfix,
		Namespace: robot.Namespace,
	}
}

func (robot *Robot) GetRobotToolsMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robot.Name + robotToolsPostfix,
		Namespace: robot.Namespace,
	}
}

func (robot *Robot) GetRobotRuntimeMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robot.Name + robotRuntimePostfix,
		Namespace: robot.Namespace,
	}
}

func (robot *Robot) GetRobotDataMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robot.Name + robotDataPostfix,
		Namespace: robot.Namespace,
	}
}
