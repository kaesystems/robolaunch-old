package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Service information.
type Service struct {
	// Name of the service.
	Name string `json:"name,omitempty"`
	// Type(s) of the service.
	Type []string `json:"type,omitempty"`
}

// Response of service request.
type ServiceData struct {
	// Response message from ROS client library.
	Message string `json:"message,omitempty"`
	// Last time the service data is updated.
	LastUpdateTimestamp string `json:"lastUpdateTimestamp,omitempty"`
	// Service list.
	ServiceList []Service `json:"actionList,omitempty"`
}

// RobotServiceSpec defines the desired state of RobotService
type RobotServiceSpec struct {
	// Update frequency of service information.
	UpdateFrequency string `json:"updateFrequency,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// RobotService is the Schema for the robotservices API
type RobotService struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Data ServiceData      `json:"data,omitempty"`
	Spec RobotServiceSpec `json:"spec,omitempty"`
}

//+kubebuilder:object:root=true

// RobotServiceList contains a list of RobotService
type RobotServiceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotService `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotService{}, &RobotServiceList{})
}
