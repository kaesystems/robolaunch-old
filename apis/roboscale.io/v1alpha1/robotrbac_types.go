package v1alpha1

import (
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

// RobotRBACSpec defines the desired state of RobotRBAC
type RobotRBACSpec struct {
	Rules []rbacv1.PolicyRule `json:"rules,omitempty"`
}

// RBAC resource information.
type RBACResourceStatus struct {
	// Indicates if RBAC resource is created.
	Created bool `json:"created,omitempty"`
	// Name reference of the RBAC Resource.
	Name string `json:"name,omitempty"`
}

type RobotRBACPhase string

const (
	RobotRBACPhaseCreatingRole           RobotRBACPhase = "CreatingRole"
	RobotRBACPhaseCreatingServiceAccount RobotRBACPhase = "CreatingServiceAccount"
	RobotRBACPhaseCreatingRoleBinding    RobotRBACPhase = "CreatingRoleBinding"
	RobotRBACPhaseReady                  RobotRBACPhase = "Ready"

	RobotRBACPhaseMalfunctioned RobotRBACPhase = "Malfunctioned"
)

// RobotRBACStatus defines the observed state of RobotRBAC
type RobotRBACStatus struct {
	RoleStatus           RBACResourceStatus `json:"roleStatus,omitempty"`
	ServiceAccountStatus RBACResourceStatus `json:"serviceAccountStatus,omitempty"`
	RoleBindingStatus    RBACResourceStatus `json:"roleBindingStatus,omitempty"`

	Phase RobotRBACPhase `json:"phase,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
type RobotRBAC struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RobotRBACSpec   `json:"spec,omitempty"`
	Status RobotRBACStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// RobotRBACList contains a list of RobotRBAC
type RobotRBACList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotRBAC `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotRBAC{}, &RobotRBACList{})
}

func (robotrbac *RobotRBAC) GetRoleMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotrbac.Name + robotRBACRolePostfix,
		Namespace: robotrbac.Namespace,
	}
}

func (robotrbac *RobotRBAC) GetServiceAccountMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotrbac.Name + robotRBACServiceAccountPostfix,
		Namespace: robotrbac.Namespace,
	}
}

func (robotrbac *RobotRBAC) GetRoleBindingMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotrbac.Name + robotRBACRoleBindingPostfix,
		Namespace: robotrbac.Namespace,
	}
}

func (robotrbac *RobotRBAC) GetOwnerMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotrbac.OwnerReferences[0].Name,
		Namespace: robotrbac.Namespace,
	}
}
