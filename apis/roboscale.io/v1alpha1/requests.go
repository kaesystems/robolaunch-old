package v1alpha1

type NodeRequests struct {
	LastUpdateRequest      string `json:"lastUpdateRequest,omitempty"`
	LastTerminationRequest string `json:"lastTerminationRequest,omitempty"`
}
