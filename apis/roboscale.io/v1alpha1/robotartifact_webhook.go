package v1alpha1

import (
	"path/filepath"
	"reflect"
	"strings"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

// log is for logging in this package.
var robotartifactlog = logf.Log.WithName("robotartifact-resource")

func (r *RobotArtifact) SetupWebhookWithManager(mgr ctrl.Manager) error {
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

//+kubebuilder:webhook:path=/mutate-robot-roboscale-io-v1alpha1-robotartifact,mutating=true,failurePolicy=fail,sideEffects=None,groups=robot.roboscale.io,resources=robotartifacts,verbs=create;update,versions=v1alpha1,name=mrobotartifact.kb.io,admissionReviewVersions=v1

var _ webhook.Defaulter = &RobotArtifact{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *RobotArtifact) Default() {
	robotartifactlog.Info("default", "name", r.Name)

	if reflect.DeepEqual(r.Artifacts.ROS, ROS{}) {
		DefaultDistro(r)
	}

	if reflect.DeepEqual(r.Artifacts.CloudIDEContainer, ContainerArtifact{}) {
		DefaultCloudIDEContainer(r)
	}

	if reflect.DeepEqual(r.Artifacts.JobContainer, ContainerArtifact{}) {
		DefaultJobContainer(r)
	}

	if reflect.DeepEqual(r.Artifacts.TrackerContainer, ContainerArtifact{}) {
		DefaultTrackerContainer(r)
	}

	if reflect.DeepEqual(r.Artifacts.BridgeContainer, ContainerArtifact{}) {
		DefaultBridgeContainer(r)
	}

	if reflect.DeepEqual(r.Artifacts.FoxgloveContainer, ContainerArtifact{}) {
		DefaultFoxgloveContainer(r)
	}

	if reflect.DeepEqual(r.Artifacts.VDIContainer, ContainerArtifact{}) {
		DefaultVDIContainer(r)
	}

	if reflect.DeepEqual(r.Artifacts.NodeContainer, ContainerArtifact{}) {
		DefaultNodeContainer(r)
	}

	if reflect.DeepEqual(r.Artifacts.StorageClassConfigs, StorageClassConfigs{}) {
		DefaultStorageClasses(r)
	}

}

func DefaultDistro(r *RobotArtifact) {
	const (
		MelodicImage  BuilderImage = "ros:melodic-ros-base-focal"
		NoeticImage   BuilderImage = "tunahanertekin/noetic-copier:v0.2.3"
		FoxyImage     BuilderImage = "robolaunchio/robot:base-foxy-agnostic-xfce"
		GalacticImage BuilderImage = "ros:galactic-ros-base-focal"
	)
	switch r.Robot.Distro {
	case "melodic":
		r.Artifacts.ROS = ROS{
			BuilderImage:      MelodicImage,
			ROSPackageManager: ROSPackageManagerROS1,
		}
	case "noetic":
		r.Artifacts.ROS = ROS{
			BuilderImage:      NoeticImage,
			ROSPackageManager: ROSPackageManagerROS1,
		}
	case "foxy":
		r.Artifacts.ROS = ROS{
			BuilderImage:      FoxyImage,
			ROSPackageManager: ROSPackageManagerROS2,
		}
	case "galactic":
		r.Artifacts.ROS = ROS{
			BuilderImage:      GalacticImage,
			ROSPackageManager: ROSPackageManagerROS2,
		}
	}
}

func DefaultCloudIDEContainer(r *RobotArtifact) {
	cloudIDEContainerPorts := map[string]corev1.ContainerPort{
		"cloud-ide": {
			Name:          "code-server",
			ContainerPort: 9000,
		},
	}

	var cmdBuilder strings.Builder
	cmdBuilder.WriteString("export PASSWORD=123 && ")
	cmdBuilder.WriteString("cat " + filepath.Join("/etc", "ros2_cli_override.sh") + " >> ~/.bashrc && ")
	cmdBuilder.WriteString("code-server " + r.Robot.WorkspacesPath + " --bind-addr 0.0.0.0:$CODE_SERVER_PORT")

	r.Artifacts.CloudIDEContainer = ContainerArtifact{
		Name:       "cloud-ide",
		Image:      "ubuntu:focal",
		Ports:      cloudIDEContainerPorts,
		Entrypoint: Bash(cmdBuilder.String()),
		Env: []corev1.EnvVar{
			Env("CODE_SERVER_PORT", "9000"),
			Env("ROBOT_NAMESPACE", r.Namespace),
			Env("ROBOT_NAME", r.Name),
			Env("TERM", "xterm-256color"),
		},
	}
}

func DefaultJobContainer(r *RobotArtifact) {
	r.Artifacts.JobContainer = ContainerArtifact{
		Image: "ubuntu:focal",
		Env:   GetROSEnvironmentVariables(r.Robot),
	}
}

func DefaultTrackerContainer(r *RobotArtifact) {

	dataExport := r.Robot.DataExport

	if !dataExport.Nodes.Enabled {
		dataExport.Nodes.UpdateFrequency = "0"
	} else {
		if dataExport.Nodes.UpdateFrequency == "0" {
			dataExport.Nodes.UpdateFrequency = "0.1"
		}
	}

	if !dataExport.Topics.Enabled {
		dataExport.Topics.UpdateFrequency = "0"
	} else {
		if dataExport.Topics.UpdateFrequency == "0" {
			dataExport.Topics.UpdateFrequency = "0.1"
		}
	}

	if !dataExport.Services.Enabled {
		dataExport.Services.UpdateFrequency = "0"
	} else {
		if dataExport.Services.UpdateFrequency == "0" {
			dataExport.Services.UpdateFrequency = "0.1"
		}
	}

	if !dataExport.Actions.Enabled {
		dataExport.Actions.UpdateFrequency = "0"
	} else {
		if dataExport.Actions.UpdateFrequency == "0" {
			dataExport.Actions.UpdateFrequency = "0.1"
		}
	}

	trackerWatchConfig := []corev1.EnvVar{
		Env("UPDATE_FREQUENCY", "0.2"),
		Env("K8S_NODE_FREQUENCY", dataExport.Nodes.UpdateFrequency),
		Env("K8S_TOPIC_FREQUENCY", dataExport.Topics.UpdateFrequency),
		Env("K8S_SERVICE_FREQUENCY", dataExport.Services.UpdateFrequency),
		Env("K8S_ACTION_FREQUENCY", dataExport.Actions.UpdateFrequency),
	}

	trackerPatchEnv := []corev1.EnvVar{
		Env("K8S_NAMESPACE", r.Namespace),
		Env("K8S_RESOURCE_GROUP", r.GroupVersionKind().Group),
		Env("K8S_RESOURCE_VERSION", r.GroupVersionKind().Version),
		Env("K8S_NODE_RESOURCE_KIND", "RobotNode"),
		Env("K8S_NODE_RESOURCE_NAME", r.Name+robotDataPostfix+robotNodePostfix),
		Env("K8S_TOPIC_RESOURCE_KIND", "RobotTopic"),
		Env("K8S_TOPIC_RESOURCE_NAME", r.Name+robotDataPostfix+robotTopicPostfix),
		Env("K8S_SERVICE_RESOURCE_KIND", "RobotService"),
		Env("K8S_SERVICE_RESOURCE_NAME", r.Name+robotDataPostfix+robotServicePostfix),
		Env("K8S_ACTION_RESOURCE_KIND", "RobotAction"),
		Env("K8S_ACTION_RESOURCE_NAME", r.Name+robotDataPostfix+robotActionPostfix),
	}

	trackerPatchEnv = append(trackerPatchEnv, trackerWatchConfig...)

	var cmdBuilder strings.Builder
	cmdBuilder.WriteString("cd $TRACKER_PATH && ")
	cmdBuilder.WriteString("echo \"export PATH=$PATH:" + filepath.Join("/var", "lib", "ros-tracker", "app", "helper_programs") + "\" >> ~/.bashrc && ")
	cmdBuilder.WriteString("source " + filepath.Join("/opt", "ros", "$DISTRO", "setup.bash") + " && ")
	cmdBuilder.WriteString("uvicorn app.main:app --host 0.0.0.0 --port $TRACKER_PORT")

	r.Artifacts.TrackerContainer = ContainerArtifact{
		Name:  "tracker",
		Image: "ubuntu:focal",
		Ports: map[string]corev1.ContainerPort{
			"tracker": {
				Name:          "tracker",
				ContainerPort: 5000,
			},
		},
		Entrypoint: Bash(cmdBuilder.String()),
		Env:        append(GetTrackerEnvironmentVariables(r.Robot), trackerPatchEnv...),
	}
}

func DefaultBridgeContainer(r *RobotArtifact) {

	var cmdBuilder strings.Builder
	cmdBuilder.WriteString("source /opt/ros/$DISTRO/setup.bash && ")
	cmdBuilder.WriteString("ros2 launch rosbridge_server rosbridge_websocket_launch.xml address:=0.0.0.0 port:=9090")

	r.Artifacts.BridgeContainer = ContainerArtifact{
		Name:       "bridge",
		Image:      "ubuntu:focal",
		Entrypoint: Bash(cmdBuilder.String()),
		Ports: map[string]corev1.ContainerPort{
			"bridge-ws": {
				Name:          "bridge-ws",
				ContainerPort: 9090,
			},
		},
	}
}

func DefaultFoxgloveContainer(r *RobotArtifact) {
	r.Artifacts.FoxgloveContainer = ContainerArtifact{
		Name:  "foxglove",
		Image: "ghcr.io/foxglove/studio:latest",
		Ports: map[string]corev1.ContainerPort{
			"foxglove": {
				Name:          "foxglove",
				ContainerPort: 8080,
			},
		},
	}
}

func DefaultVDIContainer(r *RobotArtifact) {

	var cmdBuilder strings.Builder
	cmdBuilder.WriteString(filepath.Join("/etc", "vdi", "generate-xorg.sh") + " && ")
	cmdBuilder.WriteString("supervisord -c " + filepath.Join("/etc", "vdi", "supervisord.conf"))

	r.Artifacts.VDIContainer = ContainerArtifact{
		Name:       "vdi",
		Image:      string(r.Artifacts.ROS.BuilderImage),
		Entrypoint: Bash(cmdBuilder.String()),
		Env: []corev1.EnvVar{
			Env("DISPLAY", ":0"),
			Env("VIDEO_PORT", "DFP"),
		},
	}
}

func DefaultNodeContainer(r *RobotArtifact) {
	r.Artifacts.NodeContainer = ContainerArtifact{
		Image: "ubuntu:focal",
	}
}

func DefaultStorageClasses(r *RobotArtifact) {
	r.Artifacts.StorageClassConfigs = StorageClassConfigs{
		WorkspaceVolumes: StorageClassConfig{
			Name:       "rook-ceph-block",
			AccessMode: corev1.ReadWriteOnce,
		},
		LinuxVolumes: StorageClassConfig{
			Name:       "rook-ceph-block",
			AccessMode: corev1.ReadWriteOnce,
		},
	}

}

//+kubebuilder:webhook:path=/validate-robot-roboscale-io-v1alpha1-robotartifact,mutating=false,failurePolicy=fail,sideEffects=None,groups=robot.roboscale.io,resources=robotartifacts,verbs=create;update,versions=v1alpha1,name=vrobotartifact.kb.io,admissionReviewVersions=v1

var _ webhook.Validator = &RobotArtifact{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *RobotArtifact) ValidateCreate() error {
	robotartifactlog.Info("validate create", "name", r.Name)

	// TODO(user): fill in your validation logic upon object creation.
	return nil
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *RobotArtifact) ValidateUpdate(old runtime.Object) error {
	robotartifactlog.Info("validate update", "name", r.Name)

	// TODO(user): fill in your validation logic upon object update.
	return nil
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *RobotArtifact) ValidateDelete() error {
	robotartifactlog.Info("validate delete", "name", r.Name)

	// TODO(user): fill in your validation logic upon object deletion.
	return nil
}
