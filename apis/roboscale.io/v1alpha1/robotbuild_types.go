package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

type RobotBuildSpec struct {
	Steps []Step `json:"steps,omitempty"`
}

type StepStatus struct {
	Step       Step     `json:"step,omitempty"`
	JobName    string   `json:"jobName,omitempty"`
	JobCreated bool     `json:"created,omitempty"`
	JobPhase   JobPhase `json:"jobPhase,omitempty"`
	// Status     batchv1.JobStatus `json:"status,omitempty"`
}

type ScriptConfigMapStatus struct {
	Created bool `json:"created,omitempty"`
}

type RobotBuildPhase string

const (
	RobotBuildPhaseCreatingConfigMap RobotBuildPhase = "CreatingConfigMap"
	RobotBuildPhaseBuildingRobot     RobotBuildPhase = "BuildingRobot"
	RobotBuildPhaseReady             RobotBuildPhase = "Ready"
	RobotBuildPhaseMalfunctioned     RobotBuildPhase = "Malfunctioned"
)

type RobotBuildStatus struct {
	ScriptConfigMapStatus ScriptConfigMapStatus `json:"scriptConfigMapStatus,omitempty"`
	Steps                 map[string]StepStatus `json:"steps,omitempty"`

	Phase RobotBuildPhase `json:"phase,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
type RobotBuild struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RobotBuildSpec   `json:"spec,omitempty"`
	Status RobotBuildStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true
type RobotBuildList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotBuild `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotBuild{}, &RobotBuildList{})
}

func (robotruntime *RobotBuild) GetConfigMapMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotruntime.Name + rosConfigMapPostfix,
		Namespace: robotruntime.Namespace,
	}
}

func (robotruntime *RobotBuild) GetOwnerMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotruntime.OwnerReferences[0].Name,
		Namespace: robotruntime.Namespace,
	}
}
