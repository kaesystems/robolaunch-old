package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

// RobotDataSpec defines the desired state of RobotData
type RobotDataSpec struct {
	DataExport DataExport `json:"dataExport,omitempty"`
}

type RobotDataTypeReference struct {
	Name      string `json:"name,omitempty"`
	Namespace string `json:"namespace,omitempty"`
}

// Abstract status of owned resource RobotNode.
type NodeStatus struct {
	// Indicates whether RobotNode resource is created or not.
	Created bool `json:"created,omitempty"`
	// Reference to the RobotTopic resource.
	Reference RobotDataTypeReference `json:"reference,omitempty"`
}

// Abstract status of owned resource RobotTopic.
type TopicStatus struct {
	// Indicates whether RobotTopic resource is created or not.
	Created bool `json:"created,omitempty"`
	// Reference to the RobotTopic resource.
	Reference RobotDataTypeReference `json:"reference,omitempty"`
}

// Abstract status of owned resource RobotService.
type ServiceStatus struct {
	// Indicates whether RobotService resource is created or not.
	Created bool `json:"created,omitempty"`
	// Reference to the RobotTopic resource.
	Reference RobotDataTypeReference `json:"reference,omitempty"`
}

// Abstract status of owned resource RobotAction.
type ActionStatus struct {
	// Indicates whether RobotAction resource is created or not.
	Created bool `json:"created,omitempty"`
	// Reference to the RobotTopic resource.
	Reference RobotDataTypeReference `json:"reference,omitempty"`
}

// RobotDataStatus defines the observed state of RobotData
type RobotDataStatus struct {
	// RobotNode resource status.
	NodeStatus NodeStatus `json:"nodeStatus,omitempty"`
	// RobotTopic resource status.
	TopicStatus TopicStatus `json:"topicStatus,omitempty"`
	// RobotService resource status.
	ServiceStatus ServiceStatus `json:"serviceStatus,omitempty"`
	// RobotAction resource status.
	ActionStatus ActionStatus `json:"actionStatus,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// RobotData is the Schema for the robotdata API
type RobotData struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RobotDataSpec   `json:"spec,omitempty"`
	Status RobotDataStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// RobotDataList contains a list of RobotData
type RobotDataList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotData `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotData{}, &RobotDataList{})
}

func (robotdata *RobotData) GetRobotNodeMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotdata.Name + robotNodePostfix,
		Namespace: robotdata.Namespace,
	}
}

func (robotdata *RobotData) GetRobotTopicMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotdata.Name + robotTopicPostfix,
		Namespace: robotdata.Namespace,
	}
}

func (robotdata *RobotData) GetRobotServiceMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotdata.Name + robotServicePostfix,
		Namespace: robotdata.Namespace,
	}
}

func (robotdata *RobotData) GetRobotActionMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotdata.Name + robotActionPostfix,
		Namespace: robotdata.Namespace,
	}
}

func (robotdata *RobotData) GetOwnerMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotdata.OwnerReferences[0].Name,
		Namespace: robotdata.Namespace,
	}
}
