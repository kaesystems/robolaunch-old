package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

type WorkspaceStatus struct {
	Created bool `json:"created,omitempty"`
}

type CloneJobStatus struct {
	Created bool     `json:"created,omitempty"`
	Phase   JobPhase `json:"phase,omitempty"`
}

type RobotCloneSpec struct {
}

type RobotClonePhase string

const (
	RobotClonePhaseCreatingWorkspace   RobotClonePhase = "CreatingWorkspace"
	RobotClonePhaseCloningRepositories RobotClonePhase = "CloningRepositories"
	RobotClonePhaseFailed              RobotClonePhase = "Failed"
	RobotClonePhaseReady               RobotClonePhase = "Ready"

	RobotClonePhaseMalfunctioned RobotClonePhase = "Malfunctioned"
)

type RobotCloneStatus struct {
	WorkspaceStatus WorkspaceStatus `json:"workspaceStatus,omitempty"`
	CloneJobStatus  CloneJobStatus  `json:"cloneJobStatus,omitempty"`
	Phase           RobotClonePhase `json:"phase,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
type RobotClone struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RobotCloneSpec   `json:"spec,omitempty"`
	Status RobotCloneStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true
type RobotCloneList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotClone `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotClone{}, &RobotCloneList{})
}

func (robotclone *RobotClone) GetPVCWorkspaceMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotclone.Name + rosVolumeWorkspacePostfix,
		Namespace: robotclone.Namespace,
	}
}

func (robotclone *RobotClone) GetCloneJobMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotclone.Name + cloneJobPostfix,
		Namespace: robotclone.Namespace,
	}
}

func (robotclone *RobotClone) GetOwnerMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotclone.OwnerReferences[0].Name,
		Namespace: robotclone.Namespace,
	}
}
