package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

type RobotToolsSpec struct {
	Tools Tools `json:"tools,omitempty"`
}

type ToolsPodStatus struct {
	Created bool            `json:"created,omitempty"`
	Phase   corev1.PodPhase `json:"phase,omitempty"`
	IP      string          `json:"ip,omitempty"`
}

type ToolsServiceStatus struct {
	Created bool             `json:"created,omitempty"`
	Ports   map[string]int32 `json:"ports,omitempty"`
}

type VDIStatusForRobot struct {
	Created bool      `json:"created,omitempty"`
	Status  VDIStatus `json:"status,omitempty"`
}

type RobotToolsPhase string

const (
	RobotToolsPhaseConfiguringTools RobotToolsPhase = "ConfiguringTools"
	RobotToolsPhaseReady            RobotToolsPhase = "Ready"
	RobotToolsPhaseMalfunctioned    RobotToolsPhase = "Malfunctioned"
)

type RobotToolsStatus struct {
	TrackerPodStatus      ToolsPodStatus     `json:"toolsPodStatus,omitempty"`
	TrackerServiceStatus  ToolsServiceStatus `json:"toolsServiceStatus,omitempty"`
	CloudIDEPodStatus     ToolsPodStatus     `json:"cloudIDEPodStatus,omitempty"`
	CloudIDEServiceStatus ToolsServiceStatus `json:"cloudIDEServiceStatus,omitempty"`
	BridgePodStatus       ToolsPodStatus     `json:"bridgePodStatus,omitempty"`
	BridgeServiceStatus   ToolsServiceStatus `json:"bridgeServiceStatus,omitempty"`
	FoxglovePodStatus     ToolsPodStatus     `json:"foxglovePodStatus,omitempty"`
	FoxgloveServiceStatus ToolsServiceStatus `json:"foxgloveServiceStatus,omitempty"`
	VDIInstanceStatus     VDIStatusForRobot  `json:"vdiStatus,omitempty"`

	Phase RobotToolsPhase `json:"phase,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

type RobotTools struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RobotToolsSpec   `json:"spec,omitempty"`
	Status RobotToolsStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

type RobotToolsList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotTools `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotTools{}, &RobotToolsList{})
}

func (robottools *RobotTools) GetTrackerPodMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robottools.Name + trackerPodPostfix,
		Namespace: robottools.Namespace,
	}
}

func (robottools *RobotTools) GetTrackerServiceMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robottools.Name + trackerServicePostfix,
		Namespace: robottools.Namespace,
	}
}

func (robottools *RobotTools) GetCloudIDEPodMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robottools.Name + cloudIDEPodPostfix,
		Namespace: robottools.Namespace,
	}
}

func (robottools *RobotTools) GetCloudIDEServiceMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robottools.Name + cloudIDEServicePostfix,
		Namespace: robottools.Namespace,
	}
}

func (robottools *RobotTools) GetOwnerMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robottools.OwnerReferences[0].Name,
		Namespace: robottools.Namespace,
	}
}

func (robottools *RobotTools) GetBridgePodMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robottools.Name + bridgePodPostfix,
		Namespace: robottools.Namespace,
	}
}

func (robottools *RobotTools) GetBridgeServiceMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robottools.Name + bridgeServicePostfix,
		Namespace: robottools.Namespace,
	}
}

func (robottools *RobotTools) GetFoxglovePodMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robottools.Name + foxglovePodPostfix,
		Namespace: robottools.Namespace,
	}
}

func (robottools *RobotTools) GetFoxgloveServiceMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robottools.Name + foxgloveServicePostfix,
		Namespace: robottools.Namespace,
	}
}

func (robottools *RobotTools) GetVDIMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robottools.Name + vdiPostfix,
		Namespace: robottools.Namespace,
	}
}
