package v1alpha1

const (
	robotConfigPostfix  = "-config"
	robotClonePostfix   = "-clone"
	robotBuildPostfix   = "-build"
	robotToolsPostfix   = "-tools"
	robotRuntimePostfix = "-run"

	robotArtifactPostfix = "-artifact"
	robotDataPostfix     = "-data"
	robotNodePostfix     = "-node"
	robotTopicPostfix    = "-topic"
	robotServicePostfix  = "-service"
	robotActionPostfix   = "-action"

	robotRBACPostfix               = "-rbac"
	robotRBACRolePostfix           = "-role"
	robotRBACServiceAccountPostfix = "-sa"
	robotRBACRoleBindingPostfix    = "-rb"

	rosVolumeVarPostfix       = "-pvc-var"
	rosVolumeOptPostfix       = "-pvc-opt"
	rosVolumeUsrPostfix       = "-pvc-usr"
	rosVolumeEtcPostfix       = "-pvc-etc"
	rosVolumeX11UnixPostfix   = "-pvc-x11"
	rosVolumeWorkspacePostfix = "-pvc-workspace"

	rosConfigMapPostfix = "-cm"
	loaderJobPostfix    = "-loader"
	cloneJobPostfix     = ""
	robotLaunchPostfix  = "-launch"

	trackerPodPostfix      = "-tracker"
	trackerServicePostfix  = "-tracker-svc"
	cloudIDEPodPostfix     = "-cloud-ide"
	cloudIDEServicePostfix = "-cloud-ide-svc"
	bridgePodPostfix       = "-bridge"
	bridgeServicePostfix   = "-bridge-svc"
	foxglovePodPostfix     = "-foxglove"
	foxgloveServicePostfix = "-foxglove-svc"
	vdiPostfix             = "-vdi"
	vdiPodPostfix          = ""
	vdiServicePostfix      = "-svc"

	launchPodPostfix = ""

	CustomScriptsPath               = "/etc/custom"
	X11UnixPath                     = "/tmp/.X11-unix"
	HelpersPath                     = "/var/lib/robolaunch-helpers/"
	RequestUpdateAnnotationKey      = "lastUpdateRequest"
	RequestTerminationAnnotationKey = "lastTerminationRequest"

	SuperClientConfig = "<?xml version='1.0' encoding='UTF-8' ?>" +
		"<dds>" +
		"<profiles xmlns='http://www.eprosima.com/XMLSchemas/fastRTPS_Profiles'>" +
		"<participant profile_name='super_client_profile' is_default_profile='true'>" +
		"<rtps>" +
		"		<builtin>" +
		"			<discovery_config>" +
		"				<discoveryProtocol>SUPER_CLIENT</discoveryProtocol>" +
		"				<discoveryServersList>" +
		"					<RemoteServer prefix='" + "%s" + "'>" +
		"						<metatrafficUnicastLocatorList>" +
		"							<locator>" +
		"								<udpv4>" +
		"								<address>" + "%s" + "</address>" +
		"									<port>11811</port>" +
		"								</udpv4>" +
		"							</locator>" +
		"						</metatrafficUnicastLocatorList>" +
		"					</RemoteServer>" +
		"				</discoveryServersList>" +
		"			</discovery_config>" +
		"		</builtin>" +
		"	</rtps>" +
		"</participant>" +
		"</profiles>" +
		"</dds>"
)

var ContainerPostfixLength int = 4
