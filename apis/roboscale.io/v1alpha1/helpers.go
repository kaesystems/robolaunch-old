package v1alpha1

import (
	"path/filepath"
	"strconv"
	"strings"

	corev1 "k8s.io/api/core/v1"
)

func Bash(command string) []string {
	return []string{
		"/bin/bash",
		"-c",
		command,
	}
}

func Env(key string, value string) corev1.EnvVar {
	return corev1.EnvVar{
		Name:  key,
		Value: value,
	}
}

func getWsSubDir(distro ROSDistro) string {
	if distro == ROSDistroMelodic ||
		distro == ROSDistroNoetic {
		return "devel"
	} else if distro == ROSDistroFoxy ||
		distro == ROSDistroGalactic {
		return "install"
	}
	return "install"
}

func GetCommonEnvironmentVariables(
	robotDef RobotDefinition,
) []corev1.EnvVar {

	environmentVariables := []corev1.EnvVar{
		Env("WORKSPACE", robotDef.WorkspacesPath),
		Env("DISTRO", string(robotDef.Distro)),
		Env("WSSUBDIR", getWsSubDir(robotDef.Distro)),
	}

	return environmentVariables
}

func GetROSEnvironmentVariables(
	robotDef RobotDefinition,
) []corev1.EnvVar {

	genericEnv := GetCommonEnvironmentVariables(
		robotDef,
	)

	environmentVariables := []corev1.EnvVar{
		Env("CODE_SERVER_PORT", "9000"),
	}

	env := append(genericEnv, environmentVariables...)

	return env
}

func GetCloneCommand(robotDef RobotDefinition, wsKey int) string {

	var cmdBuilder strings.Builder
	for key, repo := range robotDef.Workspaces[wsKey].Repositories {
		cmdBuilder.WriteString("git clone " + repo.URL + " -b " + repo.Branch + " " + repo.Name)
		if key != len(robotDef.Workspaces[wsKey].Repositories)-1 {
			cmdBuilder.WriteString("; ")
		}
	}
	return cmdBuilder.String()
}

func GetTrackerEnvironmentVariables(robotDef RobotDefinition) []corev1.EnvVar {

	genericEnv := GetCommonEnvironmentVariables(
		robotDef,
	)
	environmentVariables := []corev1.EnvVar{
		Env("TRACKER_PATH", "/var/lib/ros-tracker"),
		Env("TRACKER_PORT", "5000"),
	}

	return append(genericEnv, environmentVariables...)
}

func GetNodeContainerEnvironmentVariables(robotDef RobotDefinition, wsKey int) []corev1.EnvVar {

	genericEnv := GetCommonEnvironmentVariables(
		robotDef,
	)

	environmentVariables := []corev1.EnvVar{
		Env("WSSUBPATH", robotDef.Workspaces[wsKey].Name),
		Env("DISPLAY", ":0"),
	}

	return append(genericEnv, environmentVariables...)
}

func GenerateRunCommandAsEnv(robot Robot, node Node) corev1.EnvVar {

	robotName := robot.Name
	robotDef := robot.Spec.Robot

	commandKey := "COMMAND"
	launchPath := GetLaunchfilePathAbsoluteFromNode(node, robotDef)

	var parameterBuilder strings.Builder
	for key, val := range node.LaunchParameters {
		parameterBuilder.WriteString(key + ":=" + val + " ")
	}

	var cmdBuilder strings.Builder

	cmdBuilder.WriteString(HelpersPath + "robolaunch.py inspect ")
	cmdBuilder.WriteString("--launch-file " + launchPath)
	cmdBuilder.WriteString(" --launch-number " + strconv.Itoa(node.Index) + " ")
	cmdBuilder.WriteString(parameterBuilder.String())

	if robot.Spec.Robot.Namespacing {
		rosNs := strings.ReplaceAll(robotName, "-", "_")
		cmdBuilder.WriteString(" --namespace " + rosNs)
	}

	return Env(commandKey, cmdBuilder.String())
}

func GeneratePrelaunchCommandAsEnv(node Node, robotDef RobotDefinition, artifacts Artifacts) corev1.EnvVar {

	commandKey := "PRELAUNCH"
	command := robotDef.Workspaces[node.WorkspaceKey].Repositories[node.RepositoryKey].Launch.Prelaunch.Command

	if command == "" {
		command = "sleep 1"
	}

	return Env(commandKey, command)
}

func GetLaunchfilePathAbsoluteFromNode(node Node, robotDef RobotDefinition) string {

	workspacesPath := robotDef.WorkspacesPath
	wsName := robotDef.Workspaces[node.WorkspaceKey].Name
	repoName := robotDef.Workspaces[node.WorkspaceKey].Repositories[node.RepositoryKey].Name
	lfRelativePath := robotDef.Workspaces[node.WorkspaceKey].Repositories[node.RepositoryKey].Launch.LaunchFilePath

	return GetLaunchfilePathAbsolute(workspacesPath, wsName, repoName, lfRelativePath)
}

func GetWorkspaceSourceFilePath(workspacesPath string, wsName string, distro ROSDistro) string {
	return filepath.Join(workspacesPath, wsName, getWsSubDir(distro), "setup.bash")
}

func GetLaunchfilePathAbsolute(workspacesPath string, wsName string, repoName string, lfRelativePath string) string {
	return filepath.Join(workspacesPath, wsName, "src", repoName, lfRelativePath)
}
