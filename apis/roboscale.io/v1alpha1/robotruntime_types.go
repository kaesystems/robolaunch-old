package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

type RobotRuntimeSpec struct{}

type LaunchStatus struct {
	Created bool              `json:"created,omitempty"`
	Status  RobotLaunchStatus `json:"status,omitempty"`
}

type RobotRuntimePhase string

const (
	RobotRuntimePhaseCreatingRobotLaunch RobotRuntimePhase = "CreatingRobotLaunch"
	RobotRuntimePhaseReady               RobotRuntimePhase = "Ready"
	RobotRuntimePhaseMalfunctioned       RobotRuntimePhase = "Malfunctioned"
)

type RobotRuntimeStatus struct {
	LaunchStatus LaunchStatus `json:"launchStatus,omitempty"`

	Phase RobotRuntimePhase `json:"phase,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

type RobotRuntime struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RobotRuntimeSpec   `json:"spec,omitempty"`
	Status RobotRuntimeStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

type RobotRuntimeList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotRuntime `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotRuntime{}, &RobotRuntimeList{})
}

func (robotruntime *RobotRuntime) GetRobotLaunchMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotruntime.Name + robotLaunchPostfix,
		Namespace: robotruntime.Namespace,
	}
}

func (robotruntime *RobotRuntime) GetOwnerMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotruntime.OwnerReferences[0].Name,
		Namespace: robotruntime.Namespace,
	}
}
