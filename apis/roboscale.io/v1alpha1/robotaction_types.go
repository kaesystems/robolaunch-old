package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ActionNodeInfo struct {
	// Name of the node.
	Name string `json:"name,omitempty"`
	// Type of the action.
	Type string `json:"type,omitempty"`
}

// Action information.
type Action struct {
	// Name of the action.
	Name string `json:"name,omitempty"`
	// Type(s) of the action.
	Type []string `json:"type,omitempty"`
	// Client count of action.
	ClientCount int `json:"clientCount,omitempty"`
	// Client nodes of action.
	ClientNodes []ActionNodeInfo `json:"clientNodes,omitempty"`
	// Server count of action.
	ServerCount int `json:"serverCount,omitempty"`
	// Server nodes of action.
	ServerNodes []ActionNodeInfo `json:"serverNodes,omitempty"`
}

// Response of action request.
type ActionData struct {
	// Response message from ROS client library.
	Message string `json:"message,omitempty"`
	// Last time the action data is updated.
	LastUpdateTimestamp string `json:"lastUpdateTimestamp,omitempty"`
	// Action list.
	ActionList []Action `json:"actionList,omitempty"`
}

// RobotActionSpec defines the desired state of RobotAction
type RobotActionSpec struct {
	// Update frequency of action information.
	UpdateFrequency string `json:"updateFrequency,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// RobotAction is the Schema for the robotactions API
type RobotAction struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Data ActionData      `json:"data,omitempty"`
	Spec RobotActionSpec `json:"spec,omitempty"`
}

//+kubebuilder:object:root=true

// RobotActionList contains a list of RobotAction
type RobotActionList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotAction `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotAction{}, &RobotActionList{})
}
