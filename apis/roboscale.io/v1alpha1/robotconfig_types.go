package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

type JobPhase string

const (
	JobActive    JobPhase = "Active"
	JobSucceeded JobPhase = "Succeeded"
	JobFailed    JobPhase = "Failed"
)

type RobotConfigPhase string

const (
	RobotConfigPhaseCreatingRBAC           RobotConfigPhase = "CreatingRBAC"
	RobotConfigPhaseConfiguringEnvironment RobotConfigPhase = "ConfiguringEnvironment"
	RobotConfigPhaseFailed                 RobotConfigPhase = "Failed"
	RobotConfigPhaseReady                  RobotConfigPhase = "Ready"

	RobotConfigPhaseMalfunctioned RobotConfigPhase = "Malfunctioned"
)

type RobotConfigSpec struct{}

type Var struct {
	Created bool `json:"created,omitempty"`
}

type Opt struct {
	Created bool `json:"created,omitempty"`
}

type Usr struct {
	Created bool `json:"created,omitempty"`
}

type Etc struct {
	Created bool `json:"created,omitempty"`
}

type X11Unix struct {
	Created bool `json:"created,omitempty"`
}

type ROSVolumes struct {
	Var     Var     `json:"var,omitempty"`
	Opt     Opt     `json:"opt,omitempty"`
	Usr     Usr     `json:"usr,omitempty"`
	Etc     Etc     `json:"etc,omitempty"`
	X11Unix X11Unix `json:"x11Unix,omitempty"`
}

type RBACConfigStatus struct {
	Created bool           `json:"created,omitempty"`
	Phase   RobotRBACPhase `json:"phase,omitempty"`
}

type VolumeStatuses struct {
	ROS ROSVolumes `json:"robot,omitempty"`
}

type LoaderJobStatus struct {
	Created bool     `json:"created,omitempty"`
	Phase   JobPhase `json:"phase,omitempty"`
}

type RobotConfigStatus struct {
	TrackerRBACStatus  RBACConfigStatus `json:"trackerRBACStatus,omitempty"`
	CloudIDERBACStatus RBACConfigStatus `json:"cloudIDERBACStatus,omitempty"`
	VolumeStatuses     VolumeStatuses   `json:"volumeStatuses,omitempty"`
	LoaderJobStatus    LoaderJobStatus  `json:"loaderJobStatus,omitempty"`
	Phase              RobotConfigPhase `json:"phase,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

type RobotConfig struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RobotConfigSpec   `json:"spec,omitempty"`
	Status RobotConfigStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

type RobotConfigList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RobotConfig `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RobotConfig{}, &RobotConfigList{})
}

func (robotconfig *RobotConfig) GetTrackerRBACMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotconfig.Name + "-tracker" + robotRBACPostfix,
		Namespace: robotconfig.Namespace,
	}
}

func (robotconfig *RobotConfig) GetCloudIDERBACMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotconfig.Name + "-cloudide" + robotRBACPostfix,
		Namespace: robotconfig.Namespace,
	}
}

func (robotconfig *RobotConfig) GetPVCVarMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotconfig.Name + rosVolumeVarPostfix,
		Namespace: robotconfig.Namespace,
	}
}

func (robotconfig *RobotConfig) GetPVCOptMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotconfig.Name + rosVolumeOptPostfix,
		Namespace: robotconfig.Namespace,
	}
}

func (robotconfig *RobotConfig) GetPVCUsrMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotconfig.Name + rosVolumeUsrPostfix,
		Namespace: robotconfig.Namespace,
	}
}

func (robotconfig *RobotConfig) GetPVCEtcMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotconfig.Name + rosVolumeEtcPostfix,
		Namespace: robotconfig.Namespace,
	}
}

func (robotconfig *RobotConfig) GetPVCX11UnixMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotconfig.Name + rosVolumeX11UnixPostfix,
		Namespace: robotconfig.Namespace,
	}
}

func (robotconfig *RobotConfig) GetLoaderJobMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotconfig.Name + loaderJobPostfix,
		Namespace: robotconfig.Namespace,
	}
}

func (robotconfig *RobotConfig) GetOwnerMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotconfig.OwnerReferences[0].Name,
		Namespace: robotconfig.Namespace,
	}
}
