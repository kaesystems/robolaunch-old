package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

type VDISpec struct {
	Config ToolConfig `json:"config,omitempty"`
}

type VDIPodStatus struct {
	Created bool            `json:"created,omitempty"`
	Phase   corev1.PodPhase `json:"phase,omitempty"`
	IP      string          `json:"ip,omitempty"`
}

type VDIServiceStatus struct {
	Created bool `json:"created,omitempty"`
}

type VDIPhase string

const (
	VDIPhaseStarting VDIPhase = "Starting"
	VDIPhaseReady    VDIPhase = "Ready"
)

type VDIStatus struct {
	VDIPodStatus     VDIPodStatus     `json:"vdiPodStatus,omitempty"`
	VDIServiceStatus VDIServiceStatus `json:"vdiServiceStatus,omitempty"`

	Phase VDIPhase `json:"phase,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

type VDI struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   VDISpec   `json:"spec,omitempty"`
	Status VDIStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

type VDIList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []VDI `json:"items"`
}

func init() {
	SchemeBuilder.Register(&VDI{}, &VDIList{})
}

func (vdi *VDI) GetVDIPodMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Namespace: vdi.Namespace,
		Name:      vdi.Name + vdiPodPostfix,
	}
}

func (vdi *VDI) GetVDIServiceMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Namespace: vdi.Namespace,
		Name:      vdi.Name + vdiServicePostfix,
	}
}

func (robotruntime *VDI) GetOwnerMetadata() *types.NamespacedName {
	return &types.NamespacedName{
		Name:      robotruntime.OwnerReferences[0].Name,
		Namespace: robotruntime.Namespace,
	}
}
